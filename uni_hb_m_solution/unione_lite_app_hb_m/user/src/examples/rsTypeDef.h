/* 定义类型 */
#ifndef __RSTYPEDEF_H__
#define __RSTYPEDEF_H__

typedef unsigned char uchar;
typedef unsigned int  uint;
typedef unsigned long ulint;

/* 定义8位结构体 */
typedef struct
{
	uchar b7:1;
	uchar b6:1;
	uchar b5:1;
	uchar b4:1;
	uchar b3:1;
	uchar b2:1;
	uchar b1:1;
	uchar b0:1;
} BYTE_FIELD;

/* 定义16位结构体 */
typedef struct
{
	uint b15:1;
	uint b14:1;
	uint b13:1;
	uint b12:1;
	uint b11:1;
	uint b10:1;
	uint b9:1;
	uint b8:1;
	uint b7:1;
	uint b6:1;
	uint b5:1;
	uint b4:1;
	uint b3:1;
	uint b2:1;
	uint b1:1;
	uint b0:1;
} WORD_FIELD;

// 定义8位共用体
typedef union
{
	uchar ucByte;
	BYTE_FIELD stBit;
} TYPE_BYTE;

// 定义16位共用体
typedef union
{
	uint uiWord;
	uchar ucByte[2];
	WORD_FIELD stBit;
} TYPE_WORD;

/*============================================================================================================*/
typedef union{
	unsigned char byte;
	struct{
		unsigned char b0:1;
		unsigned char b1:1;
		unsigned char b2:1;
		unsigned char b3:1;
		unsigned char b4:1;
		unsigned char b5:1;
		unsigned char b6:1;
		unsigned char b7:1;	
	}bit;
}SegByte;
/*============================================================================================================*/
#endif//__RSTYPEDEF_H__