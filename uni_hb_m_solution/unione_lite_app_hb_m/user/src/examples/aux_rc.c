
#include "greebanlv.h"
#include "ir_tx.h"
#include "unione.h"
#include "aux_MacroDefine.h"
#include "iot_ctaux.h"

/*============================================================================================================*/

volatile SegByte auxFlag0;
volatile SegByte auxFlag1;
volatile SegByte auxFlag2;

#define fgDispTxdata00			auxFlag0.bit.b7
#define fgTime5msSft			auxFlag0.bit.b6
#define fgShrinkRun				auxFlag0.bit.b5
#define fgRmtStart				auxFlag0.bit.b4
#define fgRF_Txd_Data			auxFlag0.bit.b3
#define fgRmtDataEndBit			auxFlag0.bit.b2
#define fgRmtDataDivide			auxFlag0.bit.b1
//#define 	 			auxFlag0.bit.b0

#define fgLRSwing		 	auxFlag1.bit.b7
#define fgDispRxding		auxFlag1.bit.b6
#define fgDispRxd	 		auxFlag1.bit.b5
#define fgDispRxdChk 		auxFlag1.bit.b4
#define fgDispCommErr 		auxFlag1.bit.b3
#define fgUDSwing 			auxFlag1.bit.b2
#define fgRF_Txd_ready 		auxFlag1.bit.b1
#define fgVoiceCommErr	 	auxFlag1.bit.b0

#define fgInitFlag 			auxFlag2.bit.b2
#define fgCtrol 			auxFlag2.bit.b1
#define fgWakeUp	 		auxFlag2.bit.b0

volatile unsigned char guaux8DispTxd_2ms;
volatile unsigned char guaux8DispRxdDly_2ms;
volatile unsigned char guaux8DispComm_1s;
volatile unsigned char guaux8DispRxdCnt;
volatile unsigned char guaux8DispTxdCnt;
volatile unsigned char guaux8DispTxdCode;
volatile unsigned char guaux8DispRxdBuf[DISPRXDCNT];
volatile unsigned char guaux8DispRxdTmpBuf[DISPRXDCNT];
volatile unsigned char guaux8DispTxdBuf[DISPTXDCNT];

unsigned char guaux8SetMode;
unsigned char guaux8SetFan;
unsigned char guaux8SetTemp;
//unsigned char guaux8RunMode;
unsigned char guaux8SetDisp_onOff;

volatile unsigned char guaux8Time1ms_200us;

volatile unsigned char guaux8Time2ms_10us;

volatile unsigned int guaux16Time5ms_10us;
volatile unsigned char guaux8Time100ms_5ms;
volatile unsigned char guaux8Time1s_5ms;
volatile unsigned char guaux8Time50ms_200us;
volatile unsigned char guaux8Time1s_50ms;

volatile unsigned int guaux16RmtLen;
volatile unsigned char  guaux8RmtBit;
volatile unsigned char  guaux8RmtCnt;
volatile unsigned char  guaux8RmtDly_1s;

volatile unsigned char guaux8TimeShrink1s_5ms;

volatile unsigned char guaux8RmtBuf[RMTCNT];
volatile unsigned char guaux8RmDataTxd;
volatile unsigned char guaux8RmDataBitLen;

volatile unsigned char guaux8RT_TxdNUM;

volatile unsigned char guaux8RF_TempB;
volatile unsigned char guaux8RF_TempC;

volatile unsigned char guaux8LEDRunTime_Red;
volatile unsigned char guaux8Disp_OnOff;
//volatile unsigned char guaux8LEDRunTime_Blu;

volatile unsigned char guaux8VoiceRstTime_Init;
volatile unsigned char guaux8VoiceRstTime_Run;
volatile unsigned char guaux8VoiceErrorTime;

volatile unsigned char guaux8DisOn;

volatile unsigned char guaux8SetTem_Auto;
volatile unsigned char guaux8SetTem_Cool;
volatile unsigned char guaux8SetTem_Dry;
volatile unsigned char guaux8SetTem_Fan;
volatile unsigned char guaux8SetTem_Heat;
volatile unsigned char guaux8FanMemory_Auto;
volatile unsigned char guaux8FanMemory_Cool;
volatile unsigned char guaux8FanMemory_Dry;
volatile unsigned char guaux8FanMemory_Fan;
volatile unsigned char guaux8FanMemory_Heat;
volatile unsigned char guaux8TurboTiny_Auto;
volatile unsigned char guaux8TurboTiny_Cool;
volatile unsigned char guaux8TurboTiny_Dry;
volatile unsigned char guaux8TurboTiny_Fan;
volatile unsigned char guaux8TurboTiny_Heat;

volatile unsigned char guaux8SetMode_Memory;

extern ctaux_air_status g_ataux_air_status_bsp;
/*============================================================================================================*/


/*============================================================================================================*/
void aux_remote_isr(void)
{
	//guaux8Time2ms_10us++;
	//guaux16Time5ms_10us++;

	if(fgRF_Txd_ready)
	{
		if(guaux16RmtLen<0xffff)
		{
			guaux16RmtLen++;
		}

		if (fgRmtStart == 1)
		{
			if (fgRmtDataEndBit == 0)
			{
				if(guaux8RmtBit == 0)
				{
					guaux8RmDataTxd = guaux8RmtBuf[guaux8RmtCnt];
				}

				if(guaux8RmDataTxd & 0x01)
				{
					guaux8RmDataBitLen = (((56+169)*10)/TIMER2_PERIOD);//56+169;
				}
				else
				{
					guaux8RmDataBitLen = (((56+56)*10)/TIMER2_PERIOD);//56+56;
				}
#if 0
				if(guaux16RmtLen EQU 1)
				{
//					TMR01H = 0x80;
//					TDR01L = 32;
//					TOE0 |= 0x02;
//					TS0 |= 0x02;
					ir_tx_pin_low();
				}
				else if(guaux16RmtLen <56)
				{
					;
				}
				else if((guaux16RmtLen <= 56))
				{
//					TT0 |= 0x02;
//					TOE0 &= 0xFD;
//					TO0 &= 0xFD;
					ir_tx_pin_high();
				}
				else if(guaux16RmtLen > guaux8RmDataBitLen)
				{
					guaux16RmtLen = 0;
					guaux8RmDataTxd >>= 1;
					guaux8RmtBit++;
					if (guaux8RmtBit >= 8)
					{
						guaux8RmtBit = 0;
						guaux8RmtCnt++;
						if(guaux8RmtCnt>= RMTCNT)
						{
							fgRmtDataEndBit = 1;
						}
					}
				}
#else
				//data
				if(guaux16RmtLen <= ((56*10)/TIMER2_PERIOD))
				{
//					TMR01H = 0x80;
//					TDR01L = 32;
//					TOE0 |= 0x02;
//					TS0 |= 0x02;
					ir_tx_pin_low();
				}
				else if((guaux16RmtLen <= guaux8RmDataBitLen))
				{
//					TT0 |= 0x02;
//					TOE0 &= 0xFD;
//					TO0 &= 0xFD;
					ir_tx_pin_high();
				}
				else
				{
					guaux16RmtLen = 0;
					guaux8RmDataTxd >>= 1;
					guaux8RmtBit++;
					if (guaux8RmtBit >= 8)
					{
						guaux8RmtBit = 0;
						guaux8RmtCnt++;
						if(guaux8RmtCnt>= RMTCNT)
						{
							fgRmtDataEndBit = 1;
						}
					}
				}

#endif
			}
			else
			{
				//if(!fgRmtDataDivide)
				{
#if 0
					if(guaux16RmtLen EQU 1)
					{
//						TMR01H = 0x80;
//						TDR01L = 32;
//						TOE0 |= 0x02;
//						TS0 |= 0x02;
						ir_tx_pin_low();
					}
					else if(guaux16RmtLen <=56)
					{
						;
					}
					else
					{
//						TT0 |= 0x02;
//						TOE0 &= 0xFD;
//						TO0 &= 0xFD;
						ir_tx_pin_high();

						guaux16RmtLen = 0;

						//if (guaux8RmtCnt >= RMTCNT)
						{
							fgRF_Txd_ready = 0;
							fgRmtStart = 0;
							fgRmtDataEndBit = 0;
							fgRmtDataDivide = 0;
							guaux8RmtCnt = 0;
							guaux8RmtBit = 0;
							guaux16RmtLen = 0;
						}
//						else
//						{
//							fgRmtDataDivide = 1;
//						}
					}
#else
					//end
					if(guaux16RmtLen <= ((56*10)/TIMER2_PERIOD))
					{
//						TMR01H = 0x80;
//						TDR01L = 32;
//						TOE0 |= 0x02;
//						TS0 |= 0x02;
						ir_tx_pin_low();
					}
					else
					{
//						TT0 |= 0x02;
//						TOE0 &= 0xFD;
//						TO0 &= 0xFD;
						ir_tx_pin_high();

						guaux16RmtLen = 0;

						//if (guaux8RmtCnt >= RMTCNT)
						{
							fgRF_Txd_ready = 0;
							fgRmtStart = 0;
							fgRmtDataEndBit = 0;
							fgRmtDataDivide = 0;
							guaux8RmtCnt = 0;
							guaux8RmtBit = 0;
							guaux16RmtLen = 0;
						}
//						else
//						{
//							fgRmtDataDivide = 1;
//						}
					}
#endif
				}
//				else
//				{
//					if(guaux16RmtLen <=522)
//					{
//						TT0 |= 0x02;
//						TOE0 &= 0xFD;
//						TO0 &= 0xFD;
//						ir_tx_pin_high();
//					}
//					else
//					{
//						guaux16RmtLen = 0;
//						fgRmtDataDivide = 0;
//						fgRmtDataEndBit = 0;
//
//						fgRmtStart = 0;
//					}
//				}
			}
		}
		else
		{
#if 0
			if(guaux16RmtLen == 1)
			{
//				TMR01H = 0x80;
//				TDR01L = 32;
//				TOE0 |= 0x02;
//				TS0 |= 0x02;
				ir_tx_pin_low();
				printf("Start0\n");
			}
			else if(guaux16RmtLen <= 900)
			{
				;
			}
			else if((guaux16RmtLen > 900) AND (guaux16RmtLen <= 1350))
			{
//				TT0 |= 0x02;
//				TOE0 &= 0xFD;
//				TO0 &= 0xFD;
				ir_tx_pin_high();
				printf("Start1\n");
			}
			else if(guaux16RmtLen > 1350)
			{
				guaux16RmtLen = 0;
				fgRmtStart = 1;
				printf("Start2\n");
			}
#else
			// head
			 if(guaux16RmtLen <= ((700*10)/TIMER2_PERIOD)/*900*/)//不知道为什么多2秒
			{
//				TMR01H = 0x80;
//				TDR01L = 32;
//				TOE0 |= 0x02;
//				TS0 |= 0x02;
				ir_tx_pin_low();
				//printf("Start0\n");
			}
			else if(guaux16RmtLen <= ((1150*10)/TIMER2_PERIOD)/*1350*/)
			{
//				TT0 |= 0x02;
//				TOE0 &= 0xFD;
//				TO0 &= 0xFD;
				ir_tx_pin_high();
				//printf("Start1\n");
			}
			else
			{
				guaux16RmtLen = 0;
				fgRmtStart = 1;
				//printf("Start2\n");
			}

#endif
		}
	}
#if 0
	else
	{
		guaux16RmtLen = 0;
		fgRmtStart = 0;
		fgRmtDataEndBit = 0;
		fgRmtDataDivide = 0;
		guaux8RmtCnt = 0;
		guaux8RmtBit = 0;

//		TT0 |= 0x02;
//		TOE0 &= 0xFD;
//		TO0 &= 0xFD;
	}
#endif
}

/*************************************************************
 ������aux_encode_irdata
 ���룺
 �����
 ���ܣ�
 �÷���
*************************************************************/
void aux_encode_irdata(unsigned char* data)
{
	unsigned char	i,j,u8Tmp = 0;
	unsigned char	k,u8VerifySum = 0;

		guaux8DispTxd_2ms = 0;
		fgDispTxdata00 = 1;
		guaux8VoiceErrorTime = 0;
		for (u8Tmp = 0;u8Tmp < DISPRXDCNT;u8Tmp++)
		{
			guaux8DispRxdBuf[u8Tmp] = data[u8Tmp];
		}
		if((guaux8DispRxdBuf[1] EQU 0x00) OR (guaux8DispRxdBuf[1] EQU 0x01))
		{
			;
//			if(guaux8DispRxdBuf[1] EQU 0x01)
//			{
//				if((guaux8DispRxdBuf[2] EQU 0x00) AND (guaux8DispRxdBuf[3] EQU 0x01))
//				{
//					//guaux8LEDRunTime_Blu = 100;
//					fgWakeUp = 1;
//				}
//				else
//				{
//					fgWakeUp = 0;
//				}
//			}
		}
		else
		{
			guaux8LEDRunTime_Red = 0;
			//guaux8LEDRunTime_Blu = 110;

			fgCtrol = 1;
			guaux8Disp_OnOff = 0;

			guaux8RmtBuf[0] = 0xC3;

			if((guaux8DispRxdBuf[1] EQU 0x02) &&(guaux8DispRxdBuf[2] EQU 0x02))
			{
			
				guaux8RmtBuf[9] = 0x00;
				g_ataux_air_status_bsp.ack_onoff=01;
				//printf("close++++++++++\n");
			}
			else
			{
				guaux8RmtBuf[9] = 0x20;
				g_ataux_air_status_bsp.ack_onoff=02;
				//printf("open===========\n");
			}
			//if(guaux8RmtBuf[9] & 0x20)
			
				if(guaux8DispRxdBuf[1] EQU 0x05)
				{
					switch (guaux8DispRxdBuf[2])
					{
						case 0x01:
							guaux8SetMode = AUTOMODE;
							guaux8RmtBuf[6] = 0x00;
							break;
						case 0x02:
							guaux8SetMode = COOLMODE;
							guaux8RmtBuf[6] = 0x20;
							break;
						case 0x03:
							guaux8SetMode = DRYMODE;
							guaux8RmtBuf[6] = 0x40;
							break;
						case 0x04:
							guaux8SetMode = FANMODE;
							guaux8RmtBuf[6] = 0xC0;
							break;
						case 0x05:
							guaux8SetMode = HEATMODE;
							guaux8RmtBuf[6] = 0x80;
							break;
						default:
							//guaux8SetMode = NOMODE;
							guaux8SetMode = AUTOMODE;
							guaux8RmtBuf[6] = 0x00;
							break;
					}
				}

				if(guaux8SetMode_Memory !=guaux8RmtBuf[6])
				{
					if(guaux8RmtBuf[6] EQU 0x00)
					{
						guaux8SetTemp = guaux8SetTem_Auto;
						guaux8RmtBuf[4] = guaux8FanMemory_Auto;
						guaux8RmtBuf[5] = guaux8TurboTiny_Auto;
					}
					else if(guaux8RmtBuf[6] EQU 0x20)
					{
						guaux8SetTemp = guaux8SetTem_Cool;
						guaux8RmtBuf[4] = guaux8FanMemory_Cool;
						guaux8RmtBuf[5] = guaux8TurboTiny_Cool;
					}
					else if(guaux8RmtBuf[6] EQU 0x40)
					{
						guaux8SetTemp = guaux8SetTem_Dry;
						guaux8RmtBuf[4] = guaux8FanMemory_Dry;
						guaux8RmtBuf[5] = guaux8TurboTiny_Dry;
					}
					else if(guaux8RmtBuf[6] EQU 0xC0)
					{
						guaux8SetTemp = guaux8SetTem_Fan;
						guaux8RmtBuf[4] = guaux8FanMemory_Fan;
						guaux8RmtBuf[5] = guaux8TurboTiny_Fan;
					}
					else if(guaux8RmtBuf[6] EQU 0x80)
					{
						guaux8SetTemp = guaux8SetTem_Heat;
						guaux8RmtBuf[4] = guaux8FanMemory_Heat;
						guaux8RmtBuf[5] = guaux8TurboTiny_Heat;
					}
				}
				guaux8SetMode_Memory = guaux8RmtBuf[6];

				i = 0x00;
				j = 0x00;
				if(guaux8DispRxdBuf[1] EQU 0x07)
				{
					if(guaux8DispRxdBuf[2] EQU 0x01)
					{
						i = 0x00;
						j = guaux8RmtBuf[2] & 0x1F;
						guaux8RmtBuf[2] = j | (i<<5);
						fgLRSwing = 1;
					}
					else if(guaux8DispRxdBuf[2] EQU 0x02)
					{
						i = 0x07;
						j = guaux8RmtBuf[2] & 0x1F;
						guaux8RmtBuf[2] = j | (i<<5);
						fgLRSwing = 0;
					}
					else if(guaux8DispRxdBuf[2] EQU 0x03)
					{
						i = 0x00;
						j = guaux8RmtBuf[1] & 0xF8;
						guaux8RmtBuf[1] = i | j;
						fgUDSwing = 1;
					}
					else if(guaux8DispRxdBuf[2] EQU 0x04)
					{
						i = 0x07;
						j = guaux8RmtBuf[1] & 0xF8;
						guaux8RmtBuf[1] = i | j;
						fgUDSwing = 0;
					}
				}

				i = 0x00;
				j = 0x00;
				if(guaux8DispRxdBuf[1] EQU 0x04)
				{
					j = guaux8DispRxdBuf[2] + 7;
					if(j<=8)
					{
						j = 8;
					}
					else if(j>=24)
					{
						j = 24;
					}
					guaux8SetTemp = j;
					i = guaux8RmtBuf[1] & 0x07;
					guaux8RmtBuf[1] = i | (j<<3);
				}

				i = 0x00;
				if(guaux8DispRxdBuf[1] EQU 0x03)
				{
					if(guaux8DispRxdBuf[2] EQU 0x01)
					{
						if(guaux8SetTemp>=24)
						{
							guaux8SetTemp = 24;
						}
						else if(guaux8SetTemp<=8)
						{
							guaux8SetTemp = 9;
						}
						else
						{
							guaux8SetTemp++;
						}
					}
					else if(guaux8DispRxdBuf[2] EQU 0x02)
					{
						if(guaux8SetTemp>=24)
						{
							guaux8SetTemp = 23;
						}
						else if(guaux8SetTemp<=8)
						{
							guaux8SetTemp = 8;
						}
						else
						{
							guaux8SetTemp--;
						}
					}
					i = guaux8RmtBuf[1] & 0x07;
					guaux8RmtBuf[1] = i | (guaux8SetTemp<<3);
				}
				i = 0x00;
				if(guaux8SetTemp<8)
				{
					guaux8SetTemp = 18;
					i = guaux8RmtBuf[1] & 0x07;
					guaux8RmtBuf[1] = i | (guaux8SetTemp<<3);
				}

				if(guaux8DispRxdBuf[1] EQU 0x06)
				{
					switch (guaux8DispRxdBuf[2])
					{
						case 0x01:
							guaux8SetFan = TINYFAN;
							guaux8RmtBuf[4] = 0x60;
							guaux8RmtBuf[5] = 0x80;
							break;

						case 0x02:
							guaux8SetFan = LOWFAN;
							guaux8RmtBuf[4] = 0x60;
							guaux8RmtBuf[5] = 0x00;
							break;

						case 0x03:
							guaux8SetFan = MIDFAN;
							guaux8RmtBuf[4] = 0x40;
							guaux8RmtBuf[5] = 0x00;
							break;

						case 0x04:
							guaux8SetFan = HIGHFAN;
							guaux8RmtBuf[4] = 0x20;
							guaux8RmtBuf[5] = 0x00;
							break;

						case 0x05:
							guaux8SetFan = TURBFAN;
							//guaux8RT_TxdNUM = 2;
							guaux8RmtBuf[4] = 0x20;
							guaux8RmtBuf[5] = 0x40;
							break;

						case 0x06:
							if((guaux8RmtBuf[5] & 0xC0) EQU 0x40)
							{
								guaux8RmtBuf[4] = 0x20;
								guaux8SetFan = TURBFAN;
							}
							else if((guaux8RmtBuf[5] & 0xC0) EQU 0x80)
							{
								guaux8RmtBuf[4] = 0x60;
								guaux8RmtBuf[5] = 0x00;
								guaux8SetFan = LOWFAN;
							}
							else if(guaux8RmtBuf[4] EQU 0x60)
							{
								guaux8RmtBuf[4] = 0x40;
								guaux8SetFan = MIDFAN;
							}
							else if(guaux8RmtBuf[4] EQU 0x40)
							{
								guaux8RmtBuf[4] = 0x20;
								guaux8SetFan = HIGHFAN;
							}
							else if(guaux8RmtBuf[4] EQU 0x20)
							{
								guaux8RmtBuf[5] = 0x40;
								guaux8SetFan = TURBFAN;
							}
							break;

						case 0x07:
							if((guaux8RmtBuf[5] & 0xC0) EQU 0x80)
							{
								guaux8RmtBuf[4] = 0x60;
								guaux8SetFan = TINYFAN;
							}
							else if((guaux8RmtBuf[5] & 0xC0) EQU 0x40)
							{
								guaux8RmtBuf[4] = 0x20;
								guaux8RmtBuf[5] = 0x00;
								guaux8SetFan = HIGHFAN;
							}
							else if(guaux8RmtBuf[4] EQU 0x20)
							{
								guaux8RmtBuf[4] = 0x40;
								guaux8SetFan = MIDFAN;
							}
							else if(guaux8RmtBuf[4] EQU 0x40)
							{
								guaux8RmtBuf[4] = 0x60;
								guaux8SetFan = LOWFAN;
							}
							else if(guaux8RmtBuf[4] EQU 0x60)
							{
								guaux8RmtBuf[5] = 0x80;
								guaux8SetFan = TINYFAN;
							}
							break;

						case 0x08:
							guaux8SetFan = TURBFAN;
							guaux8RmtBuf[5] = 0x40;
							guaux8RmtBuf[4] = 0x20;
							break;

						case 0x09:
							guaux8SetFan = TINYFAN;
							guaux8RmtBuf[5] = 0x80;
							guaux8RmtBuf[4] = 0x60;
							break;

						default:
							guaux8SetFan = LOWFAN;
							guaux8RmtBuf[4] = 0x60;
							guaux8RmtBuf[5] = 0x00;
							break;
					}
				}
				if((guaux8RmtBuf[4] & 0xE0) EQU 0)
				{
					guaux8RmtBuf[4] = 0xA0;
					guaux8SetFan = AUTOFAN;
				}

				if(guaux8RmtBuf[6] EQU 0x00)
				{
					guaux8SetTem_Auto = guaux8SetTemp;
					g_ataux_air_status_bsp.ack_temp=guaux8SetTem_Auto+8;
					guaux8FanMemory_Auto = guaux8RmtBuf[4];
					guaux8TurboTiny_Auto = guaux8RmtBuf[5];
				}
				else if(guaux8RmtBuf[6] EQU 0x20)
				{
					guaux8SetTem_Cool = guaux8SetTemp;
					g_ataux_air_status_bsp.ack_temp=guaux8SetTem_Cool+8;
					guaux8FanMemory_Cool = guaux8RmtBuf[4];
					guaux8TurboTiny_Cool = guaux8RmtBuf[5];
				}
				else if(guaux8RmtBuf[6] EQU 0x40)
				{
					guaux8SetTem_Dry = guaux8SetTemp;
					g_ataux_air_status_bsp.ack_temp=guaux8SetTem_Dry+8;
					guaux8FanMemory_Dry = guaux8RmtBuf[4];
					guaux8TurboTiny_Dry = guaux8RmtBuf[5];
				}
				else if(guaux8RmtBuf[6] EQU 0xC0)
				{
					guaux8SetTem_Fan = guaux8SetTemp;
					g_ataux_air_status_bsp.ack_temp=guaux8SetTem_Fan+8;
					guaux8FanMemory_Fan = guaux8RmtBuf[4];
					guaux8TurboTiny_Fan = guaux8RmtBuf[5];
				}
				else if(guaux8RmtBuf[6] EQU 0x80)
				{
					guaux8SetTem_Heat = guaux8SetTemp;
					g_ataux_air_status_bsp.ack_temp=guaux8SetTem_Heat+8;
					guaux8FanMemory_Heat = guaux8RmtBuf[4];
					guaux8TurboTiny_Heat = guaux8RmtBuf[5];
				}

				//���½������������
				if((guaux8RmtBuf[6] EQU 0x00) OR (guaux8RmtBuf[6] EQU 0xC0))
				{
					guaux8RmtBuf[1] &= 0x07;	//�Զ����ͷ�ģʽ���¶Ȳ��ɵ���������������⣬���򲻿ɵ��Ļ��ͻ��޷�����
				}
				else
				{
					i = guaux8RmtBuf[1] & 0x07;
					guaux8RmtBuf[1] = i | (guaux8SetTemp<<3);
				}

				if(guaux8RmtBuf[6] EQU 0x40)
				{
					guaux8RmtBuf[4] = 0xA0;	//��ʪģʽ�°��Զ��紦��
					guaux8RmtBuf[5] = 0x00;
				}
			
			guaux8DisOn = 0;
			if(guaux8DispRxdBuf[1] EQU 0x08)
				{
					if(guaux8DispRxdBuf[2] EQU 0x01)
					{
						guaux8RmtBuf[11] = 0x15;
					}
					else if(guaux8DispRxdBuf[2] EQU 0x02)
					{
						guaux8RmtBuf[11] = 0x15;
					}
					guaux8SetDisp_onOff = !guaux8SetDisp_onOff;
					guaux8DisOn = guaux8RmtBuf[11];	//��¼��ʾ״̬�����������������ʱ��ʾ�Ὺ���л�
				}

			if(!guaux8DisOn)	//��һ�η���ʱ���㣬���������������ʱ��ʾ���ڿ��͹ؼ䲻�л�
				{
					guaux8RmtBuf[11] = 0x00;
				}
			for (k = 0;k < (RMTCNT-1);k++)
			{
				u8VerifySum += guaux8RmtBuf[k];
			}
			guaux8RmtBuf[12] = u8VerifySum;
			fgRF_Txd_Data = 1;
		}
}

/*************************************************************
 ������aux_remote_irsend
 ���룺��
 �������?
 ���ܣ�ң�ط������ݴ���
 �÷���main��ÿ5ms����
*************************************************************/
void aux_remote_irsend(void)
{
	unsigned char i,j,k,m;

	//RB_DEBUG_PRINT(" fgRF_Txd_Data = %d, fgRF_Txd_ready = %d\n",fgRF_Txd_Data,fgRF_Txd_ready);
	//RB_DEBUG_PRINT(" fgRmtStart = %d, guaux16RmtLen = %d\n",fgRmtStart,guaux16RmtLen);


	//if((fgRF_Txd_Data) AND (guaux8RT_TxdNUM))
	if(fgRF_Txd_Data)
	{
		fgRF_Txd_Data = 0;

		//guaux8RT_TxdNUM = 0;
		guaux16RmtLen = 0;
		fgRmtStart = 0;
		fgRmtDataEndBit = 0;
		fgRmtDataDivide = 0;
		guaux8RmtCnt = 0;
		guaux8RmtBit = 0;

		fgRF_Txd_ready = 1;
	}
	//user_timer_resume(1);
	//ir_tx_pin_high();

	//RB_DEBUG_PRINT(" fgRF_Txd_Data = %d, fgRF_Txd_ready = %d\n",fgRF_Txd_Data,fgRF_Txd_ready);
}


/*============================================================================================================*/
void aux_get_state(pAIRCOND_STATE st)
{
	RB_DEBUG_PRINT(">>>\n");
	//st->device = 3;
	st->onoff = guaux8RmtBuf[9]? 1:0;
	st->mode = guaux8SetMode;
	st->settemp = guaux8SetTemp + 8;
	if(guaux8SetFan > 0)
		st->fanspeed = guaux8SetFan - 1;
	else
		st->fanspeed = 0;

	if( guaux8SetMode == AUTOMODE)
		st->fanspeed = AUTOFAN;


	st->fanmode =(fgUDSwing << 4) | ( fgLRSwing );

	st->light_onoff = !!guaux8SetDisp_onOff;
}

/*============================================================================================================*/
void aux_ir_init(void)
{
	auxFlag0.byte = 0;
	auxFlag1.byte = 0;
	auxFlag2.byte = 0;

	guaux8Time2ms_10us = 0;
	guaux16Time5ms_10us = 0;
	guaux8LEDRunTime_Red = 50;

	guaux8RmtBuf[6] = 0x20;		//Ĭ�Ϸ�����ģʽ�������Զ�ģʽ���޷����¶ȵĻ���
	guaux8SetMode_Memory = guaux8RmtBuf[6];

	guaux8SetFan = LOWFAN;
	guaux8SetMode = AUTOMODE;
	guaux8SetDisp_onOff = 1;

	fgInitFlag = 1;
}


/*============================================================================================================*/

//Aux send ir data
int Aux_RemoteSend(unsigned char *data){
	unsigned char i = 0;
	if( (get_suport_devices() & INTERNAL_RC_AUX) == 0 )
		return -1;

	if(fgInitFlag == 0)
		aux_ir_init();

	aux_encode_irdata(data);
	printf(" Aux...start ... \n");

	aux_remote_irsend();

	printf(" Aux...end \n");

	i = 0;
  	while( (fgRF_Txd_ready == 1) && (i < 100) )
	{
		i++;
		uni_usleep(50);
	}
	//RB_PRINT(" fgRF_Txd_ready = %d \n",fgRF_Txd_ready);
	uni_msleep(15);

	return 1;
}

/*============================================================================================================*/
