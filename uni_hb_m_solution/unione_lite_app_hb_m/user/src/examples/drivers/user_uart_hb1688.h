/**************************************************************************
 * Copyright (C) 2017-2017  Unisound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **************************************************************************
 *
 * Description : user_uart_hb1688.h
 * Author      : wufangfang@unisound.com
 * Date        : 2020.04.10
 *
 **************************************************************************/
#ifndef USER_INC_USER_UART_HB1688_H_
#define USER_INC_USER_UART_HB1688_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "unione.h"

#define HB1688_LRC_LEN      208

typedef void (*_lrc_learn_recv)(uni_bool is_ok, uni_u8 *buf, uni_u8 len);

typedef void (*_version_recv)(uni_bool is_ok, uni_u16 version);

int user_uart_hb1688_init(void);

int user_uart_hb1688_final(void);

int user_uart_hb1688_get_version(_version_recv recv_cb);

int user_uart_hb1688_arc_learn(_lrc_learn_recv recv_cb);

int user_uart_hb1688_urc_learn(_lrc_learn_recv recv_cb);

int user_uart_hb1688_learn_exit(void);

int user_uart_hb1688_arc_send(uni_u8 *buf, uni_u8 len);

int user_uart_hb1688_urc_send(uni_u8 *buf, uni_u8 len);

int user_uart_hb1688_send_exit(void);

#ifdef __cplusplus
}
#endif
#endif

