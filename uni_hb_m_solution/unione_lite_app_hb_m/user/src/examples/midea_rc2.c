#include "midea_rc2.h"
#include "midea2_MacroDefine.h"
#include "unione.h"

/*============================================================================================================*/


volatile SegByte midea2Flag0;
volatile SegByte midea2Flag1;
volatile SegByte midea2Flag2;

#define fgDispTxdata00			midea2Flag0.bit.b7
#define fgTime5msSft			midea2Flag0.bit.b6
#define fgShrinkRun				midea2Flag0.bit.b5
#define fgRmtStart				midea2Flag0.bit.b4
#define fgRF_Txd_Data			midea2Flag0.bit.b3
#define fgRmtDataEndBit			midea2Flag0.bit.b2
#define fgRmtDataDivide			midea2Flag0.bit.b1
#define fgOn	 				midea2Flag0.bit.b0


#define fgLRSwing		 	midea2Flag1.bit.b7
#define fgDispRxding		midea2Flag1.bit.b6
#define fgDispRxd	 		midea2Flag1.bit.b5
#define fgDispRxdChk 		midea2Flag1.bit.b4
#define fgDispCommErr 		midea2Flag1.bit.b3
#define fgUDSwing 			midea2Flag1.bit.b2
#define fgRF_Txd_ready 		midea2Flag1.bit.b1
#define fgVoiceCommErr	 	midea2Flag1.bit.b0

#define fgLight	 		midea2Flag2.bit.b7
#define fgInitFlag			midea2Flag2.bit.b2
#define fgCtrol 			midea2Flag2.bit.b1
#define fgWakeUp	 		midea2Flag2.bit.b0

volatile uchar gumidea28DispTxd_2ms;
volatile uchar gumidea28DispRxdDly_2ms;
volatile uchar gumidea28DispComm_1s;
volatile uchar gumidea28DispRxdCnt;
volatile uchar gumidea28DispTxdCnt;
volatile uchar gumidea28DispTxdCode;
volatile uchar gumidea28DispRxdBuf[DISPRXDCNT];
volatile uchar gumidea28DispRxdTmpBuf[DISPRXDCNT];
volatile uchar gumidea28DispTxdBuf[DISPTXDCNT];


volatile uchar gumidea28SetMode;
volatile uchar gumidea28SetFan;
volatile uchar gumidea28SetTemp;
volatile uchar gumidea28RunMode;


volatile uchar gumidea28Time1ms_200us;

volatile uchar gumidea28Time2ms_10us;

volatile uint gumidea216Time5ms_10us;
volatile uchar gumidea28Time100ms_5ms;
volatile uchar gumidea28Time1s_5ms;
volatile uchar gumidea28Time50ms_200us;
volatile uchar gumidea28Time1s_50ms;

volatile uint gumidea216RmtLen;
volatile uchar gumidea28RmtBit;
volatile uchar gumidea28RmtCnt;
volatile uchar gumidea28RmtDly_1s;

volatile uchar gumidea28TimeShrink1s_5ms;

volatile uchar gumidea28RmtBuf[RMTCNT];
volatile uchar gumidea28RmDataTxd;
volatile uchar gumidea28RmDataBitLen;

volatile uchar gumidea28RT_TxdNUM;

volatile uchar gumidea28RF_TempB;
volatile uchar gumidea28RF_TempC;

volatile uchar gumidea28LEDRunTime_Red;
volatile uchar gumidea28Disp_OnOff;
//volatile uchar gumidea28LEDRunTime_Blu;

volatile uchar gumidea28VoiceRstTime_Init;
volatile uchar gumidea28VoiceRstTime_Run;
volatile uchar gumidea28VoiceErrorTime;

volatile uchar gumidea28RT_TxdJIANRONG;
volatile uchar gumidea28RF_StopTime;
//volatile uchar gumidea28RT_TxdJIANRONG_3;


static const uchar gumidea28RmtSetTemp[] =
{
	0,
	1,
	3,
	2,
	6,
	7,
	5,
	4,
	12,
	13,
	9,
	8,
	10,
	11
};

/*============================================================================================================*/
void midea2_remote_isr(void)
{
	//gumidea28Time2ms_10us++;
	//gumidea216Time5ms_10us++;

	if(fgRF_Txd_ready)
	{
		if(gumidea216RmtLen<0xffff)
		{
			gumidea216RmtLen++;
		}

		if (fgRmtStart)
		{
			if (!fgRmtDataEndBit)
			{
				if(!gumidea28RmtBit)
				{
					gumidea28RmDataTxd = gumidea28RmtBuf[gumidea28RmtCnt];
				}

				if(gumidea28RmDataTxd & 0x80)
				{
					gumidea28RmDataBitLen = (((54+162)*10)/TIMER2_PERIOD);//54+162;
				}
				else
				{
					gumidea28RmDataBitLen = (((64+54)*10)/TIMER2_PERIOD);//54+54;
				}
#if 0
				if(gumidea216RmtLen EQU 1)
				{
//					TMR01H = 0x80;
//					TDR01L = 32;
//					TOE0 |= 0x02;
//					TS0 |= 0x02;
					ir_tx_pin_low();
				}
				else if(gumidea216RmtLen <54)
				{
					;
				}
				else if((gumidea216RmtLen <= 54))
				{
//					TT0 |= 0x02;
//					TOE0 &= 0xFD;
//					TO0 &= 0xFD;
					ir_tx_pin_high();
				}
				else if(gumidea216RmtLen > gumidea28RmDataBitLen)
				{
					gumidea216RmtLen = 0;
					gumidea28RmDataTxd <<= 1;
					gumidea28RmtBit++;
					if (gumidea28RmtBit >= 8)
					{
						gumidea28RmtBit = 0;
						gumidea28RmtCnt++;
						if((gumidea28RmtCnt EQU 6) OR (gumidea28RmtCnt EQU 12))
						{
							fgRmtDataEndBit = 1;
							ir_tx_pin_high();
						}
					}
				}
#else
				if(gumidea216RmtLen <= ((10*10)/TIMER2_PERIOD) /*1*/)
				{
				}
				if(gumidea216RmtLen < ((64*10)/TIMER2_PERIOD))
				{
//					TMR01H = 0x80;
//					TDR01L = 32;
//					TOE0 |= 0x02;
//					TS0 |= 0x02;
					ir_tx_pin_low();
				}
				else if((gumidea216RmtLen <= gumidea28RmDataBitLen))
				{
//					TT0 |= 0x02;
//					TOE0 &= 0xFD;
//					TO0 &= 0xFD;
					ir_tx_pin_high();
				}
				else
				{
					gumidea216RmtLen = 0;
					gumidea28RmDataTxd <<= 1;
					gumidea28RmtBit++;
					if (gumidea28RmtBit >= 8)
					{
						gumidea28RmtBit = 0;
						gumidea28RmtCnt++;
						if((gumidea28RmtCnt EQU 6) OR (gumidea28RmtCnt EQU 12))
						{
							fgRmtDataEndBit = 1;
							ir_tx_pin_high();
						}
					}
				}

#endif
			}
			else
			{
				if(!fgRmtDataDivide)
				{
#if 0
					if(gumidea216RmtLen EQU 1)
					{
//						TMR01H = 0x80;
//						TDR01L = 32;
//						TOE0 |= 0x02;
//						TS0 |= 0x02;
						ir_tx_pin_low();
					}
					else if(gumidea216RmtLen <=54)
					{
						;
					}
					else
					{
//						TT0 |= 0x02;
//						TOE0 &= 0xFD;
//						TO0 &= 0xFD;
						ir_tx_pin_high();

						gumidea216RmtLen = 0;

						if (gumidea28RmtCnt >= 12)
						{
							fgRF_Txd_ready = 0;
							fgRmtStart = 0;
							fgRmtDataEndBit = 0;
							fgRmtDataDivide = 0;
							gumidea28RmtCnt = 0;
							gumidea28RmtBit = 0;

							gumidea216RmtLen = 0;
						}
						else
						{
							fgRmtDataDivide = 1;
						}
					}
#else
					if(gumidea216RmtLen <= ((10*10)/TIMER2_PERIOD) /*1*/)
					{
					}
					else if(gumidea216RmtLen <= ((64*10)/TIMER2_PERIOD))
					{
//						TMR01H = 0x80;
//						TDR01L = 32;
//						TOE0 |= 0x02;
//						TS0 |= 0x02;
						ir_tx_pin_low();
					}
					else
					{
//						TT0 |= 0x02;
//						TOE0 &= 0xFD;
//						TO0 &= 0xFD;
						ir_tx_pin_high();

						gumidea216RmtLen = 0;

						if (gumidea28RmtCnt >= 12)
						{
							fgRF_Txd_ready = 0;
							fgRmtStart = 0;
							fgRmtDataEndBit = 0;
							fgRmtDataDivide = 0;
							gumidea28RmtCnt = 0;
							gumidea28RmtBit = 0;

							gumidea216RmtLen = 0;
						}
						else
						{
							fgRmtDataDivide = 1;
						}
					}

#endif
				}
				else
				{
					if(gumidea216RmtLen <= ((100*10)/TIMER2_PERIOD) /*1*/)
					{
					}
					else if(gumidea216RmtLen <= ((422*10)/TIMER2_PERIOD) /*522*/)
					{
//						TT0 |= 0x02;
//						TOE0 &= 0xFD;
//						TO0 &= 0xFD;
						ir_tx_pin_high();
					}
					else
					{
						gumidea216RmtLen = 0;
						fgRmtDataDivide = 0;
						fgRmtDataEndBit = 0;

						fgRmtStart = 0;
					}
				}
			}
		}
		else
		{
#if 0
			if(gumidea216RmtLen EQU 1)
			{
//				TMR01H = 0x80;
//				TDR01L = 32;
//				TOE0 |= 0x02;
//				TS0 |= 0x02;
				ir_tx_pin_low();
			}
			else if(gumidea216RmtLen <=440)
			{
				;
			}
			else if((gumidea216RmtLen >440) AND (gumidea216RmtLen <=880))
			{
//				TT0 |= 0x02;
//				TOE0 &= 0xFD;
//				TO0 &= 0xFD;
				ir_tx_pin_high();
			}
			else if(gumidea216RmtLen >880)
			{
				gumidea216RmtLen = 0;
				fgRmtStart = 1;
			}
		}
#else
			if(gumidea216RmtLen <= ((100*10)/TIMER2_PERIOD) /*1*/)
				{
				}
			else if(gumidea216RmtLen <= ((540*10)/TIMER2_PERIOD) /*440*/)
			{
//				TMR01H = 0x80;
//				TDR01L = 32;
//				TOE0 |= 0x02;
//				TS0 |= 0x02;
				ir_tx_pin_low();
			}
			else if(gumidea216RmtLen <= ((980*10)/TIMER2_PERIOD)/*880*/)
			{
//				TT0 |= 0x02;
//				TOE0 &= 0xFD;
//				TO0 &= 0xFD;
				ir_tx_pin_high();
			}
			else
			{
				gumidea216RmtLen = 0;
				fgRmtStart = 1;
			}
		}

#endif
	}
#if 0
	else
	{
		gumidea216RmtLen = 0;
		fgRmtStart = 0;
		fgRmtDataEndBit = 0;
		fgRmtDataDivide = 0;
	}
#endif
}



/*************************************************************
 ������midea2_encode_irdata
 ���룺
 �����
 ���ܣ�
 �÷���
*************************************************************/
void midea2_encode_irdata(unsigned char* data)
{
	uchar	u8Tmp = 0;

		gumidea28DispTxd_2ms = 0;
		fgDispTxdata00 = 1;
		gumidea28VoiceErrorTime = 0;
//		for (u8Tmp = 0;u8Tmp < DISPRXDCNT;u8Tmp++)
//		{
//			data[u8Tmp] = gumidea28DispRxdTmpBuf[u8Tmp];
//		}
		if((data[1] EQU 0x00) OR (data[1] EQU 0x01))
		{
			;
//			if(data[1] EQU 0x01)
//			{
//				if((data[2] EQU 0x00) AND (data[3] EQU 0x01))
//				{
//					//gumidea28LEDRunTime_Blu = 100;
//					fgWakeUp = 1;
//				}
//				else
//				{
//					fgWakeUp = 0;
//				}
//			}
		}
		else
		{
			fgRF_Txd_Data = 1;
			gumidea28LEDRunTime_Red = 0;
			//gumidea28LEDRunTime_Blu = 110;

			fgCtrol = 1;
			gumidea28Disp_OnOff = 0;

			gumidea28RT_TxdNUM = 5;
			if(data[1] EQU 0x02)
			{
				if(data[2] EQU 0x01)
				{
					gumidea28RT_TxdNUM = 5;
					fgOn = 1;
				}
				else if(data[2] EQU 0x02)
				{
					gumidea28RT_TxdNUM = 1;
					fgOn = 0;
				}
			}
			else
			{
				if(!fgOn)
				{
					gumidea28RT_TxdNUM = 1;	//�ػ�״̬�³��ֿ���������ͳһ���ػ�������ͻ�������¿���
				}
				else
				{
					if(data[1] EQU 0x03)
					{

							if(data[2] EQU 0x01)
							{
								if(gumidea28SetTemp >= 30)
								{
									gumidea28SetTemp = 30;
								}
								else
								{
									gumidea28SetTemp++;
								}
							}
							else if(data[2] EQU 0x02)
							{
								if(gumidea28SetTemp <= 16)
								{
									gumidea28SetTemp = 16;
								}
								else
								{
									gumidea28SetTemp--;
								}
							}

					}
					else if(data[1] EQU 0x04)
					{

							gumidea28SetTemp = data[2] + 15;
							if(gumidea28SetTemp<=16)
							{
								gumidea28SetTemp = 16;
							}
							if(gumidea28SetTemp>=30)
							{
								gumidea28SetTemp = 30;
							}

					}
					else if(data[1] EQU 0x05)
					{

							switch (data[2])
							{
								case 0x01:
									gumidea28SetMode = AUTOMODE;
									break;
								case 0x02:
									gumidea28SetMode = COOLMODE;
									break;
								case 0x03:
									gumidea28SetMode = DRYMODE;
									break;
								case 0x04:
									gumidea28SetMode = FANMODE;
									break;
								case 0x05:
									gumidea28SetMode = HEATMODE;
									break;
								default:
									//gumidea28SetMode = NOMODE;
									gumidea28SetMode = AUTOMODE;
									break;
							}
					}
					else if(data[1] EQU 0x06)
					{

							switch (data[2])
							{
								case 0x01:
									if((gumidea28SetMode EQU COOLMODE) OR (gumidea28SetMode EQU HEATMODE) OR (gumidea28SetMode EQU FANMODE))	//���䡢���ȡ��ͷ��²��о���
									{
										gumidea28SetFan = TINYFAN;
									}
									break;

								case 0x02:
									gumidea28SetFan = LOWFAN;
									break;

								case 0x03:
									gumidea28SetFan = MIDFAN;
									break;

								case 0x04:
									gumidea28SetFan = HIGHFAN;
									break;

								case 0x05:
									if((gumidea28SetMode EQU COOLMODE) OR (gumidea28SetMode EQU HEATMODE))	//���䡢�����²���ǿ��
									{
										gumidea28SetFan = TURBFAN;
										gumidea28RT_TxdNUM = 2;
									}
									else if(gumidea28SetMode EQU FANMODE)
									{
										gumidea28SetFan = HIGHFAN;
									}
									break;

								case 0x06:
									if(gumidea28SetFan>=TURBFAN)
									{
										if((gumidea28SetMode EQU COOLMODE) OR (gumidea28SetMode EQU HEATMODE))	//���䡢�����²���ǿ��
										{
											gumidea28SetFan = TURBFAN;
											gumidea28RT_TxdNUM = 2;
										}
										else if(gumidea28SetMode EQU FANMODE)
										{
											gumidea28SetFan = HIGHFAN;
										}
									}
									else
									{
										gumidea28SetFan++;
									}
									break;

								case 0x07:
									if(gumidea28SetFan<=TINYFAN)
									{
										if((gumidea28SetMode EQU COOLMODE) OR (gumidea28SetMode EQU HEATMODE) OR (gumidea28SetMode EQU FANMODE))	//���䡢���ȡ��ͷ��²��о���
										{
											gumidea28SetFan = TINYFAN;
										}
									}
									else
									{
										gumidea28SetFan--;
									}
									break;

								case 0x08:
									if((gumidea28SetMode EQU COOLMODE) OR (gumidea28SetMode EQU HEATMODE))	//���䡢�����²���ǿ��
									{
										gumidea28SetFan = TURBFAN;
										gumidea28RT_TxdNUM = 2;
									}
									else if(gumidea28SetMode EQU FANMODE)
									{
										gumidea28SetFan = HIGHFAN;
									}
									break;

								case 0x09:
									if((gumidea28SetMode EQU COOLMODE) OR (gumidea28SetMode EQU HEATMODE) OR (gumidea28SetMode EQU FANMODE))	//���䡢���ȡ��ͷ��²��о���
									{
										gumidea28SetFan = TINYFAN;
									}
									break;

								default:
									gumidea28SetFan = LOWFAN;
									break;
							}
					}
					else if(data[1] EQU 0x07)
					{
						{
							if(data[2] EQU 0x01)
							{
								fgLRSwing = 1;
								gumidea28RT_TxdNUM = 3;
							}
							else if(data[2] EQU 0x02)
							{
								fgLRSwing = 0;
								gumidea28RT_TxdNUM = 3;
							}
							else if(data[2] EQU 0x03)
							{
								fgUDSwing = 1;
								gumidea28RT_TxdNUM = 4;
							}
							else if(data[2] EQU 0x04)
							{
								fgUDSwing = 0;
								gumidea28RT_TxdNUM = 4;
							}
						}
					}
					else if(data[1] EQU 0x08)
					{
						if((data[2] EQU 0x01) OR (data[2] EQU 0x02))
						{
							gumidea28RT_TxdNUM = 6;
						}
						if((data[2] EQU 0x01))
							fgLight = 1;// On
						else
							fgLight = 0;
					}

					//���½������������?
					if((gumidea28SetMode EQU AUTOMODE) OR (gumidea28SetMode EQU DRYMODE))
					{
						gumidea28SetFan = NOFAN;	//�Զ�����ʪģʽ�£�ң�ط��밴000���̶���
					}
				}
			}

			if(!gumidea28SetMode)
			{
				//gumidea28SetMode = AUTOMODE;
				gumidea28SetMode = COOLMODE;
			}
			if(!gumidea28SetTemp)
			{
				//gumidea28SetTemp = 23;
				gumidea28SetTemp = 26;
			}
			if(!gumidea28SetFan)
			{
				if((gumidea28SetMode EQU AUTOMODE) OR (gumidea28SetMode EQU DRYMODE))
				{
					;
				}
				else
				{
					gumidea28SetFan = AUTOFAN;
				}
			}
		}
}

/*************************************************************
 ������midea2_RmtBufData
 ���룺��
 �������?
 ���ܣ�ң�ط���������ֵ
 �÷���subRemoteDecode�е���
*************************************************************/
void midea2_RmtBufData(uchar TempA,uchar TempB,uchar TempC)
{

	gumidea28RmtBuf[0] = TempA;
	gumidea28RmtBuf[1] = (~gumidea28RmtBuf[0]);

	gumidea28RmtBuf[6] = gumidea28RmtBuf[0];
	gumidea28RmtBuf[7] = gumidea28RmtBuf[1];

	gumidea28RmtBuf[2] = TempB;
	gumidea28RmtBuf[3] = (~gumidea28RmtBuf[2]);

	gumidea28RmtBuf[8] = gumidea28RmtBuf[2];
	gumidea28RmtBuf[9] = gumidea28RmtBuf[3];

	gumidea28RmtBuf[4] = TempC;
	gumidea28RmtBuf[5] = (~gumidea28RmtBuf[4]);

	gumidea28RmtBuf[10] = gumidea28RmtBuf[4];
	gumidea28RmtBuf[11] = gumidea28RmtBuf[5];
}

/*************************************************************
 ������subRemoteDecode
 ���룺��
 �������?
 ���ܣ�ң�ط������ݴ���
 �÷���main��ÿ5ms����
*************************************************************/
void midea2_RemoteDecode(void)
{
	uchar i,j,k,m;

	if((fgRF_Txd_Data) AND (gumidea28RT_TxdNUM))
	{
		fgRF_Txd_Data = 0;

		if(gumidea28RT_TxdNUM EQU 1)
		{
			midea2_RmtBufData(0xB2,0x7B,0xE0);
		}
		else if((gumidea28RT_TxdNUM EQU 2)
			OR (gumidea28RT_TxdNUM EQU 3)
			OR (gumidea28RT_TxdNUM EQU 6))
		{
			if(gumidea28RT_TxdNUM EQU 2)
			{
				midea2_RmtBufData(0xB5,0xF5,0xA2);
				gumidea28RT_TxdJIANRONG = 2;
			}
			else if(gumidea28RT_TxdNUM EQU 3)
			{
				midea2_RmtBufData(0xB5,0xF5,0xA7);
				//midea2_RmtBufData(0xB9,0xF5,0x08);
				gumidea28RT_TxdJIANRONG = 3;
			}
			else
			{
				midea2_RmtBufData(0xB5,0xF5,0xA5);
				//midea2_RmtBufData(0xB9,0xF5,0x09);
				gumidea28RT_TxdJIANRONG = 6;
			}
		}
		else if(gumidea28RT_TxdNUM EQU 4)
		{
			midea2_RmtBufData(0xB2,0x6B,0xE0);
			gumidea28RT_TxdJIANRONG = 4;
		}
		else if(gumidea28RT_TxdNUM EQU 5)
		{
			gumidea28RmtBuf[0] = 0xB2;
			gumidea28RmtBuf[1] = (~gumidea28RmtBuf[0]);

			gumidea28RmtBuf[6] = gumidea28RmtBuf[0];
			gumidea28RmtBuf[7] = gumidea28RmtBuf[1];

			i = 0;
			switch (gumidea28SetFan)
			{
				case AUTOFAN:
					i = 0xA0;
					break;

				case TINYFAN:
				case LOWFAN:
					i = 0x80;
					break;

				case MIDFAN:
					i = 0x40;
					break;

				case HIGHFAN:
					i = 0x20;
					break;

				case NOFAN:		//�Զ�����ʪģʽ�£�ң�ط��밴000���̶���
					i = 0x00;
					break;

				default:
					break;
			}
			gumidea28RF_TempB = i | 0x1f;

			i = 0;
			j = 0;
			k = 0;
			m = 0;
			switch (gumidea28SetMode)
			{
				case AUTOMODE:
					j = 0x08;
					break;
				case COOLMODE:
					j = 0x00;
					break;
				case DRYMODE:
					j = 0x04;
					break;
				case FANMODE:
					j = 0x04;
					break;
				case HEATMODE:
					j = 0x0c;
					break;
				default:
					break;
			}

			if(gumidea28SetTemp>=17)
			{
				i = gumidea28SetTemp - 17;
			}
			else
			{
				i = 0;
			}

			if(gumidea28SetMode EQU FANMODE)
			{
				k = 0x0E;
			}
			else
			{
				k = gumidea28RmtSetTemp[i];
			}
			m = k<<4;
			gumidea28RF_TempC = j | m;

			midea2_RmtBufData(0xB2,gumidea28RF_TempB,gumidea28RF_TempC);
		}
		gumidea28RT_TxdNUM = 0;
		fgRF_Txd_ready = 1;
	}
	else
	{
		if(!fgRF_Txd_ready)	//���½�����������⣬�൱�ڷ���������?
		{
			gumidea28RF_StopTime++;
			if(gumidea28RF_StopTime>=60)	//60*5ms	���?00ms�ٷ��͵ڶ�������
			{
				gumidea28RF_StopTime = 0;
				if(gumidea28RT_TxdJIANRONG EQU 6)
				{
					midea2_RmtBufData(0xB9,0xF5,0x09);
					fgRF_Txd_ready = 1;
					gumidea28RT_TxdJIANRONG = 0;
				}
				else if(gumidea28RT_TxdJIANRONG EQU 3)
				{
					if(fgLRSwing)
					{
						midea2_RmtBufData(0xB9,0xF5,0x07);
					}
					else
					{
						midea2_RmtBufData(0xB9,0xF5,0x08);
					}
					fgRF_Txd_ready = 1;
					gumidea28RT_TxdJIANRONG = 0;
				}
				else if(gumidea28RT_TxdJIANRONG EQU 4)
				{
					if(fgUDSwing)
					{
						midea2_RmtBufData(0xB9,0xF5,0x04);
					}
					else
					{
						midea2_RmtBufData(0xB9,0xF5,0x05);
					}
					fgRF_Txd_ready = 1;
					gumidea28RT_TxdJIANRONG = 0;
				}
				else if(gumidea28RT_TxdJIANRONG EQU 2)
				{
					midea2_RmtBufData(0xB9,0xF5,0x01);
					fgRF_Txd_ready = 1;
					gumidea28RT_TxdJIANRONG = 0;
				}
				else
				{
					gumidea28RT_TxdJIANRONG = 0;
				}
			}
		}
		else
		{
			gumidea28RF_StopTime = 0;
		}
	}
}


/*************************************************************
 ������midea2_remote_irsend
 ���룺��
 �������?
 ���ܣ�ң�ط������ݴ���
 �÷���main��ÿ5ms����
*************************************************************/
void midea2_remote_irsend(void)
{
	unsigned char i,j,k,m;

	//RB_DEBUG_PRINT(" fgRF_Txd_Data = %d, fgRF_Txd_ready = %d\n",fgRF_Txd_Data,fgRF_Txd_ready);

	midea2_RemoteDecode();

	//user_timer_resume(1);
	//ir_tx_pin_high();

	//RB_DEBUG_PRINT(" fgRF_Txd_Data = %d, fgRF_Txd_ready = %d\n",fgRF_Txd_Data,fgRF_Txd_ready);
}


/*============================================================================================================*/
void midea2_get_state(pAIRCOND_STATE st)
{
	RB_DEBUG_PRINT(">>>\n");
	
	if( fgInitFlag == 0 )
	{
		midea2_ir_init();
	}
		
	//st->device = 2;
	st->onoff = fgOn;
	RB_DEBUG_PRINT("st->onoff = %d >>>\n",st->onoff);
	
	// mode
	{
		if( AUTOMODE == gumidea28SetMode)
			st->mode = 0;
		else if( COOLMODE == gumidea28SetMode)
			st->mode = 1;
		else if( DRYMODE == gumidea28SetMode)
			st->mode = 2;
		else if( FANMODE == gumidea28SetMode)
			st->mode = 3;
		else if( HEATMODE == gumidea28SetMode)
			st->mode = 4;
		else
			st->mode = 0;
	}
	
	if( FANMODE != gumidea28SetMode)
	{
	// temp
	if( gumidea28SetTemp < 17 )
		st->settemp = 17;
	else if( gumidea28SetTemp > 30 )
		st->settemp = 30;	
	else
		st->settemp = gumidea28SetTemp;
	}
	
	// Fan
	{
		if(gumidea28SetFan > 0)
			st->fanspeed = (gumidea28SetFan - 1) | 0x10;
		else
			st->fanspeed = 0x10;//NOFAN
	
		if( AUTOMODE == gumidea28SetMode)
			st->fanspeed = 0x10;//NOFAN
		if( DRYMODE == gumidea28SetMode)
			st->fanspeed = 0x10;//NOFAN
	}

	st->fanmode = (fgUDSwing << 4) | ( fgLRSwing );

	st->light_onoff = fgLight;
}

/*============================================================================================================*/
void midea2_ir_init(void)
{
	midea2Flag0.byte = 0;
	midea2Flag1.byte = 0;
	midea2Flag2.byte = 0;

	gumidea28Time2ms_10us = 0;
	gumidea216Time5ms_10us = 0;
	gumidea28LEDRunTime_Red = 50;

	gumidea28SetTemp = 26;
	gumidea28SetMode = COOLMODE;
	gumidea28SetFan = AUTOFAN;

	fgLight = 0;
	fgInitFlag = 1;
}

/*============================================================================================================*/

//Haier send ir data
int Midea2_RemoteSend(unsigned char *data){
	unsigned char i = 0;
	//RB_DEBUG_PRINT("++\n");
	if( (get_suport_devices() & INTERNAL_RC_MIDEA2) == 0 )
		return -1;

	if(fgInitFlag == 0)
		midea2_ir_init();

	printf(" start ... \n");

	midea2_encode_irdata(data);
	midea2_remote_irsend();

	if( gumidea28RT_TxdJIANRONG != 0)
	{
		i = 0;
	  	//while( (fgRmtDataEndBit == 0) && (i < 100) )
	  	while( (fgRF_Txd_ready == 1) && (i < 100) )
		{
			i++;
			uni_msleep(1);
		}
		uni_msleep(24);
		printf(" 2... \n");
		//midea2_RemoteDecode();
		gumidea28RF_StopTime = 60;
		midea2_remote_irsend();
	}

	printf(" end \n");

	i = 0;
  //while( (fgRmtDataEndBit == 0) && (i < 100) )
  while( (fgRF_Txd_ready == 1) && (i < 100) )
	{
		i++;
		uni_msleep(1);
	}
	//RB_PRINT(" fgRF_Txd_ready = %d \n",fgRF_Txd_ready);
	uni_msleep(24);

	fgRF_Txd_ready = 0;
	fgRmtStart = 0;
	fgRmtDataEndBit = 0;
	fgRmtDataDivide = 0;
	gumidea28RmtCnt = 0;
	gumidea28RmtBit = 0;

	gumidea216RmtLen = 0;
	//RB_DEBUG_PRINT("--\n");
	return 1;
}

/*============================================================================================================*/
