
#ifndef	__ASR_ID_H__
#define __ASR_ID_H__

#define ASR_ID_NULL	  			(0)
#define ASR_ID_WAKEUP1  		(1)	//小贝同学
#define ASR_ID_WAKEUP2  		(2)	//格力空调
#define ASR_ID_WAKEUP3  		(3)	//小美小美
#define ASR_ID_WAKEUP4  		(4)	//你好小奥

#define ASR_ID_ON				(5)		//打开空调
#define ASR_ID_OFF				(6)		//关闭空调

#define ASR_ID_TEMP_INC			(7)		//升高温度
#define ASR_ID_TEMP_DEC			(8)		//降低温度
#define ASR_ID_TEMP_BASE  		(9)
#define ASR_ID_TEMP_16				(ASR_ID_TEMP_BASE+0)	//调到十六度
#define ASR_ID_TEMP_17	(ASR_ID_TEMP_BASE+1)	//调到十七度
#define ASR_ID_TEMP_18	(ASR_ID_TEMP_BASE+2)	//调到十八度
#define ASR_ID_TEMP_19	(ASR_ID_TEMP_BASE+3)	//调到十九度
#define ASR_ID_TEMP_20	(ASR_ID_TEMP_BASE+4)	//调到二十度
#define ASR_ID_TEMP_21	(ASR_ID_TEMP_BASE+5)	//调到二十一度
#define ASR_ID_TEMP_22	(ASR_ID_TEMP_BASE+6)	//调到二十二度
#define ASR_ID_TEMP_23	(ASR_ID_TEMP_BASE+7)	//调到二十三度
#define ASR_ID_TEMP_24	(ASR_ID_TEMP_BASE+8)	//调到二十四度
#define ASR_ID_TEMP_25	(ASR_ID_TEMP_BASE+9)	//调到二十五度
#define ASR_ID_TEMP_26	(ASR_ID_TEMP_BASE+10)	//调到二十六度
#define ASR_ID_TEMP_27	(ASR_ID_TEMP_BASE+11)	//调到二十七度
#define ASR_ID_TEMP_28	(ASR_ID_TEMP_BASE+12)	//调到二十八度
#define ASR_ID_TEMP_29	(ASR_ID_TEMP_BASE+13)	//调到二十九度
#define ASR_ID_TEMP_30	(ASR_ID_TEMP_BASE+14)	//调到三十度
#define ASR_ID_TEMP_31	(ASR_ID_TEMP_BASE+15)	//调到三十度
#define ASR_ID_TEMP_32	(ASR_ID_TEMP_BASE+16)	//调到三十度



#define ASR_ID_MODE_BASE  		(26)
#define ASR_ID_MODE_AUTOMODE	(ASR_ID_MODE_BASE+0)	//自动模式
#define ASR_ID_MODE_COOLMODE	(ASR_ID_MODE_BASE+1)	//制冷模式
#define ASR_ID_MODE_DRYMODE		(ASR_ID_MODE_BASE+2)	//除湿模式
#define ASR_ID_MODE_FANMODE		(ASR_ID_MODE_BASE+3)	//送风模式
#define ASR_ID_MODE_HEATMODE	(ASR_ID_MODE_BASE+4)	//加热模式

#define ASR_ID_FAN_BASE  		(31)
#define ASR_ID_FAN_TINYFAN			(ASR_ID_FAN_BASE+0x00)	//静音档
#define ASR_ID_FAN_LOWFAN			(ASR_ID_FAN_BASE+0x01)	//低风档
#define ASR_ID_FAN_MIDFAN			(ASR_ID_FAN_BASE+0x02)	//中风档
#define ASR_ID_FAN_HIGHFAN			(ASR_ID_FAN_BASE+0x03)	//高风档
#define ASR_ID_FAN_TURBFAN			(ASR_ID_FAN_BASE+0x04)	//强劲档
#define ASR_ID_FAN_INCFAN			(ASR_ID_FAN_BASE+0x05)	//增大风速
#define ASR_ID_FAN_DECFAN			(ASR_ID_FAN_BASE+0x06)	//减小风速
#define ASR_ID_FAN_MAXFAN			(ASR_ID_FAN_BASE+0x07)	//最大风
#define ASR_ID_FAN_MINFAN			(ASR_ID_FAN_BASE+0x08)	//最小风

#define ASR_ID_FANMOVE_BASE  	(40)
#define ASR_ID_FANMOVE_LR_ON					(ASR_ID_FANMOVE_BASE+0x00)	//打开左右
#define ASR_ID_FANMOVE_LR_OFF					(ASR_ID_FANMOVE_BASE+0x01)	//关闭左右
#define ASR_ID_FANMOVE_UP_ON					(ASR_ID_FANMOVE_BASE+0x02)	//打开上下
#define ASR_ID_FANMOVE_UP_OFF					(ASR_ID_FANMOVE_BASE+0x03)	//关闭上下

#define ASR_ID_LIGHT_BASE  		(44)
#define ASR_ID_LIGHT_ON					(ASR_ID_LIGHT_BASE+0x00)	//打开灯光
#define ASR_ID_LIGHT_OFF				(ASR_ID_LIGHT_BASE+0x01)	//关闭灯光
#define ASR_ID_LIGHT_ON_OFF				(ASR_ID_LIGHT_BASE+0x02)	


#define ASR_ID_ALARM_BASE  		(47)
#define ASR_ID_ALARM_OFF_60MIN		ASR_ID_ALARM_BASE+0
#define ASR_ID_ALARM_OFF_120MIN		ASR_ID_ALARM_BASE+1
#define ASR_ID_ALARM_OFF_240MIN		ASR_ID_ALARM_BASE+2
//#define ASR_ID_ALARM_OFF_480MIN		ASR_ID_ALARM_BASE+3

#define ASR_ID_CONFIG_WIFI		(50)		//进入配网模式
#define ASR_ID_RESET_DEFAULT	(51)		//恢复出厂设置
#define ASR_ID_SHOW_NETSTATUS	(68)

#define ASR_ID_RECOGNIZE_AIRCON (52)		//学习空调
#define ASR_ID_RECOGNIZE_FAN	(53)		//学习风扇
#define ASR_ID_RECOGNIZE_STB	(54)		//学习机顶盒
#define ASR_ID_RECOGNIZE_TV		(55)		//学习电视

#define ASR_ID_END_BASE  		(69)
#define ASR_ID_TIME_OUT  		(888)

#endif//__ASR_ID_H__
