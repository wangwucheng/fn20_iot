#include "protocolInterface.h"
#include "greebanlv.h"
#include "buzz.h"
#include "asr_id.h"
#include "iot_ctaux.h"
#include "ir_tx.h"
#include "user_uart.h"
#include "uni_iot.h"
#include "uni_hal_uart.h"
#include "uni_databuf.h"

void setnumber(int id);

#ifdef __IOT_CTAUX_USED__

#define MAX_RX_SIZE	(256)
#define MIN_RX_SIZE	(12)

unsigned char rxData[MAX_RX_SIZE*2];
static unsigned char *pr_index = rxData;
static unsigned char *pw_index = rxData;

static int  gwifistate = 0;
static int  giotinit_flag = 0;
static int  giotfactory_flag = 0;

#define SATE_REPORT_PERIOD_TIME			(600)
static uint32_t update_status_timeout = 0;

static ProtocolHandle ctaux_handle;
unsigned char gctaux_msgtype = PASSTH_CTL_DOWN;


ctaux_pro_path g_ctaux_protacol_sdata_path;
ctaux_pro g_ctaux_protacol_sdata;
//ctaux_pro_query g_ctaux_protacol_sdata_query_csum;


ctaux_air_status g_ataux_air_status={0x00,0x82,0x00,0x01,0x1a,0x02,0x01,0x01,0x01,0x02};

static unsigned char msgindex = 0;

unsigned char ctaux_mac[6] = {0,0,0,0,0,0};

unsigned char gsta_connect = 0;
unsigned char DeviceTYPEID = PROTOCOL_BB_TYPEID;

unsigned char g_csum1 = 0;
unsigned char g_csum2 = 0;


#ifndef DEBUG
#define DEBUG
#endif

/*============================================================================================================*/
const unsigned char RET_NULL[]={0x00};
const unsigned char RET_ERR[]={0xFB,0x00};
const unsigned char RET_OK[]={0xFB,0x01};
const unsigned char RET_ERR_NOTSUPORT[]={0xFB,0x02};
const unsigned char RET_ERR_CKS[]={0xFB,0x03};
const unsigned char RET_ERR_BUSY[]={0xFB,0x04};

const unsigned char RET_DEV_LIST[]={0xA3,\
								'D','e','f','a','u','l','t','\0',\
								'M','i','d','e','a','\0','\0','\0',\
								'G','r','e','e','\0','\0','\0','\0',\
								'A','u','x','\0','\0','\0','\0','\0',\
								'T','C','L','\0','\0','\0','\0','\0',\
								'H','a','i','e','r','\0','\0','\0',\
								'H','i','s','e','n','s','e','\0',\
								'C','h','i','g','o','\0','\0','\0',\
								'D','o','t','e','l','s','\0','\0',\
								};
const unsigned char RET_FM_VER[]={0xFE,0x27,HD_VER,FM_VER};
//const unsigned char RET_SN[]={0xAA,0x2A,0x10,0x3A,0x00,0x00,0x00,0x00,0x00,0x07,0x30,0x30,0x30,0x30,0x31,0x30,0x31,0x31,0x31,0x5A,0x4E,0x43,0x5A,0x31,0x34,0x37,0x35,0x31,0x35,0x31,0x32,0x33,0x30,0x30,0x30,0x30,0x32,0x39,0x30,0x30,0x30,0x30,0xD4};
const unsigned char RET_SN[]={0xAA,0x2A,0x27,0x0D,0x00,0x00,0x00,0x00,0x00,0x07,0x30,0x30,0x30,0x30,0x32,0x37,0x33,0x33,0x31,0x30,0x30,0x32,0x37,0x30,0x30,0x30,0x33,0x39,0x41,0x31,0x36,0x52,0x56,0x31,0x30,0x30,0x30,0x31,0x30,0x30,0x30,0x30,0x14};
/*============================================================================================================*/
typedef void (*pFuncCb)(int); 
pFuncCb callback_function_id;

void setnum(int n,void (* ptr)())
{
	callback_function_id=ptr;
}


extern void update_ctaux_aircond_state(unsigned char number)
{
	char t=number;
	switch(number)
	{
		case ASR_ID_ON:
			g_ataux_air_status.ack_onoff = 0x02;
			break;

		case ASR_ID_OFF:
			g_ataux_air_status.ack_onoff = 0x01;
			break;

		case ASR_ID_TEMP_INC:
			if(g_ataux_air_status.ack_temp < 0x10 )
				g_ataux_air_status.ack_temp = 0x10;
			g_ataux_air_status.ack_temp ++;
			if(g_ataux_air_status.ack_temp > 0x20 )
				g_ataux_air_status.ack_temp = 0x20;
			break;

		case ASR_ID_TEMP_DEC:
			if(g_ataux_air_status.ack_temp > 0x20)
				g_ataux_air_status.ack_temp = 0x20;
			g_ataux_air_status.ack_temp --;
			if(g_ataux_air_status.ack_temp < 0x10)
				g_ataux_air_status.ack_temp = 0x10;
			break;

		case ASR_ID_TEMP_16 ... ASR_ID_TEMP_30:
			g_ataux_air_status.ack_temp =t-ASR_ID_TEMP_BASE+0x10;
			break;

		case ASR_ID_MODE_AUTOMODE ... ASR_ID_MODE_HEATMODE:
			g_ataux_air_status.ack_mode =t-ASR_ID_MODE_AUTOMODE;
			if(g_ataux_air_status.ack_mode == 0 )//AUTOMODE
			{
				g_ataux_air_status.ack_temp = 0x1a;
				g_ataux_air_status.ack_fan = 0x06;
			}
			else if(g_ataux_air_status.ack_mode == 2 )//DRYMODE
			{
				g_ataux_air_status.ack_fan = 0x06;
			}
			break;

		case ASR_ID_FAN_TINYFAN ... ASR_ID_FAN_TURBFAN:
			g_ataux_air_status.ack_fan =t-ASR_ID_FAN_TINYFAN;
			break;

		case ASR_ID_FAN_MAXFAN:
			g_ataux_air_status.ack_fan = 0x06;
			break;

		case ASR_ID_FAN_MINFAN:
			g_ataux_air_status.ack_fan = 0x01;
			break;

		case ASR_ID_FAN_INCFAN:
			g_ataux_air_status.ack_fan ++;
			if(g_ataux_air_status.ack_fan > 0x06)
				g_ataux_air_status.ack_fan = 0x06;
			break;

		case ASR_ID_FAN_DECFAN:
			g_ataux_air_status.ack_fan --;
			if(g_ataux_air_status.ack_fan <  0x01)
				g_ataux_air_status.ack_fan =  0x01;
			break;

		case ASR_ID_FANMOVE_LR_ON:
			g_ataux_air_status.ack_swing_lr =0x02;
			break;
		case ASR_ID_FANMOVE_LR_OFF:
			g_ataux_air_status.ack_swing_lr =0x01;
			break;
		case ASR_ID_FANMOVE_UP_ON:
			g_ataux_air_status.ack_swing_ud =0x02;
			break;
		case ASR_ID_FANMOVE_UP_OFF:
			g_ataux_air_status.ack_swing_ud =0x01;
			break;

		case ASR_ID_LIGHT_ON:
			g_ataux_air_status.ack_display = 0x02;
			break;

		case ASR_ID_LIGHT_OFF:
			g_ataux_air_status.ack_display = 0x01;
			break;

		default:
			break;
		}
	
		printf("--\r\n");
}


void iot_ctauxAircondState(ProtocolHandle handle,unsigned char number)
{
	update_ctaux_aircond_state(number);
	return;
}

unsigned short iot_aux_checksum(unsigned char * data,unsigned short len)
{
	uint64_t cksum = 0; 
	int i = 0; 
	while (i < len){ 
		if (i + 2 > len) 
			cksum += data[i]; 
		else 
			cksum += data[i + 1] << 8 | data[i]; 
		i += 2; 
	}
	cksum = (cksum >> 16) + (cksum & 0xffff);
	cksum += (cksum >>16); cksum = (uint16_t)~cksum; 
	return ((cksum >> 8) | (cksum & 0xff) << 8); 
}

void iot_ctaux_set_typeid(char typeid)
{
	DeviceTYPEID = typeid;
}

unsigned char *iot_ctaux_protocol_data(ProtocolHandle handle,unsigned char msgtype,unsigned char * msg_data,int msglen_data,unsigned char ack_type )
{
	Protocol_Params *params = (Protocol_Params *)handle;
	unsigned short csum;
	if(msg_data == NULL && msglen_data > 0 )
		return NULL;
	if(msglen_data > PROTOCOL_MAX_MSGSIZE )
	{
		printf("err:length = %d > %d \r\n",msglen_data,PROTOCOL_MAX_MSGSIZE);
		return NULL;
	}
	uni_memset(&g_ctaux_protacol_sdata,0x00,sizeof(g_ctaux_protacol_sdata));
	uni_memset(&g_ctaux_protacol_sdata_path,0x00,sizeof(g_ctaux_protacol_sdata_path));
	
	g_ctaux_protacol_sdata.sync1 = PROTOCOL_SYNC_HEAD1;//帧头 0xBB00
	g_ctaux_protacol_sdata.sync2 = PROTOCOL_SYNC_HEAD2;
	g_ctaux_protacol_sdata.cmd_type1	= PROTOCOL_VERSION_1;//新协议标志
	g_ctaux_protacol_sdata.cmd_type2 = PROTOCOL_VERSION_2;
	g_ctaux_protacol_sdata.subcmd_type = ack_type;//消息命令类型
	g_ctaux_protacol_sdata.length2 = (msglen_data+7)>>8;//消息长度
	g_ctaux_protacol_sdata.length1 = msglen_data+7;
	g_ctaux_protacol_sdata.dev_type1 = PROTOCOL_DEV_TYPE_1;//t荣邦插座
	g_ctaux_protacol_sdata.dev_type2 = PROTOCOL_DEV_TYPE_2;
	g_ctaux_protacol_sdata.dev_subtype1 = PROTOCOL_SUBDEV_TYPE_1;//t荣邦插座
	g_ctaux_protacol_sdata.dev_subtype2 = PROTOCOL_SUBDEV_TYPE_2;
	g_ctaux_protacol_sdata.reserve = PROTOCOL_RESERVE;//预留

	g_ctaux_protacol_sdata_path.sync1 = PROTOCOL_SYNC_HEAD1;//帧头 0xBB00
	g_ctaux_protacol_sdata_path.sync2 = PROTOCOL_SYNC_HEAD2;
	g_ctaux_protacol_sdata_path.cmd_type1	= PROTOCOL_VERSION_1;//新协议标志
	g_ctaux_protacol_sdata_path.cmd_type2 = PROTOCOL_VERSION_2;
	g_ctaux_protacol_sdata_path.subcmd_type = ack_type;//消息命令类型
	g_ctaux_protacol_sdata_path.length2 = (msglen_data+9)>>8;//消息长度
	g_ctaux_protacol_sdata_path.length1 = msglen_data+9;
	g_ctaux_protacol_sdata_path.dev_type1 = PROTOCOL_DEV_TYPE_1;//t荣邦插座
	g_ctaux_protacol_sdata_path.dev_type2 = PROTOCOL_DEV_TYPE_2;
	g_ctaux_protacol_sdata_path.dev_subtype1 = PROTOCOL_SUBDEV_TYPE_1;//t荣邦插座
	g_ctaux_protacol_sdata_path.dev_subtype2 = PROTOCOL_SUBDEV_TYPE_2;
	g_ctaux_protacol_sdata_path.reserve = PROTOCOL_RESERVE;//预留
	//printf("g_ctaux_protacol_sdata_path.length1:0x%x\n",g_ctaux_protacol_sdata.length1);
	if(msg_data != NULL){
		switch(ack_type){
			case MCU_STATTUS_UP:
				uni_memcpy(g_ctaux_protacol_sdata.message,msg_data,msglen_data);//消息体
				break;
			case  PASSTH_CTL_DOWN:
				uni_memcpy(g_ctaux_protacol_sdata_path.message,msg_data,msglen_data);//消息体
				break;
			case MUC_CTL_MODULE_UP:
				uni_memcpy(g_ctaux_protacol_sdata.message,msg_data,msglen_data);//消息体
				break;
			default:
				printf("cope other ack_type msg_data\n");
				uni_memcpy(g_ctaux_protacol_sdata.message,msg_data,msglen_data);//消息体
				break;
		}
	}
	//g_ctaux_protacol_sdata_path.message=g_ctaux_protacol_sdata_path.message
	//if(msglen_data < PROTOCOL_MAX_MSGSIZE){
	//	g_ctaux_protacol_sdata_path.message[msglen_data]  =   g_ctaux_protacol_sdata_path.csum1;
		//g_ctaux_protacol_sdata_path.message[msglen_data+1] = g_ctaux_protacol_sdata_path.csum2;
	//}
	g_ctaux_protacol_sdata_path.ref_csum1=g_csum1;
	g_ctaux_protacol_sdata_path.ref_csum2=g_csum2;
	switch(ack_type){
		case MCU_STATTUS_UP:
			csum = iot_aux_checksum(&g_ctaux_protacol_sdata.sync1,msglen_data + PROTOCOL_MGS_DATA_HEAD_SIZE);
			g_ctaux_protacol_sdata.csum1 = (csum>>8) & 0xff;
			g_ctaux_protacol_sdata.csum2 = csum & 0xff;
			g_ctaux_protacol_sdata.message[msglen_data]  =  g_ctaux_protacol_sdata.csum1;
			g_ctaux_protacol_sdata.message[msglen_data+1] = g_ctaux_protacol_sdata.csum2;
			break;
		case PASSTH_CTL_DOWN:
			csum = iot_aux_checksum(&g_ctaux_protacol_sdata_path.sync1,msglen_data + PROTOCOL_MGS_DATA_HEAD_SIZE+2);
			g_ctaux_protacol_sdata_path.csum1 = (csum>>8) & 0xff;
			g_ctaux_protacol_sdata_path.csum2 = csum & 0xff;
			break;
		case MUC_CTL_MODULE_UP:
			csum = iot_aux_checksum(&g_ctaux_protacol_sdata.sync1,msglen_data + PROTOCOL_MGS_DATA_HEAD_SIZE);//奥克斯消息校验码
			g_ctaux_protacol_sdata.csum1 = (csum>>8) & 0xff;
			g_ctaux_protacol_sdata.csum2 = csum & 0xff;
			g_ctaux_protacol_sdata.message[msglen_data]  =  g_ctaux_protacol_sdata.csum1;
			g_ctaux_protacol_sdata.message[msglen_data+1] = g_ctaux_protacol_sdata.csum2;	
			break;
		default:
			csum = iot_aux_checksum(&g_ctaux_protacol_sdata.sync1,msglen_data + PROTOCOL_MGS_DATA_HEAD_SIZE);//奥克斯消息校验码
			g_ctaux_protacol_sdata.csum1 = (csum>>8) & 0xff;
			g_ctaux_protacol_sdata.csum2 = csum & 0xff;
			printf("seting other ack_type csum==\n");
			//prinf("send==:csum = 0x%X,msg_data:0x%x,g_ctaux_protacol_sdata.csum1:0x%x\r\n",csum,*msg_data,g_ctaux_protacol_sdata.csum1);
			break;
	}
	switch(ack_type){
		case MCU_STATTUS_UP:
			g_ctaux_protacol_sdata_path.length2=(msglen_data+7)>>8+9;
			return &g_ctaux_protacol_sdata.sync1;
		case PASSTH_CTL_DOWN:
			g_ctaux_protacol_sdata_path.length2=(msglen_data+7)>>8+9;
			return &g_ctaux_protacol_sdata_path.sync1;
		default:
			g_ctaux_protacol_sdata.length2=(msglen_data+7)>>8+9;
			printf("return other ack_type g_ctaux_protacol_sdata==\n");
			return  &g_ctaux_protacol_sdata.sync1;
	}
}

const unsigned char CMD_CFG_WIFI[]={0xAA,0x02,0xC0,0x01,0xC3};

int iot_ctaux_protocol_data_Send(ProtocolHandle handle,unsigned char msgtype,unsigned char * msg_data,int msglen_data,unsigned char ack_type)
{
	Protocol_Params *params = (Protocol_Params *)handle;
	pctaux_pro pMsg = NULL;
	pctaux_pro_path pMsg_path = NULL;
	pMsg = iot_ctaux_protocol_data(handle,msgtype,msg_data,msglen_data,ack_type);
	printf("ack_type:0x%x\n",ack_type);
	//pmsg=(unsigned char *)pMsg;
	if(pMsg != NULL )
	{	switch(ack_type){
			case MUC_CTL_MODULE_UP://buck
				pMsg_path = iot_ctaux_protocol_data(handle,msgtype,msg_data,msglen_data,ack_type);
				user_uart_send(pMsg,pMsg->length1+PROTOCOL_MGS_HEAD_SIZE-2);
				//uni_hal_uart_send(UART_PORT_T port,uint8_t* SendBuf, uint32_t BufLen,uint32_t TimeOut);
				break;
			case PASSTH_CTL_DOWN:
				pMsg_path = iot_ctaux_protocol_data(handle,msgtype,msg_data,msglen_data,ack_type);
				//pmsg_path=(unsigned char *)pMsg_path;
				user_uart_send(pMsg_path,pMsg_path->length1+PROTOCOL_MGS_HEAD_SIZE-2);
				break;
			case MCU_STATTUS_UP:
				pMsg_path = iot_ctaux_protocol_data(handle,msgtype,msg_data,msglen_data,ack_type);
				//pmsg_path=(unsigned char *)pMsg_path;
				user_uart_send(pMsg_path,pMsg_path->length1+PROTOCOL_MGS_HEAD_SIZE-2);
				break;
			case MODULE_MSG_QUERY_DOWN:
				printf("pMsg->length1:0x%x\n",pMsg->length1);
				//pMsg = iot_ctaux_protocol_data(handle,msgtype,msg_data,msglen_data,ack_type);
				user_uart_send(pMsg,pMsg->length1+PROTOCOL_MGS_HEAD_SIZE-2);
				break;
			default:
				break;
		}		
		return 0;
	}
	return -1;
}

int iot_ctauxConfigwifi(ProtocolHandle handle)
{
	printf("\n");
	unsigned char send_subcmd_type=0x03;
	unsigned char msg_data=0x02;//
	unsigned char msg_data_len=0x01;
	iot_ctaux_protocol_data_Send(handle,0x03,&msg_data,msg_data_len,send_subcmd_type);
}

int iot_ctaux_test_wifi(ProtocolHandle handle)
{
	printf("\n");
	unsigned char send_subcmd_type=0x03;
	unsigned char msg_data=0x03;//
	unsigned char msg_data_len=0x01;
	iot_ctaux_protocol_data_Send(handle,0x03,&msg_data,msg_data_len,send_subcmd_type);
}

int iot_ctaux_protocol_data_passth_ctl_res(ProtocolHandle handle, unsigned char msgtype, unsigned char * msg_data, int msglen_data,unsigned char ack_type)
{
	return iot_ctaux_protocol_data_Send(handle,msgtype,msg_data,msglen_data,ack_type);
}

int g_len=0;
static int _uart_recv_data(char *data, int length)
{
	int ret=0;
	int i=0;
  	while (DataBufferGetDataSize(pw_index) < length) {
    	uni_msleep(5);
  	}
  	ret = DataBufferRead(data, length, pw_index);
  return ret;
}

static void uart_recv(char *buf, int len) 
{
	while (DataBufferGetFreeSize(pw_index) < len) {
		uni_msleep(5);
		}
		DataBufferWrite(pw_index, buf, len);
}

extern int iot_ctaux_receive(ProtocolHandle handle)
{
	Protocol_Params *params = (Protocol_Params *)handle;
	int len=0;
	len=user_uart_recv(pw_index,MAX_RX_SIZE);
		//printf("iot_ctaux_receive:len==:%d\n",len);
	if(  len > 0 && len <= MAX_RX_SIZE)
	{
	#ifdef DEBUG
		int i = 0;
		printf("Uart R(%d):",len);
		for(i = 0;i < len ;i++){
			printf("0x%02X  ", (unsigned char)rxData[i]);
			if(i%8==0 && i >0)
				printf("\n");
			}
		printf("\r\n");
	#endif
	}
	return len;
}

/*============================================================================================================*/
/*extern int iot_ctaux_receive(ProtocolHandle handle)
{
	Protocol_Params *params = (Protocol_Params *)handle;
	int len;

	//		uni_memset(info,0xFF,sizeof(info));
	len = _hb_uart_recv(pw_index,MAX_RX_SIZE);

	if(  len > 0 && len <= MAX_RX_SIZE)
	{
#ifdef DEBUG
		int i = 0;
		printf("Uart R(%d):",len);
		for(i = 0;i < len ;i++)
			printf("0x%02X ",rxData[i]);
		printf("\r\n");
#endif

		//callback_function_id(info[0]);
		pw_index += len;
		return (pw_index - pr_index);
	}
	return (pw_index - pr_index);
}*/

//char拼接成short
static void splicing_data(unsigned short *pr_data,unsigned char *ps_data)
{
	*pr_data=0x0000;
	*pr_data=*pr_data | *ps_data;
	*pr_data=*pr_data << 8;
	ps_data++;
	*pr_data=*pr_data | *ps_data;
	return;
}

/*----------------------------------------------------------
获取有效数据并返回
data：原始输入数据
len：输入原始数据长度
ret: >= 0返回有效数据的起始偏移长度，< 0 为数据查询失败状态
----------------------------------------------------------*/
extern int iot_ctaux_valid_data(unsigned char * data,int len,unsigned char * verifydata)
{
	int i = 1;
	unsigned char ll = len;
	unsigned char *len1 = NULL;
	//unsigned char len2 = NULL;
	unsigned short csdata;
	unsigned char *pdata=data;
	unsigned short plen;
	unsigned short cs = 0;
	//uni_memset(csdata,0x00,2);
	//uni_memset(plen,0x00,2);
	//RB_DEBUG_PRINT("++\r\n");

	if( ll < MIN_RX_SIZE)
		return ERROR_GEN;
	while( ll > MIN_RX_SIZE )
	{	
		//splicing_data(hdata,pdata);
		if((*pdata==PROTOCOL_SYNC_HEAD1)&&(*(pdata+1)==PROTOCOL_SYNC_HEAD2))
		{
			break;
		}
		pdata++;
		ll--;
	}
	printf("valid_data_ll==:%d\n",ll);
	if( ll < MIN_RX_SIZE)
	{
		printf("err:Not found SYNC head\r\n");
		return ERROR_GEN;
	}
	splicing_data(&plen,pdata+PROTOCOL_OFFSET_LEN);
	len1=pdata+PROTOCOL_OFFSET_LEN;
	//*len2=*plen|0XFF;
	if( *len1 >(ll - 7 ) )//长度从消息长度后开始到结束，包含校验，不能大于总长度减去消息长度
	{
		printf("err:length === 0x%X(0x%X)\r\n",(ll - 7),*len1);
		return ERROR_GEN;
	}
	//RB_DEBUG_PRINT("ll = %d\r\n",ll);
	printf("valid_data:\n");
	for(i=0;i<*len1+5;i++){
		printf("  %0x",pdata[i]);
		if(i%8==0 &&i>0)
		printf("\n");
	}
	cs = iot_aux_checksum(pdata,*len1+5);//校验从头开始到校验码之前
	//RB_DEBUG_PRINT("cs = 0x%X(0x%X)\r\n",cs,pdata[ll]);
	splicing_data(&csdata,pdata+*len1+5);
	if(cs != csdata )
	{
		printf("err:cs = 0x%X(0x%X)\r\n",cs,csdata);
		return ERROR_CHECKSUM;
	}
	g_csum1=pdata[*len1+5];
	g_csum2=pdata[*len1+6];
	printf("g_csum1:%d,g_csum2:%d\n",g_csum1,g_csum2);

	ll = (len -2);//消息去头长度
	//RB_DEBUG_PRINT("ll = %d\r\n",ll);
	for( i = 0; i < len ;i++ ){
		verifydata[i] = pdata[i];
		printf("verifydata[%d]:%x\n",i,verifydata[i]);
	}
	//RB_DEBUG_PRINT("--\r\n");
	return (pdata + ll - data) ;
}

extern int iot_ctaux_air_on_off(ProtocolHandle handle,unsigned char msg_data)
{
	int id = 0;
	int ret = RES_OK;

	printf("on_off:msg_data = 0x%.2X\r\n",msg_data);
	//g_ataux_air_status.ack_onoff=msg_data;

	switch(msg_data)
	{
		case CMD_AIRCON_ON:
			id = ASR_ID_ON;
			//g_ataux_air_status.ack_onoff = CMD_AIRCON_ON;
			break;

		case CMD_AIRCON_OFF:
			id = ASR_ID_OFF;
			//g_ataux_air_status.ack_onoff = CMD_AIRCON_OFF;
			break;

		default:
			ret = ERROR_NOTSUPORT;
			break;
	}

	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		//callback_function_id(id);
		setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_ctaux_air_temp_ctl(ProtocolHandle handle,unsigned char msg_data)
{
	int id = 0;
	int ret = RES_OK;

	//printf("iot_ctaux_air_temp_ctl:msg_data = 0x%.2X\r\n",msg_data);
	//g_ataux_air_status.ack_temp = msg_data;

	switch(msg_data)
	{
		case CMD_AIRCON_TEMP16 ... CMD_AIRCON_TEMP32:
			id = ASR_ID_TEMP_16+msg_data-CMD_AIRCON_TEMP16;
			break;
		default:
			ret = ERROR_NOTSUPORT;
			break;
	}
	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_ctaux_air_mode_ctl(ProtocolHandle handle,unsigned char msg_data)
{
	int id = 0;
	int ret = RES_OK;

	//printf("iot_ctaux_air_mode_ctl:msg_data = 0x%.2X\r\n",msg_data);
	//g_ataux_air_status.ack_mode=msg_data;
	switch(msg_data)
	{
		case CMD_AIRCON_MODE_AUTO:
			id = ASR_ID_MODE_AUTOMODE;
			//g_ataux_air_status.ack_mode = CMD_AIRCON_MODE_AUTO;
			break;
		case CMD_AIRCON_MODE_COOL:
			id = ASR_ID_MODE_COOLMODE;
			//g_ataux_air_status.ack_mode = CMD_AIRCON_MODE_COOL;
			break;
		case CMD_AIRCON_MODE_DRY:
			id = ASR_ID_MODE_DRYMODE;
			//g_ataux_air_status.ack_mode = CMD_AIRCON_MODE_DRY;
			break;
		case CMD_AIRCON_MODE_FAN:
			id = ASR_ID_MODE_FANMODE;
			//g_ataux_air_status.ack_mode = CMD_AIRCON_MODE_FAN;
			break;
		case CMD_AIRCON_MODE_HEAT:
			id = ASR_ID_MODE_HEATMODE;
			//g_ataux_air_status.ack_mode = CMD_AIRCON_MODE_HEAT;
			break;
		default:
			ret = ASR_ID_MODE_COOLMODE;
			break;
	}

	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_ctaux_air_wind_speed(ProtocolHandle handle,unsigned char msg_data)
{
	int id = 0;
	int ret = RES_OK;

	//printf("wind_speed:msg_data = 0x%.2X\r\n",msg_data);
	//g_ataux_air_status.ack_fan=msg_data;
	switch(msg_data)
	{
		case CMD_AIRCON_FAN_TINY:
			id = ASR_ID_FAN_TINYFAN;
			//g_ataux_air_status.ack_fan = CMD_AIRCON_FAN_TINY;
			break;
		case CMD_AIRCON_FAN_LOW:
			id = ASR_ID_FAN_LOWFAN;
			//g_ataux_air_status.ack_fan = CMD_AIRCON_FAN_LOW;
			break;
		case CMD_AIRCON_FAN_MID:
			id = ASR_ID_FAN_MIDFAN;
			//g_ataux_air_status.ack_fan = CMD_AIRCON_FAN_MID;
			break;
		case CMD_AIRCON_FAN_HIGH:
			id = ASR_ID_FAN_HIGHFAN;
			//g_ataux_air_status.ack_fan = CMD_AIRCON_FAN_HIGH;
			break;
		case CMD_AIRCON_FAN_TRUB:
			id = ASR_ID_FAN_TURBFAN;
			//g_ataux_air_status.ack_fan = CMD_AIRCON_FAN_TRUB;
			break;
		case CMD_AIRCON_FAN_AUTO:
			//id = ASR_ID_OFF;暂无
			break;
		default:
			ret = CMD_AIRCON_FAN_AUTO;
			break;
	}

	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_ctaux_air_swing(ProtocolHandle handle,unsigned char msg_data,unsigned char msg_data1)
{
	int id = 0;
	int ret = RES_OK;

	//printf("air_swing::msg_data = 0x%.2X,msg_data1 = 0x%.2X\r\n",msg_data,msg_data1);
	if(msg_data != 0){
		switch(msg_data)
		{
			case CMD_AIRCON_FAN_LR_ON:
				id = ASR_ID_FANMOVE_LR_ON;
				//g_ataux_air_status.ack_swing_ud = CMD_AIRCON_FAN_LR_ON;
				break;
			case CMD_AIRCON_FAN_LR_OFF:
				id = ASR_ID_FANMOVE_LR_OFF;
				//g_ataux_air_status.ack_swing_ud = CMD_AIRCON_FAN_LR_OFF;
				break;
			default:
				ret = ERROR_NOTSUPORT;
				break;
		}
	}else{
		switch (msg_data1)
		{
			case CMD_AIRCON_FAN_UP_ON:
				id = ASR_ID_FANMOVE_UP_ON;
				//g_ataux_air_status.ack_swing_lr = CMD_AIRCON_FAN_UP_ON;
				break;
			case CMD_AIRCON_FAN_UP_OFF:
				id = ASR_ID_FANMOVE_UP_OFF;
				//g_ataux_air_status.ack_swing_lr = CMD_AIRCON_FAN_UP_OFF;
				break;
			default:
				ret = ERROR_NOTSUPORT;
				break;
		}
	}
	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_ctaux_air_display(ProtocolHandle handle,unsigned char msg_data)
{
	int id = 0;
	int ret = RES_OK;

	//printf("display:msg_data = 0x%.2X\r\n",msg_data);
	//g_ataux_air_status.ack_display=msg_data;

	switch(msg_data)
	{
		case CMD_AIRCON_DISPLAY_ON_OFF:
			id = ASR_ID_LIGHT_ON_OFF;
			//g_ataux_air_status.ack_display = CMD_AIRCON_DISPLAY_OFF;
			break;
		case CMD_AIRCON_DISPLAY_OFF:
			id = ASR_ID_LIGHT_OFF;
			//g_ataux_air_status.ack_display = CMD_AIRCON_DISPLAY_OFF;
			break;

		case CMD_AIRCON_DISPLAY_ON:
			id = ASR_ID_LIGHT_ON;
			//g_ataux_air_status.ack_display = CMD_AIRCON_DISPLAY_ON;
			break;

		default:
			ret = ERROR_NOTSUPORT;
			break;
	}

	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_ctaux_air_status_updata(ProtocolHandle handle,unsigned char msg_data)
{
	int id = 0;
	int ret = RES_OK;

	printf("msg_data = 0x%.2X\r\n",msg_data);

	switch(msg_data)
	{
		case CMD_AIRCON_ON:
			id = ASR_ID_ON;
			break;

		case CMD_AIRCON_OFF:
			id = ASR_ID_OFF;
			break;

		default:
			ret = ERROR_NOTSUPORT;
			break;
	}

	if(ret != ERROR_NOTSUPORT)
	{
		//protocolSend(handle,id);
		//callback_function_id(id);
		//setnumber(id);
		ret = RES_OK;
	}

	return ret;
}

extern int iot_quert_protocol_version(ProtocolHandle handle,unsigned char * data,unsigned char msgtype)
{
	int ret;
	Protocol_Params *params = (Protocol_Params *)handle;
	unsigned char send_subcmd_type=0x01;
	unsigned char msg_data[]={1,0,1,0,7,0,1,0,0,0,1,8,0,0,0,0,0,0,0,1};//
	unsigned char msg_data_len=20;
	//unsigned char msg_data[]={1,0,1,0,7,0,1,0,0,0};//
	//unsigned char msg_data_len=10;
	printf("iot_quert_protocol_version=1111==\n");
	iot_ctaux_protocol_data_Send(handle,0x03,msg_data,msg_data_len,send_subcmd_type);
	return ret;
}


extern int iot_ctaux_deal_data_airconductor(ProtocolHandle handle,unsigned char * data,unsigned char msgtype)
{
	Protocol_Params *params = (Protocol_Params *)handle;
	int ret = RES_OK_NODATA;
	unsigned char msg_type;
	unsigned char cmd;
	unsigned char dev;
	//RB_DEBUG_PRINT("++\r\n");

#ifdef DEBUG0
	int i = 0;
	int len = data[1]+1;
	printf("R(%d):",len);
	for(i = 0;i < len ;i++)
		printf("0x%02X ",data[i]);
	printf("\r\n");
#endif

	msg_type = data[PROTOCOL_OFFSET_MSG_CTL];
	printf("msg_type = 0x%02X\r\n",msg_type);

	cmd = data[PROTOCOL_OFFSET_MSG_CTL];
	switch(msg_type)
	{
		case CMD_DEV_FUN_QUERY:
			break;
		case CMD_DEV_STATUS_QUERY:
			iot_ctaux_protocol_data_Send(handle,0,&g_ataux_air_status.ack_head1,sizeof(ctaux_air_status),PASSTH_CTL_DOWN);
			//iot_quert_protocol_version(handle,data,MODULE_MSG_QUERY_DOWN);
			break;
		case CMD_DEV_ON_OFF:
			ret = iot_ctaux_air_on_off(handle,data[PROTOCOL_OFFSET_MSG_DATA]);
			break;
		case CMD_DEV_TEMP_CTL:
			ret = iot_ctaux_air_temp_ctl(handle,data[PROTOCOL_OFFSET_MSG_DATA]);
			break;
		case CMD_DEV_MODE_CTL:
			ret=iot_ctaux_air_mode_ctl(handle,data[PROTOCOL_OFFSET_MSG_DATA]);
			break;
		case CMD_DEV_WIND_SPEED:
			ret=iot_ctaux_air_wind_speed(handle,data[PROTOCOL_OFFSET_MSG_DATA]);
			break;
		case CMD_DEV_SWING:
			ret=iot_ctaux_air_swing(handle,data[PROTOCOL_OFFSET_MSG_DATA],data[PROTOCOL_OFFSET_MSG_DATA2]);
			break;
		case CMD_DEV_DISPLAY:
			ret=iot_ctaux_air_display(handle,data[PROTOCOL_OFFSET_MSG_DATA]);
			break;
		case CMD_DEV_STATUS_UPDATA:
			ret=iot_ctaux_air_status_updata(handle,data[PROTOCOL_OFFSET_MSG_DATA]);
			break;
		/*case CMD_GET_DEV_STATE://
			iot_ctaux_protocol_data_Send(handle,msgtype,RET_DEV_STATE,sizeof(RET_DEV_STATE));
			break;*/
		default:
			ret = ERROR_NOTSUPORT;
			break;
	}
	//iot_ctaux_protocol_data_passth_ctl_res(handle,msg_type,&g_ataux_air_status.ack_head1,sizeof(ctaux_air_status),PASSTH_CTL_DOWN);
	//iot_ctaux_protocol_data_Send(handle,0,&g_ataux_air_status.ack_head1,sizeof(ctaux_air_status),MCU_STATTUS_UP);
	//RB_DEBUG_PRINT("--\r\n");
	printf("iot_ctaux_deal_data_airconductor===\n");
	return ret;
}

extern int iot_ctaux_deal_data(ProtocolHandle handle,unsigned char * data)
{
	Protocol_Params *params = (Protocol_Params *)handle;
	pctaux_pro pMsg = NULL;
	int ret = RES_OK_NODATA;
	unsigned short msg_type;
	unsigned long cmd;
	unsigned char dev;
	unsigned char ddata;
	unsigned char subcmd8 = 0;
	unsigned short subcmd16 = 0;
	unsigned char * pp;

	printf("iot_ctaux_deal_data++==\r\n");

#ifdef DEBUG
	int i = 0;
	int len = data[1]+1;
	printf("deal_data(%d):",len);
	for(i = 0;i < len ;i++)
		printf("0x%02X ",data[i]);
	printf("\r\n");
#endif

	msg_type = data[PROTOCOL_OFFSET_SUBMSGTYPE];
	printf("msg_type = ===buck====0x%04X\r\n",msg_type);

	gctaux_msgtype = msg_type;
	switch(msg_type)
	{
		case MODULE_MSG_QUERY_UP:
			break;
		case MODULE_MSG_QUERY_DOWN:
			ret = iot_quert_protocol_version(ctaux_handle,data,MODULE_MSG_QUERY_DOWN);
			break;
		case PASSTH_CTL_DOWN:
			ret = iot_ctaux_deal_data_airconductor(handle,data,PASSTH_CTL_DOWN);
			break;
		case MUC_CTL_MODULE_UP:
			break;
		case MODULE_STATUS_DOWM:
			break;
		case MCU_STATTUS_UP:
			//ret = iot_ctaux_protocol_data_SN(handle);
			ret = RES_OK_NODATA;
			break;
		default:
			ret = ERROR_NOTSUPORT;
			break;
	}

	printf("--\r\n");
	return ret;
}

void iot_ctaux_state_report(ProtocolHandle handle,pctaux_air_status pg_ataux_air_status,int src)
{
	printf("iot_ctaux_state_report1====src:%d==\n",src);
	g_ataux_air_status.ack_onoff=pg_ataux_air_status->ack_onoff;
	g_ataux_air_status.ack_mode=pg_ataux_air_status->ack_mode;
	g_ataux_air_status.ack_temp=pg_ataux_air_status->ack_temp;
	g_ataux_air_status.ack_fan=pg_ataux_air_status->ack_fan;
	g_ataux_air_status.ack_display=pg_ataux_air_status->ack_display;
	g_ataux_air_status.ack_swing_lr=pg_ataux_air_status->ack_swing_lr;
	g_ataux_air_status.ack_swing_ud=pg_ataux_air_status->ack_swing_ud;
	if(src==0)
		iot_ctaux_protocol_data_Send(handle,0,&g_ataux_air_status.ack_head1,sizeof(ctaux_air_status),MCU_STATTUS_UP);
	else
		iot_ctaux_protocol_data_Send(handle,0,&g_ataux_air_status.ack_head1,sizeof(ctaux_air_status),PASSTH_CTL_DOWN);
}


/*============================================================================================================*/
extern int iot_ctaux_main_loop(ProtocolHandle handle)
{
	int ret = 0;
	int len = iot_ctaux_receive(handle);
	ctaux_handle=handle;
	//if(  len > 1)
	if(  len >= MIN_RX_SIZE)
	{
		unsigned char vdata[MAX_RX_SIZE];
		unsigned char * p = NULL;
		uni_memset(vdata,0,MAX_RX_SIZE);
		p = rxData;

		gctaux_msgtype = PASSTH_CTL_DOWN;
		ret = iot_ctaux_valid_data(p,len,vdata);
		printf("ret = %d\r\n",ret);
		if(  ret > 0 )
		{
			//if(ret <= len )
			/*{
				uni_memmove(p,p+ret,len - ret);
				pr_index = rxData;
				pw_index = rxData + (len - ret);
			}*/

			ret = iot_ctaux_deal_data(handle,vdata);
		}
		/*else
		{
			pr_index = rxData;
			pw_index = pr_index;
		}*/

		//printf("pr_index = 0x%p pw_index = 0x%p \r\n",pr_index,pw_index);

		//iot_ctauxRes(handle,ret);

		if( ret < 0 )
			ret = 0;

		return ret;
	}

	/*if( giotinit_flag == 0 )
	{
		ctaux_handle = handle;
		if(	gwifistate == 0 )
		{
			rb_setalarm_func(1,iot_ctaux_check_netcfg);
			rb_setalarm_func_sec(5,iot_ctaux_check_mac);			
		}

		giotinit_flag = 1;
	}*/

	return 0;
}

//static DataBufHandle g_uart_recv = NULL;
#define USER_UART_BUF_SIZE    (35)
static DataBufHandle g_uartrecv_handle = NULL;

/* 接收到串口数据后，该接口将被触发回调 */
static void _hb_uart_recv(char *buf, int len) {
  int i;
  printf("_hb_uart_recv:\n");
  for (i = 0; i < len; i++) {
    printf("%02x ", (uint8_t)buf[i]);
  }
  printf("\n===================\n");
  if (g_uartrecv_handle) DataBufferWrite(g_uartrecv_handle, buf, len);
}

int user_uart_recv(char *buf, int len) 
{
  int read_len = 0;
  if (!g_uartrecv_handle) return 0;

  read_len = DataBufferGetDataSize(g_uartrecv_handle);
  read_len = read_len>len?len:read_len;
  
  if (!read_len)
  	return read_len;
  
  if (read_len != DataBufferRead(buf, read_len, g_uartrecv_handle)) {
    printf("read error %d", read_len);
    return 0;
  }
  return read_len;
}

int hb_uart_init(ProtocolHandle handle)
{
	int ret = user_uart_init(_hb_uart_recv);
	if (ret != 0) {
    	printf("hb uart init failed");
    return -1;
	}
	
	g_uartrecv_handle = DataBufferCreate(USER_UART_BUF_SIZE);
	if (!g_uartrecv_handle) {
		user_uart_final();
		printf("user uart recv buf create fail");
	return -1;
  	}
	iot_ctaux_protocol_data_Send(handle,0,&g_ataux_air_status.ack_head1,sizeof(ctaux_air_status),PASSTH_CTL_DOWN);
  	return 0;
}

/*============================================================================================================*/

#endif//__IOT_ctaux_USED__
