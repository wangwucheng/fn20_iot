/*******************************************************************************
*   File Name:   	green.h
*   Description:	none
*   Atuhor:       hrx
*   Date:         2018/12/04
*   Version:      2018/12/04   v1.0
*******************************************************************************/

#include "rsTypeDef.h"
#ifndef _GREENBANLV_H_
#define _GREENBANLV_H_
//#define VOICE_WAKEUPBY_BEIBEI
//#define VOICE_WAKEUPBY_GREE
#define VOICE_WAKEUPBY_MIDEA
//#define VOICE_WAKEUPBY_AUX

//#define DSP_ID_USED_ALARM
//#define DSP_ID_USED_STUDY

#define FLASH_USER_CFG_USED

//#define __IOT_GREE_USED__
//#define __IOT_MIDEA_USED__
#define __IOT_CTAUX_USED__
#ifdef __IOT_GREE_USED__
	#define IOT_UART2_USED
	#define BB_VER_GMA
#elif defined __IOT_MIDEA_USED__
	#define IOT_UART2_USED
	#define BB_VER_MGA
	//#define BB_VER_AMG
#elif defined __IOT_CTAUX_USED__
	#define IOT_UART2_USED
	#define BB_VER_AMG
	//#define BB_VER_AMG
#else
	#define BB_VER_MGA
	//#define BB_VER_HISENSE
	//#define BB_VER_GMA
	//#define BB_VER_AMG
	//#define BB_VER_THH
	//#define BB_VER_HCD
	//#define BB_VER_STU
#endif

#define INTERNAL_RC_GREE					(1<<0)
#define INTERNAL_RC_MIDEA2 					(1<<1)
#define INTERNAL_RC_AUX						(1<<2)
#define INTERNAL_RC_TCL						(1<<3)
#define INTERNAL_RC_HAIER					(1<<4)
#define INTERNAL_RC_HISENSE 				(1<<5)
#define INTERNAL_RC_CHIGO					(1<<6)
#define INTERNAL_RC_DOTELS 					(1<<7)
#define INTERNAL_RC_DAIKIN					(1<<8)
#define INTERNAL_RC_MITSUBISHI 				(1<<9)
#define INTERNAL_RC_MIDEA					(1<<15)

#ifdef BB_VER_GAM
#define DEV_SORT_ARRAY	{INTERNAL_RC_GREE,INTERNAL_RC_AUX,INTERNAL_RC_MIDEA2}
#endif
#ifdef BB_VER_GMA
#define DEV_SORT_ARRAY	{INTERNAL_RC_GREE,INTERNAL_RC_MIDEA2,INTERNAL_RC_AUX}
#endif
#ifdef BB_VER_MGA
#define DEV_SORT_ARRAY	{INTERNAL_RC_MIDEA2,INTERNAL_RC_GREE,INTERNAL_RC_AUX}
#endif
#ifdef BB_VER_AMG
#define DEV_SORT_ARRAY	{INTERNAL_RC_AUX,INTERNAL_RC_MIDEA2,INTERNAL_RC_GREE}
#endif
#ifdef BB_VER_THH
#define DEV_SORT_ARRAY	{INTERNAL_RC_TCL,INTERNAL_RC_HAIER,INTERNAL_RC_HISENSE}
#endif
#ifdef BB_VER_HCD
#define DEV_SORT_ARRAY	{INTERNAL_RC_HAIER,INTERNAL_RC_CHIGO,INTERNAL_RC_DOTELS}
#endif
#ifdef BB_VER_HISENSE
#define DEV_SORT_ARRAY	{INTERNAL_RC_HISENSE}
#endif
typedef void *HUartHandle;
typedef void *ProtocolHandle;
#define SN_SIZE	(32)
#define ON	(1)
#define OFF	(0)

extern int Dev_Arr[];

#define DELAY_INTERVAL_10MS		(3)
#define DELAY_INTERVAL_50MS		(DELAY_INTERVAL_10MS*5+1)
#define DELAY_INTERVAL_60MS		(DELAY_INTERVAL_10MS*7)
#define DELAY_INTERVAL_70MS		(DELAY_INTERVAL_10MS*8)
#define DELAY_INTERVAL_100MS	(DELAY_INTERVAL_10MS*12)
#define DELAY_INTERVAL_XXMS 	(DELAY_INTERVAL_70MS)

#define MSGSIZE 6 // MSG protocol(6Bytes data): 5A(start) XX XX XX  datacheck(xor check) EF(end)
typedef struct _PROTOCOL_PARAMS_{
	HUartHandle uartHandle;
	unsigned char sendData[MSGSIZE];
	unsigned char receiveData[MSGSIZE];
	int pwm1;
	int period;	//pwm period
	int period1;	//pwm period
	int iswakeup;
	int pwmcnt;
	int cellkey;
	int loopkey;
	char hertdata[MSGSIZE];
	int hertkey;
}Protocol_Params;

typedef struct AIRCOND_STATE{
	unsigned char device;
	unsigned char onoff;
	unsigned char mode;
	unsigned char settemp;
	unsigned char envtemp;
	unsigned char fanspeed;
	unsigned char heat_onoff;
	unsigned char dry_onoff;
	unsigned char sleepmode;
	unsigned char fanmode;
	unsigned char light_onoff;
}_AIRCOND_STATE,*pAIRCOND_STATE;

void ir_melodyInit(void);
void buzz_control(unsigned int number);

unsigned int get_suport_devices(void);
unsigned int set_suport_devices(unsigned int devices);
unsigned char * get_devices_sn( void );
extern int protocolSend(ProtocolHandle handle,int number,int src);

#endif
