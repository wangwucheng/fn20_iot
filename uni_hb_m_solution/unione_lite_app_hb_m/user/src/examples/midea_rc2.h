
#ifndef __MIDEA2_RC_H__
#define __MIDEA2_RC_H__

/*============================================================================================================*/
#include "greebanlv.h"
#include "ir_tx.h"
#include "bb_type.h"
void midea2_remote_isr(void) ;

void midea2_ir_init(void);
void midea2_encode_irdata(uint8_t* func_data);
void midea2_remote_irsend(void);
void midea2_get_state(pAIRCOND_STATE st);

/*============================================================================================================*/

int Midea2_RemoteSend(unsigned char *data);
/*============================================================================================================*/


#endif//__MIDEA2_RC_H__

