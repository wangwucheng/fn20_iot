#include "ir_tx.h"
#include "gree_rc.h"
//#include "midea_rc.h"
#include "midea_rc2.h"
#include "aux_rc.h"
#include "user_sw_timer.h"
#include "user_gpio.h"
#include "user_pwm.h"

/*============================================================================================================*/
#define pwm_ch_b  PWM_NUM_1_A27                    // pwm channel
static uint16_t pwm_duty = 50;                      // pwmռ�ձ�
static uint16_t pwm_period = 38000;                    // pwmƵ�� 38K

static volatile uint8_t iPWM_IO = 1;

#define USR_TIMER_NUM  eTIMER2  // user timer2
#define PARTICLE_US    20       // 50us


timer_handle_t handle;
/*============================================================================================================*/

// ir�������� 38khz�ľ��β�
void ir_tx_pin_low(void)
{
	
	if( iPWM_IO )
	{
		//printf("ir_tx_pin_low========low\n");
		user_pwm_start(pwm_ch_b, pwm_duty);
		iPWM_IO = 0;
	}
}

// ir�������� ��1
void ir_tx_pin_high(void)
{
	if( iPWM_IO )
		return;
		//printf("ir_tx_pin_high========\n");
		user_pwm_start(pwm_ch_b, 0);//关闭PWM，切设置为低电平
		//silan_io_pwm_config(io_p, pwm_period, pwm_period);
		iPWM_IO = 1;
}


static void pwm_ctrl_sw_timer_cb(eTIMER_IDX timer) 
{
		if(timer==USR_TIMER_NUM){
			//user_timer_resume(USR_TIMER_NUM);
			midea2_remote_isr();
			gree_remote_isr();
			aux_remote_isr();
			midea_fan_remote_isr();
		}
		//midea_remote_isr();
		//midea2_remote_isr();
		//aux_remote_isr();
		//tcl_remote_isr();
		//haier_remote_isr();
		//hisense_remote_isr();
		//chigo_remote_isr();
		//dotels_remote_isr();
}

void ir_tx_init(void)
{
	printf("ir_tx_init==\n");
	user_timer_init(USR_TIMER_NUM, PARTICLE_US, false,pwm_ctrl_sw_timer_cb);
	user_timer_start(USR_TIMER_NUM);
   //user_sw_timer_init(USR_TIMER_NUM, PARTICLE_MS);
   //handle = user_sw_timer_add(PARTICLE_MS, false, pwm_ctrl_sw_timer_cb);//25除以500=50us
   user_pwm_init(pwm_ch_b, pwm_period, 1);//Ƶ��38k,num=0m,ռ�ձ�1
   user_pwm_start(pwm_ch_b, 0);//关闭PWM，切设置为低电平
}

