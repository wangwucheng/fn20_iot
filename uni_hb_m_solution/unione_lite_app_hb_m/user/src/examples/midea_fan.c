//meidi_fan_protocol file
#include "protocolInterface.h"
#include "greebanlv.h"
#include "midea_fan_MacroDefine.h"
#include "ir_tx.h"
#include "midea_fan.h"
#include "unione.h"
#include "ir_tx.h"

/*=======================================================*/
static volatile uint8_t iSendIdx = 0;

static uint8_t mc_status = 0;
static uint8_t mc_bytetime = 0;

/*========================================================*/

static SegByte unFlag0;

#define fgInitFlag			unFlag0.bit.b4		
#define fgLRSwing			unFlag0.bit.b3		
#define fgUDSwing			unFlag0.bit.b2		
#define fgRF_Txd_ready			unFlag0.bit.b1 
#define fgOn	 				unFlag0.bit.b0 

//uint8_t iDataReady = 0;
/*========================================================*/
/*========================================================*/
static uint8_t mSendBitStatus = 0;
static uint8_t mSendheadStatus = 0;

static volatile uint16_t remote_timer = 0;          // 计时       
static volatile uint8_t remote_bit_t = 0x00;        // 发送数据时，低电平的持续时间
static volatile uint8_t remote_B = 0;               // 发送bit的计数器
static volatile uint8_t remote_bit_hl = 1;          // 发送高电平的标志


static uint8_t gu8RmtBuf[6];

const unsigned char FAN_ONOFF_ARR[] = { 0x80, 0x7F, 0xC0, 0x3F, 0xC0, 0x3F };		// 开/关机
const unsigned char FAN_INC_ARR[] = { 0x80, 0x7F, 0xB0, 0x4F, 0xB0, 0x4F	};		// 加键
const unsigned char FAN_ALARM_ARR[] = { 0x80, 0x7F, 0xD0, 0x2F, 0xD0, 0x2F	};	// 定时
const unsigned char FAN_SWING_ARR[] = { 0x80, 0x7F, 0xC8, 0x37, 0xC8, 0x37	};	// 摇头
const unsigned char FAN_DEC_ARR[] = { 0x80, 0x7F, 0x88, 0x77, 0x88, 0x77	};		// 减键
const unsigned char FAN_TRUBO_ARR[] = { 0x80, 0x7F, 0xB4, 0x4B, 0xB4, 0x4B	};	// 强劲
const unsigned char FAN_MODE_ARR[] = { 0x80, 0x7F, 0xF0, 0x0F, 0xF0, 0x0F	};		// 模式

static uint8_t ir_send_head()
{
	switch(mSendheadStatus)
	{
		case 0:
			ir_tx_pin_low();
			if (remote_timer >= 450)
			{
				//printf("remote_timer:%d\n",remote_timer);
				remote_timer = 0;
				mSendheadStatus ++;
				//ir_tx_pin_high();
			}
			break;
		case 1:
			ir_tx_pin_high();
			if(remote_timer >= 225)
			{
				remote_timer = 0;
				mSendheadStatus ++;
				return true;
			}
	}

	return false;
}

static uint8_t ir_send_end()
{
		ir_tx_pin_low();
		if (remote_timer >= 76)
			{
				remote_timer = 0;
				//mSendBitStatus ++;
				return true;
			}
	/*switch(mSendBitStatus)
	{
		case 0:
			ir_tx_pin_low();
			if (remote_timer >= 55)
			{
				remote_timer = 0;
				mSendBitStatus ++;
			}
			break;
		case 1:
			//ir_tx_pin_high();
			if(remote_timer >= 10)
			{
				remote_timer= 0;
				mSendBitStatus ++;
			}
		case 2:
			//ir_tx_pin_low();
			if(remote_timer >= 10)
			{
				remote_timer = 0;
				return true;
			}
	}*/

	return false;
}

//发送一位数据,需要先在前面拉低0.56ms,再根据要发送的位值来拉高电平，位数据是0时拉高0.56ms，位数据是1时拉高1.68ms
//发送完成后返回true,未发送完成返回false
//
static uint8_t ir_send_bit(uint8_t bit)
{
	if(bit)
		mc_bytetime = (28+81);
	else
		mc_bytetime = (28+28);

	if (remote_timer < 28) {
		ir_tx_pin_low();
	}
	else
	{
		if (remote_timer < mc_bytetime) {
			ir_tx_pin_high();
		}
		else{
			remote_timer = 0;	//reset count;
			return true;
		}
	}

	return false;
}

static uint8_t ir_send_byte(uint8_t byte)
{
	if(ir_send_bit((byte >> remote_B) & 0x01))
	{
		if(remote_B == 0)
			return true;
		remote_B --;
	}
	return false;
}

/*============================================================================================================*/
void midea_fan_ir_send_command_Process(void)
{	
	//data not ready.
	//if(iDataReady == false)
	if(fgRF_Txd_ready == 0)
	{
		return;
	}

	remote_timer++;
	
	switch(mc_status)
	{
		case 0: //head
			if(ir_send_head())
			{
				mc_status++;
				remote_timer = 0;
				
				//配置发送数据的信息
				remote_B = 7;	//先发送最高位，所以要右移7位。
				iSendIdx = 0;
				mSendBitStatus = 0;
				ir_tx_pin_low();
			}
			break;

		case 1: //数据6字节
			if(ir_send_byte(gu8RmtBuf[iSendIdx]))
			{
				mSendBitStatus = 0;
				//printf(">%d:0x%.2X\n", iSendIdx,gu8RmtBuf[iSendIdx]);
				iSendIdx++;
				remote_B = 7;
				if(iSendIdx == 6)	//数据分二组，先发6个字节，下一步要发送帧分隔符。
				{
					++mc_status;
					ir_tx_pin_low();	//帧分隔符拉低540ms
					remote_timer = 0;
				}
			}

			break;
		case 2: //发送终结码
			if(ir_send_end())
			{
				remote_timer = 0;
				mc_status ++;
				ir_tx_pin_high();
			}
			break;
		default:
			break;
	}
}

/*================================================*/

/*************************************************************
 函数：midea_fan_subDisplayDecode
 输入：
 输出：
 功能：
 用法：
*************************************************************/
void midea_fan_subDisplayDecode(uint8_t* data)
{
	uint8_t u8Tmp = 0;
	int i=0;
//	hexdump("data",data,4);

	if( data[1] != MIDEA_FAN_DEV_ID )
		return;
	
	{
		printf("fan cmd = %d\n", data[2]);
		switch( data[2] )
		{
			case MIDEA_FAN_FUC_ON :
			case MIDEA_FAN_FUC_OFF :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_ONOFF_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_ONOFF_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;

			case MIDEA_FAN_FUC_SWINGON :
			case MIDEA_FAN_FUC_SWINGOFF :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_SWING_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_SWING_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;

			case MIDEA_FAN_FUC_SPEEDINC :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_INC_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_INC_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;

			case MIDEA_FAN_FUC_SPEEDDEC :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_DEC_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_DEC_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;

			case MIDEA_FAN_FUC_SPEEDHIGH :
			case MIDEA_FAN_FUC_SPEEDMAX :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_TRUBO_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_TRUBO_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;

			case MIDEA_FAN_FUC_ALARM :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_ALARM_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_ALARM_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;

			case MIDEA_FAN_FUC_MODE :
				for(i=0;i<6;i++)
				gu8RmtBuf[i]=FAN_MODE_ARR[i];
				//uni_memcpy(gu8RmtBuf,FAN_MODE_ARR,sizeof(gu8RmtBuf));
				fgRF_Txd_ready = 1;
				break;
			
			case MIDEA_FAN_FUC_SPEEDMID :
			case MIDEA_FAN_FUC_SPEEDLOW :
			case MIDEA_FAN_FUC_SPEEDMIN :
			default:
				fgRF_Txd_ready = 0;
				break;			
		}
		//for(i=0;i<6;i++)
		//printf("midea_fan_subDisplayDecode>>>>>gu8RmtBuf:%0x,\n",gu8RmtBuf[i]);
	}
}


void midea_fan_ir_send_command_Process_start()
{
	//data not ready.
	//if(iDataReady == false)
	if(fgRF_Txd_ready == 0)
	{
		return;
	}
	//ir_tx_pin_high();
	
	//midea_fan_ir_send_command_Process();
}

void midea_fan_ir_send_command(void)
{
	iSendIdx = 0;
	remote_B = 7;
	mc_bytetime = 0;
	remote_timer = 0;
	mc_status = 0;
	mSendBitStatus = 0;
	mSendheadStatus = 0;
	//printf("call ir_send_command with iDataReady = %d\n", iDataReady);
	midea_fan_ir_send_command_Process_start();
}

/*========================================================================*/
void midea_fan_ir_init(void)
{
	//iDataReady = false;
	unFlag0.byte = 0;

	fgInitFlag = 1;

}

void midea_fan_encode_irdata(uint8_t* data)
{
	//RB_DEBUG_PRINT("midea_fan_encode_irdata :0x%.2X 0x%.2X \n",data[0],data[1]);
	midea_fan_subDisplayDecode(data);
}

void midea_fan_remote_irsend(void)
{
	//RB_DEBUG_PRINT("\n");
	//hexdump("gu8RmtBuf",gu8RmtBuf,sizeof(gu8RmtBuf));
	midea_fan_ir_send_command();
}

void midea_fan_remote_isr(void) 
{
	midea_fan_ir_send_command_Process();
	//midea_fan_ir_send_command();
}

/*===================================================================*/
// Midea send ir data
int midea_fan_RemoteSend(unsigned char *data){
	uint8_t i = 0;
	//if( (get_suport_devices() & INTERNAL_RC_MIDEA_FAN) == 0 )
	//	return -1;

	if(fgInitFlag == 0)
		midea_fan_ir_init();

	midea_fan_encode_irdata(data);
	//RB_PRINT("midea_fan_remote_ir_send start ... \n");
	midea_fan_remote_irsend();
	
	//RB_PRINT("midea_fan_remote_ir_send end \n");

	i = 0;
  while( (fgRF_Txd_ready == 1) && (i < 100) )
	{
		i++;
		uni_msleep(1);
	}
	uni_msleep(5);

	fgRF_Txd_ready = 0;	
	iSendIdx = 0;
	remote_B = 7;
	mc_bytetime = 0;
	remote_timer = 0;
	mc_status = 0;
	
	mSendBitStatus = 0;
	mSendheadStatus = 0;
	return 1;
}

/*=====================================================*/
