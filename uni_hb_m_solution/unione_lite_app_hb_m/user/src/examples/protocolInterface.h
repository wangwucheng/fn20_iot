#ifndef __PROTOCOL_INTERFACE_H__
#define __PROTOCOL_INTERFACE_H__
#define _HUWEN_UART_

#define REMOTE_IR_INTERNAL
	
typedef void *ProtocolHandle;
extern void protocolInit(ProtocolHandle *handle);
extern void protocolDestroy(ProtocolHandle handle);
extern int protocolSend(ProtocolHandle handle,int number,int src);
extern int protocolReceive(ProtocolHandle handle);
#endif