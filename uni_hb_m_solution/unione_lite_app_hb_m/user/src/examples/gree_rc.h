
#ifndef __GREE_RC_H__
#define __GREE_RC_H__
/*============================================================================================================*/
#include "greebanlv.h"
#include "ir_tx.h"
#include "bb_type.h"

void gree_remote_isr(void);

void gree_ir_init(void);
void gree_encode_irdata(uint8_t* func_data);
void gree_remote_irsend(uint8_t stype);
void gree_get_state(pAIRCOND_STATE st);


/*============================================================================================================*/

int Gree_RemoteSend(unsigned char *data);

/*============================================================================================================*/

#endif//__GREE_RC_H__
