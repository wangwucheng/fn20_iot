#ifndef INC_UNI_NLU_CONTENT_H_
#define INC_UNI_NLU_CONTENT_H_

typedef struct {
  uni_u32 key_word_hash_code; /* 存放识别词汇对应的hashcode */
  uni_u8  nlu_content_str_index; /* 存放nlu映射表中的索引，实现多个识别词汇可对应同一个nlu，暂支持256条，如果不够换u16 */
  char    *hash_collision_orginal_str /* 类似Java String equal，当hash发生碰撞时，赋值为识别词汇，否则设置为NULL */;
} uni_nlu_content_mapping_t;

enum {
  eCMD_wakeup_uni,
  eCMD_LearnMatchAC,
  eCMD_NightLampOn,
  eCMD_NightLampOff,
  eCMD_ACTurnOn,
  eCMD_ACTurnOff,
  eCMD_ACModeAuto,
  eCMD_ACModeCool,
  eCMD_ACModeHeat,
  eCMD_ACModeDry,
  eCMD_ACModeFan,
  eCMD_ACModeSleep,
  eCMD_ACWindAuto,
  eCMD_ACWindLow,
  eCMD_ACWindMid,
  eCMD_ACWindHigh,
  eCMD_ACWindInc,
  eCMD_ACWindDec,
  eCMD_ACTmp16,
  eCMD_ACTmp17,
  eCMD_ACTmp18,
  eCMD_ACTmp19,
  eCMD_ACTmp20,
  eCMD_ACTmp21,
  eCMD_ACTmp22,
  eCMD_ACTmp23,
  eCMD_ACTmp24,
  eCMD_ACTmp25,
  eCMD_ACTmp26,
  eCMD_ACTmp27,
  eCMD_ACTmp28,
  eCMD_ACTmp29,
  eCMD_ACTmp30,
  eCMD_RubbishWord,
  eCMD_ACTmpInc,
  eCMD_ACTmpDec,
  eCMD_ACSweptOn,
  eCMD_ACSweptOff,
  eCMD_ACSweptVert,
  eCMD_ACSweptCross,
  eCMD_volumeUpUni,
  eCMD_volumeDownUni,
  eCMD_ACScanStart,
  eCMD_ACScanStop,
  eCMD_SetRestore,
  eCMD_SpeakerMute,
  eCMD_SpeakerUnmute,
  eCMD_NightLampInc,
  eCMD_NightLampDec,
};

const char* const g_nlu_content_str[] = {
[eCMD_wakeup_uni] = "{\"asr_recongize\":\"小智小智\",\"text\":\"小智小智\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"wakeup_uni\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[103, 104]\"}}",
[eCMD_LearnMatchAC] = "{\"asr_recongize\":\"匹配空调\",\"text\":\"匹配空调\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"LearnMatchAC\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[107, 108, 109, 110, 111]\"}}",
[eCMD_NightLampOn] = "{\"asr_recongize\":\"打开小夜灯\",\"text\":\"打开小夜灯\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"NightLampOn\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[112]\"}}",
[eCMD_NightLampOff] = "{\"asr_recongize\":\"关闭小夜灯\",\"text\":\"关闭小夜灯\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"NightLampOff\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[113]\"}}",
[eCMD_ACTurnOn] = "{\"asr_recongize\":\"打开空调\",\"text\":\"打开空调\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTurnOn\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[114]\"}}",
[eCMD_ACTurnOff] = "{\"asr_recongize\":\"关闭空调\",\"text\":\"关闭空调\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTurnOff\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[115]\"}}",
[eCMD_ACModeAuto] = "{\"asr_recongize\":\"自动模式\",\"text\":\"自动模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACModeAuto\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[116]\"}}",
[eCMD_ACModeCool] = "{\"asr_recongize\":\"制冷模式\",\"text\":\"制冷模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACModeCool\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[117]\"}}",
[eCMD_ACModeHeat] = "{\"asr_recongize\":\"制热模式\",\"text\":\"制热模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACModeHeat\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[118]\"}}",
[eCMD_ACModeDry] = "{\"asr_recongize\":\"除湿模式\",\"text\":\"除湿模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACModeDry\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[119]\"}}",
[eCMD_ACModeFan] = "{\"asr_recongize\":\"送风模式\",\"text\":\"送风模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACModeFan\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[120]\"}}",
[eCMD_ACModeSleep] = "{\"asr_recongize\":\"睡眠模式\",\"text\":\"睡眠模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACModeSleep\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[121]\"}}",
[eCMD_ACWindAuto] = "{\"asr_recongize\":\"自动风\",\"text\":\"自动风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACWindAuto\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[122]\"}}",
[eCMD_ACWindLow] = "{\"asr_recongize\":\"低速风\",\"text\":\"低速风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACWindLow\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[123]\"}}",
[eCMD_ACWindMid] = "{\"asr_recongize\":\"中速风\",\"text\":\"中速风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACWindMid\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[124]\"}}",
[eCMD_ACWindHigh] = "{\"asr_recongize\":\"高速风\",\"text\":\"高速风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACWindHigh\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[125]\"}}",
[eCMD_ACWindInc] = "{\"asr_recongize\":\"风速大一点\",\"text\":\"风速大一点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACWindInc\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[126, 127]\"}}",
[eCMD_ACWindDec] = "{\"asr_recongize\":\"风速小一点\",\"text\":\"风速小一点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACWindDec\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[128, 129]\"}}",
[eCMD_ACTmp16] = "{\"asr_recongize\":\"十六度\",\"text\":\"十六度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp16\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[130]\"}}",
[eCMD_ACTmp17] = "{\"asr_recongize\":\"十七度\",\"text\":\"十七度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp17\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[131]\"}}",
[eCMD_ACTmp18] = "{\"asr_recongize\":\"十八度\",\"text\":\"十八度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp18\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[132]\"}}",
[eCMD_ACTmp19] = "{\"asr_recongize\":\"十九度\",\"text\":\"十九度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp19\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[133]\"}}",
[eCMD_ACTmp20] = "{\"asr_recongize\":\"二十度\",\"text\":\"二十度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp20\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[134]\"}}",
[eCMD_ACTmp21] = "{\"asr_recongize\":\"二十一度\",\"text\":\"二十一度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp21\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[135]\"}}",
[eCMD_ACTmp22] = "{\"asr_recongize\":\"二十二度\",\"text\":\"二十二度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp22\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[136]\"}}",
[eCMD_ACTmp23] = "{\"asr_recongize\":\"二十三度\",\"text\":\"二十三度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp23\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[137]\"}}",
[eCMD_ACTmp24] = "{\"asr_recongize\":\"二十四度\",\"text\":\"二十四度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp24\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[138]\"}}",
[eCMD_ACTmp25] = "{\"asr_recongize\":\"二十五度\",\"text\":\"二十五度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp25\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[139]\"}}",
[eCMD_ACTmp26] = "{\"asr_recongize\":\"二十六度\",\"text\":\"二十六度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp26\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[140]\"}}",
[eCMD_ACTmp27] = "{\"asr_recongize\":\"二十七度\",\"text\":\"二十七度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp27\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[141]\"}}",
[eCMD_ACTmp28] = "{\"asr_recongize\":\"二十八度\",\"text\":\"二十八度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp28\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[142]\"}}",
[eCMD_ACTmp29] = "{\"asr_recongize\":\"二十九度\",\"text\":\"二十九度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp29\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[143]\"}}",
[eCMD_ACTmp30] = "{\"asr_recongize\":\"三十度\",\"text\":\"三十度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmp30\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[144]\"}}",
[eCMD_RubbishWord] = "{\"asr_recongize\":\"六度\",\"text\":\"六度\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"RubbishWord\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[]\"}}",
[eCMD_ACTmpInc] = "{\"asr_recongize\":\"温度高一点\",\"text\":\"温度高一点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmpInc\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[145, 146]\"}}",
[eCMD_ACTmpDec] = "{\"asr_recongize\":\"温度低一点\",\"text\":\"温度低一点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACTmpDec\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[147, 148]\"}}",
[eCMD_ACSweptOn] = "{\"asr_recongize\":\"开启扫风\",\"text\":\"开启扫风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACSweptOn\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[149]\"}}",
[eCMD_ACSweptOff] = "{\"asr_recongize\":\"关闭扫风\",\"text\":\"关闭扫风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACSweptOff\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[150]\"}}",
[eCMD_ACSweptVert] = "{\"asr_recongize\":\"上下扫风\",\"text\":\"上下扫风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACSweptVert\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[151]\"}}",
[eCMD_ACSweptCross] = "{\"asr_recongize\":\"左右扫风\",\"text\":\"左右扫风\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACSweptCross\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[152]\"}}",
[eCMD_volumeUpUni] = "{\"asr_recongize\":\"大声点\",\"text\":\"大声点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"volumeUpUni\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[153]\"}}",
[eCMD_volumeDownUni] = "{\"asr_recongize\":\"小声点\",\"text\":\"小声点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"volumeDownUni\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[154]\"}}",
[eCMD_ACScanStart] = "{\"asr_recongize\":\"搜索空调\",\"text\":\"搜索空调\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACScanStart\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[155]\"}}",
[eCMD_ACScanStop] = "{\"asr_recongize\":\"停止搜索\",\"text\":\"停止搜索\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"ACScanStop\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[156]\"}}",
[eCMD_SetRestore] = "{\"asr_recongize\":\"空调复位\",\"text\":\"空调复位\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"SetRestore\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[157]\"}}",
[eCMD_SpeakerMute] = "{\"asr_recongize\":\"静音模式\",\"text\":\"静音模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"SpeakerMute\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[158]\"}}",
[eCMD_SpeakerUnmute] = "{\"asr_recongize\":\"对话模式\",\"text\":\"对话模式\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"SpeakerUnmute\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[159]\"}}",
[eCMD_NightLampInc] = "{\"asr_recongize\":\"亮一点\",\"text\":\"亮一点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"NightLampInc\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[160]\"}}",
[eCMD_NightLampDec] = "{\"asr_recongize\":\"暗一点\",\"text\":\"暗一点\",\"service\":\"cn.yunzhisheng.setting\",\"semantic\":{\"intent\":{\"operations\":[{\"cmd\":\"NightLampDec\"}]}},\"general\":{\"type\":\"T\",\"text\":\"\",\"pcm\":\"[161]\"}}",
};

/*TODO perf sort by hashcode O(logN), now version O(N)*/
const uni_nlu_content_mapping_t g_nlu_content_mapping[] = {
  {4282056454U/*小智小智*/, eCMD_wakeup_uni, NULL},
  {2029416162U/*匹配空调*/, eCMD_LearnMatchAC, NULL},
  {588800994U/*空调匹配*/, eCMD_LearnMatchAC, NULL},
  {391924867U/*打开小夜灯*/, eCMD_NightLampOn, NULL},
  {2433494512U/*关闭小夜灯*/, eCMD_NightLampOff, NULL},
  {3484489468U/*打开空调*/, eCMD_ACTurnOn, NULL},
  {2543416495U/*关闭空调*/, eCMD_ACTurnOff, NULL},
  {2685685105U/*自动模式*/, eCMD_ACModeAuto, NULL},
  {189670108U/*制冷模式*/, eCMD_ACModeCool, NULL},
  {3946129847U/*制热模式*/, eCMD_ACModeHeat, NULL},
  {3446463601U/*除湿模式*/, eCMD_ACModeDry, NULL},
  {2356087491U/*送风模式*/, eCMD_ACModeFan, NULL},
  {2627810873U/*睡眠模式*/, eCMD_ACModeSleep, NULL},
  {3579112124U/*自动风*/, eCMD_ACWindAuto, NULL},
  {2866186945U/*低速风*/, eCMD_ACWindLow, NULL},
  {189912901U/*中速风*/, eCMD_ACWindMid, NULL},
  {1398136482U/*高速风*/, eCMD_ACWindHigh, NULL},
  {1364655814U/*风速大一点*/, eCMD_ACWindInc, NULL},
  {978291490U/*风速小一点*/, eCMD_ACWindDec, NULL},
  {246084029U/*十六度*/, eCMD_ACTmp16, NULL},
  {263303227U/*十七度*/, eCMD_ACTmp17, NULL},
  {246024447U/*十八度*/, eCMD_ACTmp18, NULL},
  {265001314U/*十九度*/, eCMD_ACTmp19, NULL},
  {54308206U/*二十度*/, eCMD_ACTmp20, NULL},
  {2956679400U/*二十一度*/, eCMD_ACTmp21, NULL},
  {2958883934U/*二十二度*/, eCMD_ACTmp22, NULL},
  {2956947519U/*二十三度*/, eCMD_ACTmp23, NULL},
  {2959330799U/*二十四度*/, eCMD_ACTmp24, NULL},
  {2959122262U/*二十五度*/, eCMD_ACTmp25, NULL},
  {2939549575U/*二十六度*/, eCMD_ACTmp26, NULL},
  {2956768773U/*二十七度*/, eCMD_ACTmp27, NULL},
  {2939489993U/*二十八度*/, eCMD_ACTmp28, NULL},
  {2958466860U/*二十九度*/, eCMD_ACTmp29, NULL},
  {2496111085U/*三十度*/, eCMD_ACTmp30, NULL},
  {2389316420U/*六度*/, eCMD_RubbishWord, NULL},
  {2406535618U/*七度*/, eCMD_RubbishWord, NULL},
  {2389256838U/*八度*/, eCMD_RubbishWord, NULL},
  {2408233705U/*九度*/, eCMD_RubbishWord, NULL},
  {2395393784U/*十度*/, eCMD_RubbishWord, NULL},
  {263213854U/*十一度*/, eCMD_RubbishWord, NULL},
  {265418388U/*十二度*/, eCMD_RubbishWord, NULL},
  {263481973U/*十三度*/, eCMD_RubbishWord, NULL},
  {265865253U/*十四度*/, eCMD_RubbishWord, NULL},
  {265656716U/*十五度*/, eCMD_RubbishWord, NULL},
  {2845155337U/*三十一度*/, eCMD_RubbishWord, NULL},
  {2847359871U/*三十二度*/, eCMD_RubbishWord, NULL},
  {2845423456U/*三十三度*/, eCMD_RubbishWord, NULL},
  {2847806736U/*三十四度*/, eCMD_RubbishWord, NULL},
  {1284536590U/*温度高一点*/, eCMD_ACTmpInc, NULL},
  {2752587053U/*温度低一点*/, eCMD_ACTmpDec, NULL},
  {674118471U/*开启扫风*/, eCMD_ACSweptOn, NULL},
  {2484788376U/*关闭扫风*/, eCMD_ACSweptOff, NULL},
  {2387768525U/*上下扫风*/, eCMD_ACSweptVert, NULL},
  {3998566465U/*左右扫风*/, eCMD_ACSweptCross, NULL},
  {1069212788U/*大声点*/, eCMD_volumeUpUni, NULL},
  {682848464U/*小声点*/, eCMD_volumeDownUni, NULL},
  {1754941958U/*搜索空调*/, eCMD_ACScanStart, NULL},
  {955430846U/*停止搜索*/, eCMD_ACScanStop, NULL},
  {609651625U/*空调复位*/, eCMD_SetRestore, NULL},
  {1169384809U/*复位空调*/, eCMD_SetRestore, NULL},
  {1459339889U/*静音模式*/, eCMD_SpeakerMute, NULL},
  {2541562304U/*对话模式*/, eCMD_SpeakerUnmute, NULL},
  {175714954U/*亮一点*/, eCMD_NightLampInc, NULL},
  {1982669269U/*暗一点*/, eCMD_NightLampDec, NULL},
};

#endif
