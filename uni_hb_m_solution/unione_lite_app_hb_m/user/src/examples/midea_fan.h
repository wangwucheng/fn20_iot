
#ifndef __MIDEA_FAN_FAN_H__
#define __MIDEA_FAN_FAN_H__

#define MIDEA_FAN_DEV_ID	(0xF0)

#define MIDEA_FAN_FUC_ON						(1)		//风扇 打开风扇
#define MIDEA_FAN_FUC_OFF					(2)		//风扇 关闭风扇
#define MIDEA_FAN_FUC_SWINGON					(3)		//风扇 风扇摇头
#define MIDEA_FAN_FUC_SWINGOFF					(4)		//风扇 停止摇头
#define MIDEA_FAN_FUC_SPEEDINC					(5)		//风扇 调大风速
#define MIDEA_FAN_FUC_SPEEDDEC					(6)		//风扇 调小风速
#define MIDEA_FAN_FUC_SPEEDMAX					(7)		//风扇 最高档
#define MIDEA_FAN_FUC_SPEEDHIGH				(8)		//风扇 调到高档
#define MIDEA_FAN_FUC_SPEEDMID					(9)		//风扇 调到中档
#define MIDEA_FAN_FUC_SPEEDLOW					(10)		//风扇 调到低档
#define MIDEA_FAN_FUC_SPEEDMIN					(11)		//风扇 最低档
#define MIDEA_FAN_FUC_ALARM				(12)		//风扇 定时
#define MIDEA_FAN_FUC_MODE				(13)		//风扇 定时


void midea_fan_remote_isr(void);

void midea_fan_ir_init(void);
void midea_fan_encode_irdata(uint8_t* func_data);
void midea_fan_remote_irsend(void);


int midea_fan_RemoteSend(unsigned char *data);


#endif//__MIDEA_FAN_FAN_H__

