
#ifndef __AUX_MACRODEFINE_H__
#define __AUX_MACRODEFINE_H__

#define EQU					==
#define OR					||
#define AND					&&


#define macroDI				DI()
#define macroEI				EI()
#define macroNop			{					\
								NOP();			\
								NOP();			\
							}

#define macroWdtClr			{					\
								WDTE = 0xac;	\
							}
#define macroDly1us			{					\
								macroNop;		\
								macroNop;		\
								macroNop;		\
								macroNop;		\
							}

#define NOMODE				0x00	//
#define AUTOMODE			0x00	//
#define COOLMODE			0x01	//
#define DRYMODE				0x02	//
#define FANMODE				0x03	//
#define HEATMODE			0x04	//

#define NOFAN				0x00	//
#define TINYFAN				0x01	//
#define LOWFAN				0x02	//
#define MIDFAN				0x03	//
#define HIGHFAN				0x04	//
#define TURBFAN				0x05	//
#define AUTOFAN				0x08	//

#define RMTCNT				13		//

#define DISPRXDDLY_20MS		10
#define DISPTXD_250MS		125

#define DISPRXDCNT			6

#define DISPTXDCNT			6

#endif//__AUX_MACRODEFINE_H__