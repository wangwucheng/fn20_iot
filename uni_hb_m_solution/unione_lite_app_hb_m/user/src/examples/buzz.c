#include "buzz.h"
#include "buzz.h"
#include "greebanlv.h"
#include "user_sw_timer.h"
#include "user_pwm.h"
#include "unione.h"

utBytebit buzz_flag;// = {0};
utBytebit wakeup_flag;

/*music_struct *music_struct_config;
music_struct *music_struct_wakeup_config;
music_struct *music_struct_timeout_config;
music_struct *music_struct_order_config;
music_struct *music_struct_tips_config;
music_struct *music_struct_fail_config;
*/

unsigned char melody_power_on[6] ={ 1, 2, 3, 4, 5, 0};//power on melody data
unsigned char melody_wakeup[3] = {1, 3, 5};//wakeup melody data
unsigned char melody_timeout[3] = {5, 3, 1};//time out melody data
unsigned char melody_order[2] = {3, 1};// order melody data
unsigned char melody_di[4] = {1,1,1,1};// order melody data
unsigned char melody_fail[6] = {5,1,1,5,1,1};// order melody data
unsigned int  melody_note[8] = {OFF, DO, RE, MI ,FA, SO, LA, XI};//normal note

music_struct music_struct_config = { 
	sizeof(melody_power_on),
	MUSIC_BEAT, 
	&melody_note,
	&melody_power_on
	};

music_struct music_struct_wakeup_config = { 
	sizeof(melody_wakeup),
	MUSIC_WAKEUP_BEAT, 
	&melody_note,
	&melody_wakeup
	};

music_struct music_struct_timeout_config = { 
	sizeof(melody_timeout),
	MUSIC_WAKEUP_BEAT, 
	&melody_note,
	&melody_timeout
	};

music_struct music_struct_order_config = { 
	sizeof(melody_order),
	MUSIC_WAKEUP_BEAT, 
	&melody_note,
	&melody_order
	};

music_struct music_struct_tips_config = { 
	sizeof(melody_di),
	MUSIC_WAKEUP_BEAT, 
	&melody_note,
	&melody_di
	};

music_struct music_struct_fail_config = { 
	sizeof(melody_fail),
	MUSIC_WAKEUP_BEAT, 
	&melody_note,
	&melody_fail
	};

#define USR_TIMER_NUM_PWM   eTIMER5       // user timer5
#define PARTICLE_MS    2*1000       // 2ms

#define pwm_buzz_ch  PWM_NUM_2_A28              // pwm channel

void music_play(unsigned char status);

static char getflag(void) 
{	char cur_f;
	cur_f=wakeup_flag.ubyte;
	return cur_f;
}

static void buzz_play(void *args)
{
		while(1){
			music_play(wakeup_flag.ubyte);
			uni_msleep(2);
		}
	//silan_timer_to_clear(TIMER0_BASE);//clear timer interrupt flag
}

//beat init
static void music_beat_init(void)
{
	
	 //user_sw_timer_init(USR_TIMER_NUM_PWM , PARTICLE_MS);
	 //handle = user_sw_timer_add(PARTICLE_MS,false, _sw_time_buzz_cb);
	printf("music_beat_init\n");
	thread_param param;
  	uni_pthread_t pid;
  	uni_memset(&param, 0, sizeof(param));
	param.stack_size = STACK_RB_SIZE;
  	param.priority = OS_PRIORITY_REALTIME;
  	uni_strncpy(param.task_name, "buzz_play_process", sizeof(param.task_name) - 1);
	if (0 != uni_pthread_create(&pid, &param, buzz_play, NULL)) 
  		printf("create thread failed\n");
	user_pwm_init(pwm_buzz_ch, 20000, 1);//Ƶ��38k,num=0m,ռ�ձ�1
	/*int temp = 0;
	silan_timer_count(TIMER0_BASE, 0);//set the init time count to 0
	silan_timer_sel(TIMER0_BASE, 4);//set period to 4
	silan_timer_compare(TIMER0_BASE,TIMER0_2MS);//set this compare count to 2ms
	temp = silan_pic_request(PIC_IRQID_TIMER, PIC_SUBID_TIMER_0, timer0_handle); //relevance the interrupt handle to other function
	silan_timer_irq_enable(TIMER0_BASE);//enable the timer, and enable the timer interrupt
	printf("request status = %d\r\n", temp);*/
}

//start to play the melody which use the pwm control.
static void pwm_music_on(unsigned int duty, unsigned int period)
{
	//printf("pwm_music_on>>>>11>>period:%d,duty:%d>>>>>>>\n",period,duty);
	user_pwm_final(pwm_buzz_ch);
	user_pwm_init(pwm_buzz_ch, period, 1);//Ƶ��38k,num=0m,ռ�ձ�1
	if(period==20000)
		user_pwm_start(pwm_buzz_ch, 0);
	else
		user_pwm_start(pwm_buzz_ch, 50);
}


////////////////////usr area///////////////////////////////////////////
void music_deconfig(void)
{
	int i=0;
	printf("music_deconfig==\n");
	buzz_flag.ubyte = 0; 
	wakeup_flag.ubyte = 0;
	/*music_struct_config = (music_struct *)malloc(sizeof(music_struct));//
	music_struct_config->music_len = sizeof(melody_power_on);
	music_struct_config->music_beat = MUSIC_BEAT;
	music_struct_config->music_note = melody_note;
	music_struct_config->music_data = melody_power_on;


	music_struct_wakeup_config = (music_struct *)malloc(sizeof(music_struct));
	music_struct_wakeup_config->music_len = sizeof(melody_wakeup);
	music_struct_wakeup_config->music_beat = MUSIC_WAKEUP_BEAT;
	music_struct_wakeup_config->music_note = melody_note;
	music_struct_wakeup_config->music_data = melody_wakeup;
	
	music_struct_timeout_config = (music_struct *)malloc(sizeof(music_struct));
	music_struct_timeout_config->music_len = sizeof(melody_timeout);
	music_struct_timeout_config->music_beat = MUSIC_WAKEUP_BEAT;
	music_struct_timeout_config->music_note = melody_note;
	music_struct_timeout_config->music_data = melody_timeout;

	music_struct_order_config = (music_struct *)malloc(sizeof(music_struct));
	music_struct_order_config->music_len = sizeof(melody_order);
	music_struct_order_config->music_beat = MUSIC_WAKEUP_BEAT;
	music_struct_order_config->music_note = melody_note;
	music_struct_order_config->music_data = melody_order;

	music_struct_fail_config = (music_struct *)malloc(sizeof(music_struct));
	music_struct_fail_config->music_len = sizeof(melody_fail);
	music_struct_fail_config->music_beat = MUSIC_WAKEUP_BEAT;
	music_struct_fail_config->music_note = melody_note;
	music_struct_fail_config->music_data = melody_fail;

	music_struct_tips_config = (music_struct *)malloc(sizeof(music_struct));
	music_struct_tips_config->music_len = sizeof(melody_di);
	music_struct_tips_config->music_beat = MUSIC_WAKEUP_BEAT;
	music_struct_tips_config->music_note = melody_note;
	music_struct_tips_config->music_data = melody_di;
	*/

	music_beat_init();
	power_on_buzz();
	
}//////////////////////////////////////////////////////////////////


void music_play_one_bit(unsigned char melody_number, unsigned int *note)
{
	unsigned int *temp;
	if(melody_number == 0)
		return;

	else if(melody_number == 0xff)
	{
		pwm_music_on(DUTY_OFF, PERIOD_OFF);
		return;
	}
	temp = note + melody_number;
	//printf("music_play_one_bit=:%d\r\n", *temp);
	pwm_music_on((*temp)/2, (*temp));
}

static void play_melody(music_struct *music_struct_temp)
{
	static unsigned char temp = 0;
	static unsigned int cnt = 0;
	//printf("play temp%d\r\n", temp);
	if(temp != 0)
	{
	    //每一个音符的长度,按照多少个beat来计算.时间中断是2ms来一次,则计算多个中断后,才播放下一个音符.
		if(cnt++ < music_struct_temp->music_beat)
			return;
		else
			cnt = 0;
	}

	if(temp < music_struct_temp->music_len)
	{
		//printf("play temp==%d,music_data:%d\r\n", temp,*(music_struct_temp->music_data + temp));
		music_play_one_bit(*(music_struct_temp->music_data + temp), music_struct_temp->music_note);
		temp++;
	}
	else
	{
		wakeup_flag.ubyte = 0;
		temp = 0;
		pwm_music_on(DUTY_OFF, PERIOD_OFF);
	}
}

void music_play(unsigned char status)
{
#if 0
	static unsigned char old_status = 0;
	if( status != old_status)
	{
		printf("play status 0x%.2X\r\n", status);
		old_status = status;
	}
	else
	{
		return;
	}
#endif
	//printf("play status 0x%.2X\r\n", status);
	switch(status)
	{
		case POWER_ON_STATUS:
			play_melody(&music_struct_config);
			break;
		case WAKEUP_STATUS:
			play_melody(&music_struct_wakeup_config);
			break;
		case TIME_OUT_STATUS:
			play_melody(&music_struct_timeout_config);
			break;
		case ORDER_STATUS:
			play_melody(&music_struct_order_config);
			break;
		case FAIL_STATUS:
			play_melody(&music_struct_fail_config);
			break;
		case TIPS_STATUS:
			play_melody(&music_struct_tips_config);
			break;

		default:
			if(wakeup_flag.ubyte != 0)
			{
				printf("Err: music_play status = 0x%X\r\n", status);
				wakeup_flag.ubyte = 0;
				pwm_music_on(DUTY_OFF, PERIOD_OFF);
			}
			break;
	}
}

////////////usr example//////////////////////
void power_on_buzz()
{
	if(wakeup_flag.ubyte != 0)
		return;
	//MUSIC_ON(POWER_ON_FLAG);
	//POWER_ON_FLAG = 1;
	wakeup_flag.ubyte = POWER_ON_STATUS;
}

void wakeup_buzz()
{	
	if(wakeup_flag.ubyte != 0)
		return;
	//MUSIC_ON(WAKEUP_FLAG);
	//WAKEUP_FLAG = 1;
	wakeup_flag.ubyte = WAKEUP_STATUS;
}

void timeout_on_buzz()
{	
	
	if(wakeup_flag.ubyte != 0)
		return;
	//MUSIC_ON(TIME_OUT_FLAG);
	//TIME_OUT_FLAG = 1;
	wakeup_flag.ubyte = TIME_OUT_STATUS;
}

void order_buzz()
{
	
	if(wakeup_flag.ubyte != 0)
		return;
	//MUSIC_ON(ORDER_FLAG);
	//ORDER_FLAG = 1;
	wakeup_flag.ubyte = ORDER_STATUS;
}

void fail_buzz()
{	
	if(wakeup_flag.ubyte != 0)
		return;
	//MUSIC_ON(FAIL_FLAG);
	//FAIL_FLAG = 1;
	wakeup_flag.ubyte = FAIL_STATUS;
}

void tips_buzz()
{

	if(wakeup_flag.ubyte != 0)
		return;
	//MUSIC_ON(TIPS_FLAG);
	//TIPS_FLAG = 1;
	wakeup_flag.ubyte = TIPS_STATUS;
}
////////////////////////////////////////////


