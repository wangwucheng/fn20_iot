/*
 * file : led.c
 */
#include "led.h"
#include "user_gpio.h"
#include "user_sw_timer.h"
#include "unione.h"

uint8_t led_b_blink=0;
uint8_t led_r_blink=0;
uint8_t f_led_b_blink=0;
uint8_t f_led_r_blink=0;
uint8_t led_b_status=0;
uint8_t led_r_status=0;
static volatile int ledb_timeout = 0;//1.3s
static volatile int ledr_timeout = 0;//1.3s
//static volatile int f_b_cnt = 200;//400ms
#define USR_TIMER_LED_NUM  eTIMER5  // user timer2
#define LED_MS    2000       // 2MS

static void led_ctrl_sw_timer_cb(eTIMER_IDX timer) 
{
	if((ledb_timeout>0) || (ledr_timeout>0))
	{
		if(ledb_timeout>0)
		{
			ledb_timeout--;
			if(ledb_timeout==0)
			{
				LED_B_OnOff(OFF);
			}
		}
		if(ledr_timeout>0)
		{
			ledr_timeout--;
			if(ledr_timeout==0)
			{
				LED_R_OnOff(OFF);
			}
		}
	}	
	else
	{
		if(f_led_b_blink==1)
		{
			led_b_blink++;
			if (led_b_blink==200)
			{
				LED_B_OnOff(!led_b_status);
			}
		}
		if(f_led_r_blink==1)
		{
			led_r_blink++;
			if (led_r_blink==200)
			{
				LED_R_OnOff(!led_r_status);
			}
		}

	}		
}

void rb_init_led(void)
{
  	user_timer_init(USR_TIMER_LED_NUM, LED_MS, false,led_ctrl_sw_timer_cb);
	user_timer_start(USR_TIMER_LED_NUM);
	user_gpio_set_mode(GPIO_NUM_A25, GPIO_MODE_OUT);
	user_gpio_set_mode(GPIO_NUM_A26, GPIO_MODE_OUT);
	LED_R_OnOff(OFF);
	LED_B_OnOff(OFF);
	LED_R_OnTimeout(1300);
	printf("rb init led\r\n");
}

void LED_R_OnOff(int onoff)
{ 
	if(onoff)
		user_gpio_set_value(GPIO_NUM_A25,0);
	else
		user_gpio_set_value(GPIO_NUM_A25,1);
	led_r_status=onoff;
}

void LED_B_OnOff(int onoff)
{
	if(onoff)
		user_gpio_set_value(GPIO_NUM_A26,0);
	else
		user_gpio_set_value(GPIO_NUM_A26,1);
	led_b_status=onoff;
}

void LED_R_OnTimeout(int ms)
{
	LED_R_OnOff(ON);
	ledr_timeout=ms/2;
}

void LED_B_OnTimeout(int ms)
{
	LED_B_OnOff(ON);
	ledb_timeout=ms/2;
}

void LED_B_Blink(int onoff)
{	LED_B_OnOff(ON);
	f_led_b_blink=1;
}

void LED_R_Blink(int onoff)
{	LED_R_OnOff(ON);
	f_led_r_blink=1;
}

void LED_RB_AllOff(void)
{
	ledb_timeout=-1;
	ledr_timeout=-1;
	f_led_b_blink=0;
	f_led_r_blink=0;
	LED_B_OnOff(OFF);
	LED_R_OnOff(OFF);
}

//=======================================================
void WiFi_OnOff(int onoff)
{
	LED_R_Blink(ON);
	LED_B_Blink(ON);
}

/*void WiFi_Reset(void)
{
	WiFi_OnOff(OFF);
	silan_mdelay(50);
	WiFi_OnOff(ON);
}*/
//=======================================================
