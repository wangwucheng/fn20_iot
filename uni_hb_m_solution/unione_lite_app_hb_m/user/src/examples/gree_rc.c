
#define ISR_PERIOD	TIMER2_PERIOD
//#define ISR_PERIOD	TIMER2_50US_PERIOD

/* Ver 1.0 */
#define VER_Main    0x01
#define VER_Sub     0x00
#include "gree_rc.h"
#include "unione.h"
//============================================================================================================
typedef union {
    uint8_t Byte;
    struct {
		uint8_t b0       :1;
		uint8_t b1       :1;
		uint8_t b2       :1;
		uint8_t b3       :1;
		uint8_t b4       :1;
		uint8_t b5       :1;
		uint8_t b6       :1;
		uint8_t b7       :1;
  	} Bits;
}_tByteBits;

/* main */
static volatile _tByteBits UserMainFlag;
#define fIrNeedSend	    UserMainFlag.Bits.b0 //IR data is waitting for send, clear when send option done
#define isWakeup	    UserMainFlag.Bits.b1 //set when uart recieve a wakeup signal
#define isWakeupSend	UserMainFlag.Bits.b2 //set when ir send something, auto clear by rgb statemachine
#define irSendRunning   UserMainFlag.Bits.b3 //set when uart recieve something(except wakeup), clear when ir send option done
#define irSendOrRecieve UserMainFlag.Bits.b4 //flag for 125us interrupt to find whitch option to be done between send and recieve
#define irRecieveDone   UserMainFlag.Bits.b5 //set when ir recieved a package data, clear when the data have be saved
#define irIsIrUpdated   UserMainFlag.Bits.b6 //set after IR data updated with yaokongqi
//#define irIsSendF2      UserMainFlag.Bits.b7 //set 0 before IR send, set 1 after send F2

static volatile uint8_t send_type;
#define F0 0
#define F2 2

uint8_t LocalMode;
uint8_t LocalSwapUDLR;
uint8_t LocalTemp[5] = {0x0A, 0x0A, 0x0A, 0x0A, 0x0A};
const uint8_t F0Fanspeed[6] = {0x00, 0x10, 0x10, 0x20, 0x30, 0x30};
uint8_t LocalFanspeed[5];
uint8_t LocalF2Fanspeed[5];
_tByteBits *AllCode;
_tByteBits F0Code[10];
_tByteBits F2Code[10];

#ifndef REMOTE_IR_INTERNAL
/* UART */
volatile uint8_t * gp_uart0_tx_address;         /* uart0 send buffer address */
volatile uint16_t  g_uart0_tx_count;            /* uart0 send data number */
volatile uint8_t * gp_uart0_rx_address;         /* uart0 receive buffer address */
volatile uint16_t  g_uart0_rx_count;            /* uart0 receive data number */
volatile uint16_t  g_uart0_rx_length;           /* uart0 receive data length */
uint8_t rec_ok;
uint8_t send_buf[6];
uint8_t rec_buf[6];
#pragma interrupt r_uart0_interrupt_send(vect=INTST0)
#pragma interrupt r_uart0_interrupt_receive(vect=INTSR0)
#else
uint8_t send_buf[6];
#endif

/* IR*/
#if 0
#define READ_IR        (P13>>7)
_tByteBits IRBuf[10];
uint8_t ir_step;                            //ir recieve step
uint16_t ir_code_cnt;                       //ir recieve time count
uint8_t IRBitCount;							//
//uint8_t IRByteCount;						//

_tByteBits IrReceiveStatus;
#define ir_pin_old		        IrReceiveStatus.Bits.b0
#define ir_pin_now		        IrReceiveStatus.Bits.b1
#define ir_risingedge 	        IrReceiveStatus.Bits.b2
#define ir_fallingedge	        IrReceiveStatus.Bits.b3
#define f_IR_Err		        IrReceiveStatus.Bits.b4

#define IR_WaitHeadStart	0
#define IR_WaitHeadLowEnd	1
#define IR_WaitHeadEnd		2
#define IR_ReceiveData		3

#define C_IRZEROMIN		17	//0.875ms
#define C_IRZEROMAX		30	//1.5ms
#define C_IRONEMIN		40	//2.0ms
#define C_IRONEMAX		50	//2.5ms
#endif
#define REMOTE_IR_INTERNAL
#ifndef REMOTE_IR_INTERNAL
#define IR_L {TOE0 |= _02_TAU_CH1_OUTPUT_ENABLE; }
#define IR_H {TOE0 &= (uint8_t)~_02_TAU_CH1_OUTPUT_ENABLE; TO0 &= 0xFD;}
#else
#define IR_L {ir_tx_pin_low();}
#define IR_H {ir_tx_pin_high();}
#endif
static volatile uint8_t send_BitCount;
static volatile uint16_t send_time_tick;
static volatile uint8_t send_step;
#define SEND_HEAD       0
#define SEND_FCODE      1
#define SEND_20MS_L     2
#define SEND_AFTER_DELAY 3
#define SEND_END        4

/* RGB */
static volatile _tByteBits tick_xxus;
static volatile _tByteBits tick_10us;
static volatile _tByteBits tick_50us;
static volatile _tByteBits tick_1ms;
uint8_t DelayTime;
uint8_t RGB_STATE;
uint8_t WatchDogTime;
uint8_t ResetDelayTime;
// P02(green), P40(red), P03(reset output)
uint8_t RGB_B_ST;
#ifndef REMOTE_IR_INTERNAL
#define RGB_R_OFF    {P4 = 0x01;}
#define RGB_R_ON     {P4 = 0x00;}
#define RGB_B_OFF    {P0 |= 0x04;}
#define RGB_B_ON     {P0 &= 0xFB;}
#define RGB_B_TOG    {P0 ^= 0x08;}
#define RESET_EN     {P0 |= 0x08;}
#define RESET_DS     {P0 &= 0xF7;}
#else
#define RGB_R_OFF    {}
#define RGB_R_ON     {}
#define RGB_B_OFF    {}
#define RGB_B_ON     {}
#define RGB_B_TOG    {}
#define RESET_EN     {}
#define RESET_DS     {}
#endif

static  uint8_t fgInitFlag = 0;

/*============================================================================================================*/
void gree_ir_init(void)
{
	UserMainFlag.Byte = 0;

	/* var init */
	isWakeup = 0;
	isWakeupSend = 0;
	F0Code[0].Byte = 0x00;	F0Code[1].Byte = 0x0b;	F0Code[2].Byte = 0x20;	F0Code[3].Byte = 0x50;
	F0Code[4].Byte = 0x0A;	F0Code[5].Byte = 0x00;	F0Code[6].Byte = 0x04/*wifi*/;	F0Code[7].Byte = 0x00;
	F0Code[8].Byte = 0x00;

	F2Code[0].Byte = 0x00;	F2Code[1].Byte = 0x0b;	F2Code[2].Byte = 0x20;	F2Code[3].Byte = 0x70;
	F2Code[4].Byte = 0x0A;	F2Code[5].Byte = 0x00;	F2Code[6].Byte = 0x00;	F2Code[7].Byte = 0x00;
	F2Code[8].Byte = 0x00;

	send_buf[0] = 0x7B;
	send_buf[5] = 0x8F;

	/* IR init */
	fIrNeedSend = 0;
	irSendOrRecieve = 0;
	irIsIrUpdated = 0;
	send_step = SEND_END;
	send_type = F0;

	/* RGB init */
	RGB_STATE = 0;
	RGB_B_ST = 0;
	tick_xxus.Byte = 0;
	tick_10us.Byte = 0;
	tick_50us.Byte = 0;
	tick_1ms.Byte = 0;
	DelayTime = 50;
	WatchDogTime = 50;
	ResetDelayTime = 0;
	LocalMode = 1;

	fgInitFlag = 1;
}

/*============================================================================================================*/
void gree_remote_isr(void)
{
    /* time base ==========================10us=================================*/
#if 0
    tick_xxus.Byte ++;
    if(tick_xxus.Byte >= TICK_1MS)
    {// 1ms
        tick_xxus.Byte = 0;
        tick_1ms.Byte++;

        if(tick_1ms.Byte >= 100) //100ms
        {
            tick_1ms.Byte = 0;
            if(DelayTime > 0) DelayTime--;

            if(WatchDogTime > 0)
            {
                WatchDogTime--;
            }
            else
            {
                WatchDogTime = 50;
                ResetDelayTime = 10;
            }

            if(ResetDelayTime > 0)
            {
                ResetDelayTime--;
                RESET_DS // reset
            }
            else
            {
                RESET_EN //no reset
            }
        }
    }

    /* rgb ============================================================================================================*/
    /*
    if(ResetDelayTime > 0)
    {
        ResetDelayTime--;
        RGB_R_ON
        RGB_B_ON
    }
    else
    */
    {
        if(RGB_STATE == 1)//default state, wait for uart data to wakeup
        {
            RGB_R_OFF
            RGB_B_OFF
            RGB_B_ST = 0;
            if(isWakeup)
            {
                RGB_STATE = 2;
                DelayTime = 0;
            }
        }
        else if(RGB_STATE == 2)//wakeup state
        {
            RGB_R_OFF
            if(DelayTime == 0)
            {
                DelayTime = 4;
                //RGB_B_TOG
                if(RGB_B_ST == 1)
                {
                    RGB_B_ST = 0;
                    RGB_B_OFF
                }
                else
                {
                    RGB_B_ST = 1;
                    RGB_B_ON
                }
            }

            if(isWakeupSend)
            {
                RGB_STATE = 3;
                DelayTime = 10;
            }
            else
            {
                if(isWakeup == 0)
                    RGB_STATE = 1;
            }
        }
        else if(RGB_STATE == 3) //wakeupsend state
        {
            RGB_R_OFF
            RGB_B_ON
            RGB_B_ST = 1;
            if(isWakeupSend)
            {
                DelayTime = 10;
            }
            else
            {
                if(DelayTime == 0)
                {
                    RGB_STATE = 2;
                }
            }
        }
        else if(RGB_STATE == 0)//init state, only run one time when system startup
        {
            RGB_R_ON
            RGB_B_OFF
            RGB_B_ST = 0;
            if(DelayTime==0)
            {
                RGB_STATE = 1;
            }
            if(isWakeup)
            {
                RGB_STATE = 2;
                DelayTime = 0;
            }
        }
    }
#endif

    /* IR */
    if(irSendOrRecieve == 1)
    {//Send============================================================================================================
        if(irSendRunning == 1)
        {
            send_time_tick++;
            if(send_step == SEND_HEAD)
            {
            	if(send_type == F2){//不知道为什么F2码头总是多2ms
					if(send_time_tick <= (7000/TIMER2_PERIOD)/*178*/)
                    	IR_L          //9ms
                	else if(send_time_tick < (11500/TIMER2_PERIOD)/*271*/)
                	//printf("send_time_tick=====:%d\n",send_time_tick)
                    	IR_H    //13.5ms
                	else
                	{
                    	send_time_tick = 0;
                    	send_step = SEND_FCODE;
					//RB_DEBUG_PRINT("SEND_FCODE\n");
					//RB_DEBUG_PRINT(">0x%.2X\n",AllCode[send_BitCount>>3].Byte);
                	}
				}else{
                	if(send_time_tick <= (9000/TIMER2_PERIOD)/*178*/)
                   		IR_L          //9ms
                	else if(send_time_tick < (13500/TIMER2_PERIOD)/*271*/)
                   		IR_H    //13.5ms
                	else
                	{
                    	send_time_tick = 0;
                    	send_step = SEND_FCODE;
                	}
            	}
            }
            else if(send_step == SEND_FCODE)
            {
                if((AllCode[send_BitCount>>3].Byte>>(send_BitCount&0x07))&0x01)
                {
                    if(send_time_tick <= (630/TIMER2_PERIOD)/*14*/)
                        IR_L        //0.63ms
                    else if(send_time_tick <= (2290/TIMER2_PERIOD)/*44*/)
                        IR_H   //2.29ms
                    else
                    {
                        send_time_tick = 0;
                        send_BitCount++;
						//if( (send_BitCount & 0x07 ) == 0)
						//{
						//	RB_DEBUG_PRINT(">0x%.2X\n",AllCode[send_BitCount>>3].Byte);
						//}
                        if(send_BitCount == 36) send_step = SEND_20MS_L;
                    }
                }
                else
                {
                    if(send_time_tick <= (630/TIMER2_PERIOD)/*14*/)
                        IR_L        //0.63ms
                    else if(send_time_tick <= (1190/TIMER2_PERIOD)/*23*/)
                        IR_H        //1.19ms
                    else
                    {
                        send_time_tick = 0;
                        send_BitCount++;
						//if( (send_BitCount & 0x07 ) == 0)
						//{
						//	RB_DEBUG_PRINT(">0x%.2X\n",AllCode[send_BitCount>>3].Byte);
						//}
                        if(send_BitCount == 36) send_step = SEND_20MS_L;
                    }
                }
                if(send_BitCount > 68)
                {
                    send_time_tick = 0;
                    IR_H
                    send_step = SEND_AFTER_DELAY;
                }
            }
            else if(send_step == SEND_20MS_L)
            {
                if(send_time_tick >= (18500/TIMER2_PERIOD)/*369*/)
                {
                    send_time_tick = 0;
                    send_step = SEND_FCODE;
					//RB_DEBUG_PRINT("SEND_FCODE\n");
                }
            }
            else if(send_step == SEND_AFTER_DELAY)
            {
                if(send_time_tick >= (35000/TIMER2_PERIOD)/*700*/)
                {
                    send_step = SEND_END;
					if(send_type == F2)
					{
						isWakeupSend = 0;
						fIrNeedSend = 0;
					}

					irSendRunning = 0;
					irSendOrRecieve = 0;
					//RB_DEBUG_PRINT("SEND_END\n");
                }
            }
        }
    }
    else
    {//Recieve============================================================================================================
#if 0
        if(irRecieveDone != 1)
        {
            ir_code_cnt ++;
            if(ir_code_cnt == 4000) //200ms
	        {
		        f_IR_Err = 0;
		        ir_step = IR_WaitHeadStart;
	        }

            ir_pin_now = READ_IR;


	        ir_risingedge = ((ir_pin_now) && (ir_pin_now != ir_pin_old)) ? 1 : 0;
	        ir_fallingedge = ((!ir_pin_now) && (ir_pin_now != ir_pin_old)) ? 1 : 0;
	        ir_pin_old = ir_pin_now;

            if(ir_step == IR_WaitHeadStart)
          ����D  	           �   @�        @             @     �                           �          �            �                                        �                                  �                                           � �                                           �                                �                @         @    �         �                          @              �   �                @�       �     @                              @ @                          �                                                                                           @  �         @   �        @           �                                       �               @             @                       �   �      �?                 �  0 �                       @                         �          @    @                                             @      @             P                   �C                       @          @           �        �                      (    @      @                  �                           @      �                       �                                        (            @  �                                    �          �                              @   @           �                                      @           @ � �       �            �            �                         �   �  �  �          �           H   
                                 @         F         @                         �             �                       �  @                            �      �                                       @      �                 �             �                                        ��                 �  @                        D     �                 � �              �                 @                                             @              BitCount >= 68)
				    {
					    IRBitCount = 0;
					    ir_step = IR_WaitHeadStart;

					    irRecieveDone = 1;
				    }
				    if(f_IR_Err == 1)
				    {
					    IRBitCount = 0;
					    f_IR_Err = 0;
					    ir_step = IR_WaitHeadStart;
				    }
			    }
            }
        }
#endif
    }
}

/*============================================================================================================*/

void gree_encode_irdata(uint8_t* func_data)
{
	//RB_DEBUG_PRINT("++\n");
	//RB_DEBUG_PRINT("fIrNeedSend = %d, irSendRunning = %d, irSendOrRecieve = %d, send_step = %d, send_type = %d\n",
	//		fIrNeedSend,irSendRunning,irSendOrRecieve,send_step,send_type);
	if( fIrNeedSend == 1 )
		return;

	send_step = SEND_END;
	send_type = F0;
	irSendOrRecieve = 0;
	irSendRunning = 0;

	WatchDogTime = 50;
	ResetDelayTime = 0;

	send_buf[1] = func_data[1] + 0x10;
	if (func_data[1] == 0x09)
	{
		send_buf[2] = VER_Main ;
		send_buf[3] = VER_Sub ;
	}
	else
	{
		send_buf[2] = func_data[2] ;
		send_buf[3] = func_data[3] ;
	}

	send_buf[4] = send_buf[0] ^ send_buf[1] ^ send_buf[2] ^ send_buf[3];

	if((func_data[1] != 0x00) && (func_data[1] != 0x01) && (func_data[1] != 0x09))
	{
		isWakeup = 1;
		irSendRunning = 0;
		AllCode = F0Code;
		fIrNeedSend = 1;
	}

	if(func_data[1] == 0x01) //just wakeup
	{
		isWakeup = (func_data[3]==0)?0:1;
	}
	else if(func_data[1] == 0x02) //wakeup and set on off
	{
		F0Code[0].Bits.b3 = func_data[2]&0x01;
	}
	else if((func_data[1] == 0x03) && (F0Code[0].Bits.b3 == 1) &&(func_data[2]<0x10)) //wakeup and add or dec temp
	{
		if(func_data[2] == 0x01)
		{
			if(LocalTemp[LocalMode] < 0x0E) LocalTemp[LocalMode]++;
		}
		else
		{
			if(LocalTemp[LocalMode] > 0x00) LocalTemp[LocalMode]--;
		}
	}
	else if((func_data[1] == 0x04) && (F0Code[0].Bits.b3 == 1)) //set temp
	{
		if((func_data[2]>0x0f))
		LocalTemp[LocalMode] = ((func_data[2]-3));
		else
		LocalTemp[LocalMode] = ((func_data[2]-1));	
	}
	else if((func_data[1] == 0x05) && (F0Code[0].Bits.b3 == 1)) //mode
	{
		LocalMode = (func_data[2]-1)&0x07;
	}
	else if((func_data[1] == 0x06) && (F0Code[0].Bits.b3 == 1)) //Fanspeed
	{
		if(func_data[2] < 6)
		{
			LocalFanspeed[LocalMode] = F0Fanspeed[func_data[2]];
			LocalF2Fanspeed[LocalMode] = func_data[2];
		}
		else if(func_data[2] == 6)
		{
			if(LocalFanspeed[LocalMode] < 0x30)LocalFanspeed[LocalMode]+=0x10;
			if(LocalF2Fanspeed[LocalMode] < 0x05)LocalF2Fanspeed[LocalMode]++;
		}
		else if(func_data[2] == 7)
		{
			if(LocalFanspeed[LocalMode] > 0x1F)LocalFanspeed[LocalMode]-=0x10;
			if(LocalF2Fanspeed[LocalMode] > 1)LocalF2Fanspeed[LocalMode]--;
		}
		else if(func_data[2] == 8)
		{
			LocalFanspeed[LocalMode] = 0x30;
			LocalF2Fanspeed[LocalMode] = 0x05;
		}
		else
		{
			LocalFanspeed[LocalMode] = 0x10;
			LocalF2Fanspeed[LocalMode] = 0x01;
		}
	}
	else if((func_data[1] == 0x07) && (F0Code[0].Bits.b3 == 1)) //fan swap
	{
		switch(func_data[2])
		{
			case 0x03:
				LocalSwapUDLR |= 0x10;
				F0Code[0].Bits.b6 = 1;
				break;
			case 0x04:
				LocalSwapUDLR &= 0x0F;
				F0Code[0].Bits.b6 = 0;
				break;
			case 0x01:
				LocalSwapUDLR |= 0x01;
				F0Code[0].Bits.b6 = 1;
				break;
			case 0x02:
				LocalSwapUDLR &= 0xF0;
				F0Code[0].Bits.b6 = 0;
				break;
			default: break;
		}
	}
	else if(func_data[1] == 0x08)
	{
		if(func_data[2] == 0x02)
		{
			F0Code[2].Bits.b5 = 0;
		}
		else
		{
			F0Code[2].Bits.b5 = 1;
		}
	}

	//RB_DEBUG_PRINT("--\n");
}


/*============================================================================================================*/
void gree_remote_irsend(uint8_t stype)
{
	uint8_t i;

	//RB_DEBUG_PRINT("++\n");
	if( fIrNeedSend == 0 )
		return;

	if(stype == F2)
	{
		AllCode = F2Code;
		send_type = F2;
	}
	else
	{
		AllCode = F0Code;
		send_type = F0;
	}

	//RB_DEBUG_PRINT("fIrNeedSend = %d, irSendRunning = %d, irSendOrRecieve = %d, send_step = %d, send_type = %d\n",
	//		fIrNeedSend,irSendRunning,irSendOrRecieve,send_step,send_type);
	if(irSendRunning == 0)
	{
#if 0
		if(ir_step < IR_ReceiveData) //skip when recieve running, otherwise change to ir send mode
#endif
		{
			F0Code[0].Byte &= 0xC8;
			F0Code[0].Byte |= LocalMode | LocalFanspeed[LocalMode];
			F0Code[1].Byte &= 0xF0;
			F0Code[1].Byte |= LocalTemp[LocalMode];

			F2Code[0].Byte = F0Code[0].Byte;
			F2Code[1].Byte = F0Code[1].Byte;
			F2Code[2].Byte = F0Code[2].Byte;
			F2Code[3].Bits.b0 = F0Code[3].Bits.b0;
			F2Code[3].Bits.b1 = F0Code[3].Bits.b1;
			F2Code[3].Bits.b2 = F0Code[3].Bits.b2;
			F2Code[3].Bits.b3 = F0Code[3].Bits.b3;

			F0Code[4].Byte = 0x0A;
			F0Code[4].Bits.b4 = LocalSwapUDLR>>4; //LocalSwapRL -> 0000 or 0001, so ignor
			F0Code[5].Byte &= 0xF0;
			F0Code[5].Bits.b0 = (LocalSwapUDLR & 0x01);

			//Code7 => 16 | 15
			F2Code[7].Byte &= 0xF8;
			F2Code[7].Byte |= LocalF2Fanspeed[LocalMode]&0x07;

			F0Code[8].Byte = 0; F2Code[8].Byte = 0;
			for(i=0;i<8;i++)
			{
				F0Code[8].Byte += F0Code[i].Byte&0x0F;
				F2Code[8].Byte += F2Code[i].Byte&0x0F;
			}
			F0Code[8].Byte &= 0x0F; F2Code[8].Byte &= 0x0F;

			irSendOrRecieve = 1;
#if 0
			ir_step = 0;
#endif
			send_time_tick = 0;
			send_BitCount = 0;
			send_step = 0;

			irSendRunning = 1;
			irSendOrRecieve = 1;
		} //End of ir_step < IR_ReceiveData
	} //End of irneedsend but irSendRunning == 0


	//RB_DEBUG_PRINT("fIrNeedSend = %d, irSendRunning = %d, irSendOrRecieve = %d, send_step = %d, send_type = %d\n",
	//		fIrNeedSend,irSendRunning,irSendOrRecieve,send_step,send_type);
	//silan_timer_irq_reset(TIMER2_BASE);
	//user_timer_resume(1);
	//ir_tx_pin_high();

	uni_msleep(2);
	//RB_DEBUG_PRINT("--\n");
}

/*============================================================================================================*/
void gree_remote_irsend2(void)
{
	uint8_t i = 0;

	//RB_DEBUG_PRINT("++\n");

	if( fIrNeedSend == 0 )
		return;

  	while( (send_step != SEND_END) && (i < 100) )
	{
		i++;
		uni_msleep(200);
	}

	//RB_DEBUG_PRINT(">>>\n");

	//RB_DEBUG_PRINT("fIrNeedSend = %d, irSendRunning = %d, irSendOrRecieve = %d, send_step = %d, send_type = %d\n",
	//		fIrNeedSend,irSendRunning,irSendOrRecieve,send_step,send_type);

	gree_remote_irsend(F2);

	//RB_DEBUG_PRINT("fIrNeedSend = %d, irSendRunning = %d, irSendOrRecieve = %d, send_step = %d, send_type = %d\n",
	//		fIrNeedSend,irSendRunning,irSendOrRecieve,send_step,send_type);

	//RB_DEBUG_PRINT("--\n");
}
/*============================================================================================================*/
void gree_get_state(pAIRCOND_STATE st)
{
	//RB_DEBUG_PRINT(">>>\n");
	//st->device = 1;
	st->onoff = F0Code[0].Bits.b3;
	st->mode = LocalMode;
	st->settemp = LocalTemp[LocalMode]+16;
	if( LocalF2Fanspeed[LocalMode] > 1)
		st->fanspeed = LocalF2Fanspeed[LocalMode] - 1;
	else
		st->fanspeed = 0;

	if(LocalMode == 0)
	{
		if(st->fanspeed == 0)
			st->fanspeed = 1;
		if(st->fanspeed > 4)
			st->fanspeed = 4;
	}

	if(LocalMode == 3)
	{
		if(st->fanspeed == 0)
			st->fanspeed = 1;
	}


	st->fanmode = ( (LocalSwapUDLR >> 4)&0x01 ) | ( (LocalSwapUDLR << 4)& 0x10 );

	st->light_onoff = F0Code[2].Bits.b5;
}
/*============================================================================================================*/

//Gree send data
int Gree_RemoteSend(unsigned char *data){
	uint8_t i = 0;

	if( (get_suport_devices() & INTERNAL_RC_GREE) == 0 )
		return -1;

	if(fgInitFlag == 0)
		gree_ir_init();
	gree_encode_irdata(data);
	printf(" Gree...start ... \n");
	gree_remote_irsend(F0);

	gree_remote_irsend2();
	printf("Gree... end \n");

  	while( (send_step != SEND_END) && (i < 100) )
	{
		i++;
		uni_usleep(200);
	}
	//printf(" send_step = %d \n",send_step);
	uni_usleep(600);
	//silan_udelay(150000);

	isWakeupSend = 0;
	fIrNeedSend = 0;

	irSendRunning = 0;
	irSendOrRecieve = 0;
	send_step = 0;

	return 1;
}

/*============================================================================================================*/


