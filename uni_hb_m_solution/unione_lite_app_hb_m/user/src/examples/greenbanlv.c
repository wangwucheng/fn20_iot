/*******************************************************************************
*   File Name:   	greenbanlv.c
*   Description:	none
*   Atuhor:       buck(308101150@qq.com)
*   Date:         2020/08/20
*   Version:      2020/128/20   v1.0
*******************************************************************************/
#include "asr_id.h"
#include "greebanlv.h"
#include "gree_rc.h"
#include "midea_rc2.h"
#include "aux_rc.h"
#include "buzz.h"
#include "led.h"
#include "iot_ctaux.h"

#ifdef DEV_SORT_ARRAY
extern int Dev_Arr[] = DEV_SORT_ARRAY;
#else
extern int Dev_Arr[] = {
	INTERNAL_RC_GREE,
	INTERNAL_RC_MIDEA2,
	INTERNAL_RC_AUX,
	INTERNAL_RC_TCL,
	INTERNAL_RC_HAIER,
	INTERNAL_RC_HISENSE,
	INTERNAL_RC_CHIGO,
	INTERNAL_RC_DOTELS,
	INTERNAL_RC_DAIKIN,
	INTERNAL_RC_MITSUBISHI,
	0x00
};
#endif
#define Dev_Arr_SIZE	(sizeof(Dev_Arr)/sizeof(Dev_Arr[0]))
	
int g_dev_wakeup = 0;
unsigned int g_suport_devices = 0;
unsigned char g_bb_sn[SN_SIZE+1] = {0};
ctaux_air_status g_ataux_air_status_bsp={0x00,0x82,0x00,0x02,0x1a,0x02,0x01,0x01,0x01,0x01};

unsigned char * get_devices_sn( void )
{
	return g_bb_sn;
}

unsigned int cal_suport_devices(void)
{
	int i = 0;
	for( i = 0; i < Dev_Arr_SIZE;i++ )
	{
		g_suport_devices |= Dev_Arr[i];
	}
	return g_suport_devices;
}

unsigned int get_suport_devices(void)
{
	if(g_suport_devices == 0x00 )
		cal_suport_devices();
	return g_suport_devices;
}

unsigned int set_suport_devices(unsigned int devices)
{
	int i = 0;
	if( devices != Dev_Arr[0] )
	{
		for( i = 1; i < Dev_Arr_SIZE;i++ )
		{
			if( devices == Dev_Arr[i] )
			{
				break;
			}
		}

		if(i < Dev_Arr_SIZE )
		{
			Dev_Arr[i] = Dev_Arr[0];
		}
		else
		{
			for( i = 1; i < Dev_Arr_SIZE;i++ )
			{
				Dev_Arr[Dev_Arr_SIZE - i] = Dev_Arr[Dev_Arr_SIZE - i - 1];
			}
		}

		Dev_Arr[0] = devices;
	}
		
	g_suport_devices = cal_suport_devices();

#ifdef FLASH_USER_CFG_USED
	update_usrcfg_dev_sort(Dev_Arr[0],Dev_Arr[1],Dev_Arr[2]);
#endif

	printf(":0x%.4X\n",g_suport_devices);
	return g_suport_devices;
}


//XOR data check
static unsigned char dataCheck(unsigned char *p, unsigned char num){
	unsigned char checkValue=0;
	unsigned char i;
	for(i=0; i< num; i++){
		checkValue ^= *p; p++;
	}
	return checkValue;
}

void buzz_control(unsigned int number)
{		
	switch(number)
	{
		case ASR_ID_WAKEUP1:
		case ASR_ID_WAKEUP2:
		case ASR_ID_WAKEUP3:
		case ASR_ID_WAKEUP4:
			////////////////////////////////////
			wakeup_buzz();//wakeup melody start
			LED_RB_AllOff();
			LED_B_Blink(ON);
			////////////////////////////////////
			break;

		case ASR_ID_TIME_OUT:
			////////////////////////////////////
			timeout_on_buzz();//timerout for usr
			LED_RB_AllOff();
			////////////////////////////////////
			break;
		default:
			/////////////////////////////////
			order_buzz();//order melody start
			LED_B_OnTimeout(1300);
			/////////////////////////////////
			break;
	}
}

extern void ir_melodyInit(void){
	// IR init
	printf("ir_melodyInit=11=\n");
	//melody init
	music_deconfig();
}

void XXX_RemoteSend(unsigned int device,unsigned char *data)
{	
	switch(device)
	{
		case INTERNAL_RC_GREE:
			Gree_RemoteSend(data);
			break;

		case INTERNAL_RC_MIDEA2:
			Midea2_RemoteSend(data);
			break;

		case INTERNAL_RC_AUX:
			Aux_RemoteSend(data);
			break;
		default:
			printf("not support [0x%04X]\n",device);
			break;		
	}
}

void Sort_RemoteSend(unsigned char *data)
{
	int i = 0;
	for( i = 0; i < Dev_Arr_SIZE;i++ )
	{
		XXX_RemoteSend(Dev_Arr[i],data);
	}
	/*unsigned char test_data1[]={0x5A,0x02,0x01,0x01,0x58,0xEF}; //打开
	unsigned char test_data2[]={0x5A,0x04,0x0C,0x01,0x53,0xEF};//27度
	unsigned char test_data3[]={0x5A,0x02,0x02,0x01,0x5B,0xEF};//关闭
	printf(":0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X \n",data[0],data[1],data[2],data[3],data[4],data[5]);
	//Aux_RemoteSend(test_data1);
	//Aux_RemoteSend(test_data2);
	//Aux_RemoteSend(test_data3);
	Aux_RemoteSend(data);
	printf(":AUX_HOU::0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X \n",data[0],data[1],data[2],data[3],data[4],data[5]);
	//Gree_RemoteSend(test_data1);
	//Gree_RemoteSend(test_data2);
	//Gree_RemoteSend(data);
	Gree_RemoteSend(data);
	printf(":GREE_HOU::0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X \n",data[0],data[1],data[2],data[3],data[4],data[5]);
	//Midea2_RemoteSend(test_data1);
	//Midea2_RemoteSend(test_data2);
	Midea2_RemoteSend(data);
	printf("MEIDI2_HOU:0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X \n",data[0],data[1],data[2],data[3],data[4],data[5]);
	//Midea2_RemoteSend(data);
	*/
}

//initialise handler, Uart and GPIO
extern void protocolInit(ProtocolHandle *handle){
	Protocol_Params *params=(Protocol_Params *)malloc(sizeof(Protocol_Params));

	rb_init_led();
	get_suport_devices();

#ifdef IOT_UART2_USED
	hb_uart_init(handle);
#endif
	//init flag
	params->hertdata[0] = 0x5A;
	params->hertdata[1] = 0x00;
	params->hertdata[2] = 0x55;
	params->hertdata[3] = 0xAA;
	params->hertdata[4] = dataCheck(params->hertdata,4);
	params->hertdata[5] = 0xEF;
	params->hertkey = 1;
	//silan_pmu_wdt_reset_config(32768 * 3);//3s

	*handle = params;
}

//Uart send data and gpio control
extern int protocolSend(ProtocolHandle handle,int number,int src){
	Protocol_Params *params = (Protocol_Params *)handle;
	//unsigned char *data=params->sendData;
	unsigned char init_send_data[6]={0x5A,0,0,0,0,0xEF};
	unsigned char *data=init_send_data;
	
	//unsigned char data[6]{0x5A,0x00,0x55,0xAA,0x00,0xEF};
	if(number>0){
		data[0]=0x5A;
		int t=number;
		
		//if( wakeupCheck(t,src) == 0 )
			//return -1;
		
		if(t>ASR_ID_NULL && t<ASR_ID_TEMP_INC){
			if(t<ASR_ID_ON || t==ASR_ID_END_BASE){
				data[1]=0x01;
				data[2]=0x00;
			}
			if(t==ASR_ID_ON ){
				data[1]=0x02;
				data[2]=0x01;
				//g_ataux_air_status_bsp.ack_onoff=0x02;
			}
			if(t==ASR_ID_OFF){
				data[1]=0x02;
				data[2]=0x02;
				//g_ataux_air_status_bsp.ack_onoff=0x01;
			}
		}
		else if(t>ASR_ID_TEMP_INC-1 && t<ASR_ID_TEMP_DEC+1){
			if(t==ASR_ID_TEMP_INC ){
				data[1]=0x03;
				data[2]=0x01;
				g_ataux_air_status_bsp.ack_temp++;
			}
			if(t==ASR_ID_TEMP_DEC ){
				data[1]=0x03;
				data[2]=0x02;
				g_ataux_air_status_bsp.ack_temp--;
			}
		}
		else if(t<ASR_ID_MODE_BASE){
			data[1]=0x04;
			data[2]=t-ASR_ID_TEMP_BASE+1;
			g_ataux_air_status_bsp.ack_temp=t+7;
		}
		else if(t<ASR_ID_FAN_BASE){
			data[1]=0x05;
			if(t==ASR_ID_MODE_AUTOMODE){//自动模式
				data[2]=1;
			}
			if(t==ASR_ID_MODE_COOLMODE){//制冷模式
				data[2]=2;
			}
			if(t==ASR_ID_MODE_DRYMODE){//除湿模式
				data[2]=3;
			}
			if(t==ASR_ID_MODE_FANMODE ){//送风模式
				data[2]=4;
			}
			if(t==ASR_ID_MODE_HEATMODE ){//加热模式
				data[2]=5;
			}
			g_ataux_air_status_bsp.ack_mode=data[2];
		}
		else if(t<ASR_ID_FANMOVE_BASE){
			data[1]=0x06;
			if(t==ASR_ID_FAN_TINYFAN ){//静音档
				data[2]=0x01;
				g_ataux_air_status_bsp.ack_fan=data[2];
			}
			if(t==ASR_ID_FAN_LOWFAN ){//低风档
				data[2]=0x02;
				g_ataux_air_status_bsp.ack_fan=data[2];
			}
			if(t==ASR_ID_FAN_MIDFAN ){//中风档
				data[2]=0x03;
				g_ataux_air_status_bsp.ack_fan=data[2];
			}
			if(t==ASR_ID_FAN_HIGHFAN ){//高风档
				data[2]=0x04;
				g_ataux_air_status_bsp.ack_fan=data[2];
			}
			if(t==ASR_ID_FAN_TURBFAN ){//强劲档
				data[2]=0x05;
				g_ataux_air_status_bsp.ack_fan=data[2];
			}
			if(t==ASR_ID_FAN_INCFAN ){//增大风
				data[2]=0x06;
				g_ataux_air_status_bsp.ack_fan++;
				if(g_ataux_air_status_bsp.ack_fan >0x05)
					g_ataux_air_status_bsp.ack_fan=0x05;
			}
			if(t==ASR_ID_FAN_DECFAN ){//减小风
				data[2]=0x07;
				g_ataux_air_status_bsp.ack_fan--;
				if(g_ataux_air_status_bsp.ack_fan < 0x01)
					g_ataux_air_status_bsp.ack_fan=0x01;
			}
			if(t==ASR_ID_FAN_MAXFAN){//最小风
				data[2]=0x08;
				g_ataux_air_status_bsp.ack_fan=0x05;
			}
			if(t==ASR_ID_FAN_MINFAN){//最大风
				data[2]=0x09;
				g_ataux_air_status_bsp.ack_fan=0x01;
			}
		}
		else if(t<ASR_ID_LIGHT_BASE){
			data[1]=0x07;
			if(t==ASR_ID_FANMOVE_LR_ON){//打开左右
				data[2]=0x01;
				g_ataux_air_status_bsp.ack_swing_lr=0x02;
			}
			if(t==ASR_ID_FANMOVE_LR_OFF){//关闭左右
				data[2]=0x02;
				g_ataux_air_status_bsp.ack_swing_lr=0x01;
			}
			if(t==ASR_ID_FANMOVE_UP_ON ){//打开上下
				data[2]=0x03;
				g_ataux_air_status_bsp.ack_swing_ud=0x02;	
			}
			if(t>=ASR_ID_FANMOVE_UP_OFF ){//关闭上下
				data[2]=0x04;
				g_ataux_air_status_bsp.ack_swing_ud=0x01;	
			}	
		}
		else if(t< (ASR_ID_ALARM_BASE)){
			data[1]=0x08;
			if(t==ASR_ID_LIGHT_ON ){//打开灯
				data[2]=0x02;
				g_ataux_air_status_bsp.ack_display=0x02;
			}
			if(t==ASR_ID_LIGHT_OFF ){//关灯
				data[2]=0x01;
				g_ataux_air_status_bsp.ack_display=0x01;
			}
			if(t==ASR_ID_LIGHT_ON_OFF ){//开关灯，和开灯一样循环的
				data[2]=0x02;
				g_ataux_air_status_bsp.ack_display=0x00;
			}
		}
		data[3]=0x01;
		if(t==ASR_ID_TIME_OUT)	{
			data[1]= 0x01;
			data[2]= 0x00;
			data[3]= 0x00;
		}
		data[MSGSIZE-2]=dataCheck(data,MSGSIZE-2);
		data[MSGSIZE-1]=0xEF;
		buzz_control(number);
		
	#ifdef ASR_ID_RESET_DEFAULT
	static int rst_cnt = 0;
	if( number == ASR_ID_RESET_DEFAULT)
	{
		rst_cnt++;
		if( rst_cnt > 1 )
		{
			rst_cnt = 0;
			gree_ir_init();
			//midea_ir_init();
			//midea2_ir_init();
			//aux_ir_init();
			//LED_RB_Blink(ON);
		}
	}
	else
	{
		rst_cnt = 0;
	}
	#endif
	//printf("g_suport_devices :0x%.8X \n",g_suport_devices);
	//printf("Sort_RemoteSend:0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X \n",data[0],data[1],data[2],data[3],data[4],data[5]);
	if( t>ASR_ID_WAKEUP4&& t<ASR_ID_END_BASE){
		printf("data:%x,%x,%x,%x,%x,%x\n",data[0],data[1],data[2],data[3],data[4],data[5]);
		Sort_RemoteSend(data);
	#ifdef ASR_ID_CONFIG_WIFI
		if(number == ASR_ID_CONFIG_WIFI)
		{
			printf("Switch WiFi to AP mode.\r\n");
			iot_ctauxConfigwifi(handle);
		}
		if(number == ASR_ID_SHOW_NETSTATUS)
		{
			printf("test WiFi to AP mode.\r\n");
			iot_ctaux_test_wifi(handle);
		}
	#endif //ASR_ID_CONFIG_WIFI
	}
	if( t>ASR_ID_WAKEUP4&& t<ASR_ID_END_BASE)
		iot_ctaux_state_report(handle,&g_ataux_air_status_bsp,src);
	}
	return -1;
}

extern int protocolReceive(ProtocolHandle handle){
	int ret = -1;

#ifdef IOT_UART2_USED
	#ifdef __IOT_GREE_USED__
		iot_greeLoop(handle);
	#elif defined __IOT_MIDEA_USED__
		iot_mideaLoop(handle);
	#elif defined __IOT_CTAUX_USED__
		iot_ctaux_main_loop(handle);
	#else
		ret = -1;
	#endif//__IOT_MIDEAD_USED__
#endif

	return -1;
}


