/*
 * file : led.h
 *
 *
 *
 */
 #ifndef __LED_H__
 #define __LED_H__


#define INT_LED_USED

#define ON	(1)
#define OFF	(0)

//============================================================================================================

void rb_init_led(void);

void LED_R_OnOff(int onoff);
void LED_B_OnOff(int onoff);

void LED_R_OnTimeout(int ms);
void LED_B_OnTimeout(int ms);

void LED_R_Blink(int onoff);
void LED_B_Blink(int onoff);
void LED_RB_Blink(int onoff);

void LED_RB_AllOff(void);



//============================================================================================================

void WiFi_OnOff(int onoff);
void WiFi_Reset(void);

//============================================================================================================

#endif//__LED_H__

