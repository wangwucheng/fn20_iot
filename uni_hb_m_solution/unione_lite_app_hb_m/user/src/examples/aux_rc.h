
#ifndef __AUX_RC_H__
#define __AUX_RC_H__

#include "greebanlv.h"
#include "ir_tx.h"
#include "bb_type.h"
/*============================================================================================================*/

void aux_remote_isr(void) ;

void aux_ir_init(void);
void aux_encode_irdata(uint8_t* func_data);
void aux_remote_irsend(void);

/*============================================================================================================*/

int Aux_RemoteSend(unsigned char *data);
/*============================================================================================================*/


#endif//__AUX_RC_H__

