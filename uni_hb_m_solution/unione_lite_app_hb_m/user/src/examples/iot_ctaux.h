
#ifndef __IOT_CTAUX_H__
#define __IOT_CTAUX_H__

#ifdef __IOT_CTAUX_USED__

#define HD_VER		0x10
#define FM_VER		0x10

/* 奥克斯模组与 MCU 通信设计v1.0 */
//===CTAUX START===

//协议头和偏移地址
#define PROTOCOL_SYNC_HEAD1			(0xBB)
#define PROTOCOL_SYNC_HEAD2			(0x00)
#define PROTOCOL_VERSION_1			(0x0C)
#define PROTOCOL_VERSION_2			(0x00)

#define PROTOCOL_DEV_TYPE_1			(0x00)
#define PROTOCOL_DEV_TYPE_2			(0x07)
#define PROTOCOL_SUBDEV_TYPE_1		(0x01)
#define PROTOCOL_SUBDEV_TYPE_2		(0x00)
#define PROTOCOL_RESERVE			(0x01)
#define PROTOCOL_ACK				(0x00)

#define PROTOCOL_OFFSET_VTYPE		(2)
#define PROTOCOL_OFFSET_SUBMSGTYPE 	(4)
#define PROTOCOL_OFFSET_LEN 		(5)
#define PROTOCOL_OFFSET_DEVTYPE		(7)
#define PROTOCOL_OFFSET_SUBDEVTYPE	(9)
#define PROTOCOL_OFFSET_MSG 		(12)
#define PROTOCOL_OFFSET_MSG_CTL		(15)
#define PROTOCOL_OFFSET_MSG_DATA	(16)
#define PROTOCOL_OFFSET_MSG_DATA2	(17)
#define PROTOCOL_OFFSET_MSG_DATA3	(18)
#define PROTOCOL_OFFSET_MSG_DATA4	(19)
#define PROTOCOL_OFFSET_MSG_DATA5	(20)
#define PROTOCOL_OFFSET_MSG_DATA7	(21)
#define PROTOCOL_OFFSET_MSG_DATA8	(22)
#define PROTOCOL_OFFSET_MSG_DATA9	(23)
#define PROTOCOL_MGS_HEAD_SIZE			(9)//不包含mgs,msg从家电类型开始到结束，包含校验
#define PROTOCOL_MGS_DATA_HEAD_SIZE			(12)//不包含msg_data，msg_data指消息体
#define PROTOCOL_MAX_MSGSIZE		(200)

#define PROTOCOL_BB_TYPEID						(0x10) //产品类型: 智能插座

//总协议
typedef struct MSMART_PROTOCOL_PATH{
	unsigned char sync1;//同步头  0XBB00
	unsigned char sync2;
	unsigned char cmd_type1;//协议版本0X0C00
	unsigned char cmd_type2;
	unsigned char subcmd_type;//命令类型
	unsigned char length1;//从消息长度后开始到校验前的长度
	unsigned char length2;
	unsigned char dev_type1;//家电类型
	unsigned char dev_type2;
	unsigned char dev_subtype1;//家电子类型
	unsigned char dev_subtype2;
	unsigned char reserve;//预留
	unsigned char ref_csum1;
	unsigned char ref_csum2;
	unsigned char message[0xa];//消息体 
	unsigned char csum1;//消息校验码 
	unsigned char csum2;
}__attribute__((packed)) ctaux_pro_path,*pctaux_pro_path;

//总协议,透传
typedef struct MSMART_PROTOCOL{
	unsigned char sync1;//同步头  0XBB00
	unsigned char sync2;
	unsigned char cmd_type1;//协议版本0X0C00
	unsigned char cmd_type2;
	unsigned char subcmd_type;//命令类型
	unsigned char length1;//从消息长度后开始到校验前的长度
	unsigned char length2;
	unsigned char dev_type1;//家电类型
	unsigned char dev_type2;
	unsigned char dev_subtype1;//家电子类型
	unsigned char dev_subtype2;
	unsigned char reserve;//预留
	unsigned char message[0x14];//消息体 
	unsigned char csum1;//消息校验码 
	unsigned char csum2;

}__attribute__((packed)) ctaux_pro,*pctaux_pro;

//总协议,查询硬件
typedef struct MSMART_PROTOCOL_QUERY{
	ctaux_pro_path g_protacol_path_csum;
	unsigned char ref_csum1;
	unsigned char ref_csum2;
}__attribute__((packed)) ctaux_pro_query,*pctaux_pro_query;



//空调状态
typedef struct CTAU_STATUS{
	unsigned char ack_head1;//同步头  0x00
	unsigned char ack_type;
	unsigned char ack_flag;
	unsigned char ack_onoff;
	unsigned char ack_temp;
	unsigned char ack_mode;//协议版本0X0C00
	unsigned char ack_fan;
	unsigned char ack_swing_ud;//命令类型
	unsigned char ack_swing_lr;//从消息长度后开始到校验前的长度
	unsigned char ack_display;
}__attribute__((packed)) ctaux_air_status,*pctaux_air_status;


//ACK 类型
#define ACK_MODULE_MSG 			(0x01)
#define ACK_PASSTH_CTL 			(0x02)
#define ACK_MODULE_STATUS 		(0x03)

//SUBCMD类型
//模块信息查询命令
#define MODULE_MSG_QUERY_UP		(0x00)
#define MODULE_MSG_QUERY_DOWN 	(0X01)

//透传
#define PASSTH_CTL_DOWN 		(0X02)

//配网
#define MUC_CTL_MODULE_UP 		(0X03)

//状态同步
#define MODULE_STATUS_DOWM		(0x04)
#define MCU_STATTUS_UP			(0x0c)

//透传业务命令定义/
#define CMD_DEV_FUN_QUERY		(0x01)
#define CMD_DEV_STATUS_QUERY	(0x02)
#define CMD_DEV_ON_OFF			(0x03)
#define CMD_DEV_TEMP_CTL		(0x04)
#define CMD_DEV_MODE_CTL		(0x05)
#define CMD_DEV_WIND_SPEED		(0x06)
#define CMD_DEV_SWING			(0x07)
#define CMD_DEV_DISPLAY			(0x08)
#define CMD_DEV_STATUS_UPDATA  	(0X82)

//透传业务数据定义/
//CMD_DEV_ON_OFF//
#define CMD_AIRCON_ON			(0x02)
#define CMD_AIRCON_OFF			(0x01)

//CMD_DEV_TEMP_CTL//
#define CMD_AIRCON_TEMP16		(0x10)
#define CMD_AIRCON_TEMP17		(0x11)
#define CMD_AIRCON_TEMP18		(0x12)
#define CMD_AIRCON_TEMP19		(0x13)
#define CMD_AIRCON_TEMP20		(0x14)
#define CMD_AIRCON_TEMP21		(0x15)
#define CMD_AIRCON_TEMP22		(0x16)
#define CMD_AIRCON_TEMP23		(0x17)
#define CMD_AIRCON_TEMP24		(0x18)
#define CMD_AIRCON_TEMP25		(0x19)
#define CMD_AIRCON_TEMP26		(0x1A)
#define CMD_AIRCON_TEMP27		(0x1B)
#define CMD_AIRCON_TEMP28		(0x1C)
#define CMD_AIRCON_TEMP29		(0x1D)
#define CMD_AIRCON_TEMP30		(0x1E)
#define CMD_AIRCON_TEMP31		(0x1F)
#define CMD_AIRCON_TEMP32		(0x20)

//CMD_DEV_MODE_CTL//
#define CMD_AIRCON_MODE_AUTO	(0x01)
#define CMD_AIRCON_MODE_COOL	(0x02)
#define CMD_AIRCON_MODE_DRY		(0x03)
#define CMD_AIRCON_MODE_FAN		(0x04)
#define CMD_AIRCON_MODE_HEAT	(0x05)

//CMD_DEV_WIND_SPEED//
#define CMD_AIRCON_FAN_TINY		(0x01)
#define CMD_AIRCON_FAN_LOW		(0x02)
#define CMD_AIRCON_FAN_MID		(0x03)
#define CMD_AIRCON_FAN_HIGH		(0x04)
#define CMD_AIRCON_FAN_TRUB		(0x05)
#define CMD_AIRCON_FAN_AUTO		(0x06)

//CMD_DEV_SWING//
#define CMD_AIRCON_FAN_LR_ON	(0x02)//0DATA
#define CMD_AIRCON_FAN_LR_OFF	(0x01)
#define CMD_AIRCON_FAN_UP_ON	(0x02)//1DATA
#define CMD_AIRCON_FAN_UP_OFF	(0x01)

//CMD_DEV_DISPLAY//
#define CMD_AIRCON_DISPLAY_ON_OFF	(0x00)
#define CMD_AIRCON_DISPLAY_OFF	(0x01)
#define CMD_AIRCON_DISPLAY_ON	(0x02)

//CMD_DEV_STATUS_UPDATA//
#define ACK_OK 					(0X00)
#define ACK_AUTO_UPDATA			(0XFF)

/*============================================================*/

//extern int iot_ctaux_main_loop(ProtocolHandle handle);

/*============================================================*/

void iot_ctauxAircondState(ProtocolHandle handle,unsigned char number);
extern int iot_ctauxConfigwifi(ProtocolHandle handle);
int iot_ctaux_test_wifi(ProtocolHandle handle);
extern int iot_ctauxGetwifistate(void);
int iot_ctaux_ProtocolData_rebootwifi_factory(ProtocolHandle handle);
int iot_ctaux_ProtocolData_getwifi_mac(ProtocolHandle handle);
void iot_ctaux_state_report(ProtocolHandle handle,pctaux_air_status pg_ataux_air_status,int src);
int iot_ctaux_ProtocolData_connectwifi_netcfg(ProtocolHandle handle,char * ssid,char ssid_sz,char * pwd,char pwd_sz,char keytype);

void iot_ctaux_set_typeid(char typeid);	
void setnum(int n,void (* ptr)());
int hb_uart_init(ProtocolHandle handle);
extern int iot_ctaux_receive(ProtocolHandle handle);

//===CTAUX END===
#define CMD_GET_DEV_STATE	(0xA0)
#define CMD_RET_DEV_STATE	(0xA1)
#define CMD_GET_DEV_LIST	(0xA2)
#define CMD_RET_DEV_LIST	(0xA3)
#define CMD_SET_DEV_TYPE	(0xA4)
#define CMD_SET_IDENTMODE	(0xA5)
#define CMD_RET_IDENTMODE	(0xA6)

#define CMD_IR_DATA		(0xB0)
#define CMD_BEEP		(0xBB)

#define CMD_CONFIG_WIRELESS	(0xC0)
#define CMD_RET_WIRELESS	(0xC1)

#define CMD_OAD			(0xC5)

#define CMD_SENDDATA_INDEX	(0xD0)
#define CMD_SENDDATA_D0	(0xD1)
#define CMD_SENDDATA_D1	(0xD2)

#define CMD_RESERVE1	(0xFA)
#define CMD_RET			(0xFB)
#define CMD_RESERVE2	(0xFC)
#define CMD_GET_DEVINFO	(0xFD)
#define CMD_RET_DEVINFO	(0xFE)
#define CMD_RESERVE3	(0xFF)

/*=======================Devices=================================*/

#define CMD_DEV_DEFT		(0x00)
#define CMD_DEV_GREE		(0x01)
#define CMD_DEV_CTAUX		(0x02)
#define CMD_DEV_AUX			(0x03)
#define CMD_DEV_TCL			(0x04)
#define CMD_DEV_HAIER		(0x05)
#define CMD_DEV_HISENSE		(0x06)
#define CMD_DEV_CHIGO		(0x07)
#define CMD_DEV_DOTELS		(0x08)
#define CMD_DEV_IDENT		(0x09)
#define CMD_MAX_DEVICES		(0xFF)
/*=================================================================*/
#define	RES_OK_NODATA		99
#define	RES_OK				1
#define	ERROR_FAIL			0
#define	ERROR_GEN			-1
#define	ERROR_NOTSUPORT		-2
#define	ERROR_CHECKSUM		-3
/*===================================================================*/

#define MSMART_CMD_PASSTHROUGH_CTL					(0x06800000)
#define MSMART_CMD_PASSTHROUGH_REQ					(0x03)

#define MSMART_CMD_REPLY_INFO						(0x06800000)//????????(???)
#define MSMART_CMD_REPLY_INFO_A					(0x05)//????????(???)
#define MSMART_CMD_REPLY_ERR					(0x06)//????????(???)

#define MSMART_CMD_GET_DEV_SN						(0x07)//
#define MSMART_CMD_SET_DEV_SN						(0x11)//??? SN ??
#define MSMART_CMD_GET_DEV_SN_A					(0x65)
#define MSMART_CMD_GET_DEV_INFO					(0xA0)
#define MSMART_CMD_GET_DEV_NETSUPORT				(0xE1)

#define MSMART_CMD_WIFI_NOTIFY				(0x0D)
#define MSMART_CMD_WIFI_COMFIRM				(0x0E)

#define MSMART_CMD_GET_WIFI_STATE				(0x63)//??????????
#define MSMART_CMD_GET_WIFI_MAC				(0x13)//??????MAC

#define MSMART_CMD_CHANGE_WIFI_NETCFG				(0x64)
#define MSMART_CMD_SET_WIFI_SSIDPWD				(0x6A)
#define MSMART_CMD_CHANGE_WIFI_AP_STA					(0x81)
#define MSMART_CMD_SEND_WIFI_REBOOT						(0x82)
#define MSMART_CMD_SEND_WIFI_FAC_REBOOT				(0x83)

#define MSMART_CMD_SET_WIFI_SELFTEST				(0x14)//WiFi?? ?????? �0x14



#endif//__IOT_CTAUX_USED__

#endif//__IOT_CTAUX_H__

