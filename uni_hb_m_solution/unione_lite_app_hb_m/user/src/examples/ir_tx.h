/*
 * file : ir_tx.h
 *
 *
 *
 */
 #ifndef __IR_TX_H__
 #define __IR_TX_H__
//#include "bb_type.h"
#include "type.h"


#define PIN_IR_TX           IO_CONFIG_PA3 //PWM3/GPIOO2_14

// 用户码
#define CUSTOMER_ID_H       0xDF
#define CUSTOMER_ID_L       0x40

#define CFG_PWM_PERIOD				(62)
#define CFG_PWM_HALF_PERIOD		(62/2)

//发送一位数据时需要二个状态，一个是发送前缀，再一个是发送真实的位值。

#define TIMER2_50US_PERIOD	(50)
#define TIMER2_30US_PERIOD	(30)
#define TIMER2_20US_PERIOD	(20)
#define TIMER2_10US_PERIOD	(10)
#define TIMER2_PERIOD	(TIMER2_20US_PERIOD)

//因为时钟是50us发生一次，而540us为前缀，540us为0数据，为了不导致大量偏移，用二个不同的值来校正
#define TICK_500US	(500/TIMER2_PERIOD)	// 
#define TICK_540US		(540/TIMER2_PERIOD)	//
#define TICK_PRE540US		(540/TIMER2_PERIOD)	//
#define TICK_1080US		(1080/TIMER2_PERIOD)	//
#define TICK_1580US		(1580/TIMER2_PERIOD)	//
#define TICK_1620US		(1620/TIMER2_PERIOD)	//
#define TICK_2160US		(2160/TIMER2_PERIOD)	//
#define TICK_3080US		(3080/TIMER2_PERIOD)	//
#define TICK_4400US		(4400/TIMER2_PERIOD)	//
#define TICK_4680US		(4680/TIMER2_PERIOD)	//
#define TICK_5220US		(5220/TIMER2_PERIOD)	//
#define TICK_9000US		(9000/TIMER2_PERIOD)	//
#define TICK_10000US		(10000/TIMER2_PERIOD)	//
#define TICK_13500US		(13500/TIMER2_PERIOD)	//

#define TICK_1MS		(1000/TIMER2_PERIOD)	//
#define TICK_9MS		(9000/TIMER2_PERIOD)	//
#define TICK_13_5MS		(13500/TIMER2_PERIOD)	//
#define TICK_10MS			(10000/TIMER2_PERIOD)	//
#define TICK_50MS			(50000/TIMER2_PERIOD)	//
#define TICK_100MS			(100000/TIMER2_PERIOD)	//

/*============================================================================================================*/

void ir_tx_pin_low(void);
// ir发送引脚 38khz的矩形波
void ir_tx_pin_high(void);

/*
 * 发送ir码
 */
void ir_send(uint8_t value);

/*
 * 初始化，pwm和timer
 */
void ir_tx_init(void);

uint32_t ir_gettick(void);

#endif//__IR_TX_H__

