/**************************************************************************
 * Copyright (C) 2017-2017  Unisound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **************************************************************************
 *
 * Description : hb_smart_ac.c
 * Author      : wufangfang@unisound.com
 * Date        : 2020.04.03
 *
 **************************************************************************/
#include "unione.h"
#include "ac_device_simulator.h"
#include "user_asr.h"
#include "user_player.h"
#include "asr_id.h"
#include "greebanlv.h"
#include "protocolInterface.h"
#define TAG "smart_ac"
#include "ir_tx.h"
#include "iot_ctaux.h"

static char g_power_off_reply[32] = {0};
ProtocolHandle pHandle;
int number=0;
int g_f_dsp=0;

static void _custom_setting_cb(USER_EVENT_TYPE event, user_event_context_t *context) 
{
  event_custom_setting_t *setting = NULL;
   g_f_dsp=1;
 // printf("Action:==buck== =====================\n");	
  if (context) {
    setting = &context->custom_setting;
	printf("Action:==== %s\n", setting->cmd);
	if (NULL != uni_strstr(setting->cmd, "TurnOn")) {
			number=ASR_ID_ON;
	} else if (NULL != uni_strstr(setting->cmd, "TurnOff")) {
			number=ASR_ID_OFF;
	//TEME SETING
	} else if (NULL != uni_strstr(setting->cmd, "TempAdd")) {
			number=ASR_ID_TEMP_INC;
	} else if (NULL != uni_strstr(setting->cmd, "TempRed")) {
			number=ASR_ID_TEMP_DEC;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet16")) {
			number=ASR_ID_TEMP_16;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet17")) {
			number=ASR_ID_TEMP_17;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet18")) {
			number=ASR_ID_TEMP_18;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet19")) {
			number=ASR_ID_TEMP_19;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet20")) {
			number=ASR_ID_TEMP_20;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet21")) {
			number=ASR_ID_TEMP_21;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet22")) {
			number=ASR_ID_TEMP_22;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet23")) {
			number=ASR_ID_TEMP_23;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet24")) {
			number=ASR_ID_TEMP_24;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet25")) {
			number=ASR_ID_TEMP_25;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet26")) {
			number=ASR_ID_TEMP_26;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet27")) {
			number=ASR_ID_TEMP_27;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet28")) {
			number=ASR_ID_TEMP_28;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet29")) {
			number=ASR_ID_TEMP_29;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet30")) {
			number=ASR_ID_TEMP_30;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet31")) {
			number=ASR_ID_TEMP_31;
	} else if (NULL != uni_strstr(setting->cmd, "TempSet32")) {
			number=ASR_ID_TEMP_32;
	//MODE SETING
	} else if (NULL != uni_strstr(setting->cmd, "ModuleAuto")) {
			number=ASR_ID_MODE_AUTOMODE;
	} else if (NULL != uni_strstr(setting->cmd, "ModuleCool")) {
			number=ASR_ID_MODE_COOLMODE;
	} else if (NULL != uni_strstr(setting->cmd, "ModuleDry")) {
			number=ASR_ID_MODE_DRYMODE;
	} else if (NULL != uni_strstr(setting->cmd, "ModuleFan")) {
			number=ASR_ID_MODE_FANMODE;
	} else if (NULL != uni_strstr(setting->cmd, "ModuleHeat")) {
			number=ASR_ID_MODE_HEATMODE;
	//FAN SPEED SETING
	} else if ( uni_strcmp(setting->cmd, "Fantiny")==0) {
			number=ASR_ID_FAN_TINYFAN;
	} else if ( uni_strcmp(setting->cmd, "FanLow")==0) {
			number=ASR_ID_FAN_LOWFAN;
	} else if ( uni_strcmp(setting->cmd, "FanMid")==0) {
			number=ASR_ID_FAN_MIDFAN;
	} else if ( uni_strcmp(setting->cmd, "FanHigh")==0) {
			number=ASR_ID_FAN_HIGHFAN;
	} else if ( uni_strcmp(setting->cmd, "FanTrub")==0) {
			number=ASR_ID_FAN_TURBFAN;
	} else if (uni_strcmp(setting->cmd, "FanAdd")==0) {
			number=ASR_ID_FAN_INCFAN;
	} else if (uni_strcmp(setting->cmd, "FanRed")==0) {
			number=ASR_ID_FAN_DECFAN;
	//SWING SETTING
	} else if (NULL != uni_strstr(setting->cmd, "FanSwingOn")) {
			number=ASR_ID_FANMOVE_LR_ON;
	} else if (NULL != uni_strstr(setting->cmd, "FanSwingOff")) {
			number=ASR_ID_FANMOVE_LR_OFF;
	} else if (NULL != uni_strstr(setting->cmd, "FanLrOn")) {
			number=ASR_ID_FANMOVE_LR_ON;
	} else if (NULL != uni_strstr(setting->cmd, "FanLrOff")) {
			number=ASR_ID_FANMOVE_LR_OFF;
	} else if (NULL != uni_strstr(setting->cmd, "FanUpOn")) {
			number=ASR_ID_FANMOVE_UP_ON;
	} else if (NULL != uni_strstr(setting->cmd, "FanUpOff")) {
			number=ASR_ID_FANMOVE_UP_OFF;
	//LIGHT SETTING
	} else if (NULL != uni_strstr(setting->cmd, "LightOn")) {
			number=ASR_ID_LIGHT_ON;
	} else if (NULL != uni_strstr(setting->cmd, "LightOff")) {
			number=ASR_ID_LIGHT_OFF;
	//NET SETTING
	} else if (NULL != uni_strstr(setting->cmd, "ModuleNet")) {
			number=ASR_ID_CONFIG_WIFI;	
	} else if (NULL != uni_strstr(setting->cmd, "ModuleRet")) {
			number=ASR_ID_RESET_DEFAULT;	
	} else if (NULL != uni_strstr(setting->cmd, "ModuleStatus")) {
			number=ASR_ID_SHOW_NETSTATUS;
	} else {
		printf("_custom_setting_cb ==can't config the number=\n");
	}
  }
}

static void _goto_awakend_cb(USER_EVENT_TYPE event,
                                     user_event_context_t *context) {
  event_goto_awakend_t *awakend = NULL;
   printf("wake:====\n");	
  if (context) {
    awakend = &context->goto_awakend;
    if (EVENT_TRIGGER_ASR == awakend->trigger) {
		number=ASR_ID_WAKEUP1;
		//ir_tx_init();
      LOGT(TAG, "ASR command: %s -> %s -> %s", awakend->cmd, awakend->word_str, awakend->reply_files);
    } /*else {
      LOGT(TAG, "External command: %s", awakend->reply_files);
    }
    if (awakend->reply_files) {
      if(eACSTATE_ON == AcDeviceGetState()) {
        user_player_reply_list_num(awakend->reply_files, 1);
      } else {
        user_player_reply_list_num(awakend->reply_files, 0);
      }
    }*/
  }
}

static void _goto_sleeping_cb (USER_EVENT_TYPE event,
                                     user_event_context_t *context) {
  event_goto_sleeping_t *sleeping = NULL;
  if (context) {
    sleeping = &context->goto_sleeping;
    if (EVENT_TRIGGER_ASR == sleeping->trigger) {
      LOGT(TAG, "ASR command: %s -> %s -> %s", sleeping->cmd, sleeping->word_str,
                                               sleeping->reply_files);
    } else {
      LOGT(TAG, "External command: %s", sleeping->reply_files);
	  number=ASR_ID_TIME_OUT;
	  //user_pwm_final(0);//PWM_NUM_1_A27通道
    }
   /* if (sleeping->reply_files) {
      if(EVENT_TRIGGER_USER == sleeping->trigger) {
        user_player_reply_list_random(g_power_off_reply);
      } else {
        user_player_reply_list_random(sleeping->reply_files);
      }
    }*/
  }
}

void setnumber(int id) 
{  
   number=id;
   return;
}


static int getnumber(void) 
{	int t;
	t=number;
	number=0;
	return t;
}
									 
static void data_send(void *args)
{
	int key=0;
	while (1)
	{
		protocolReceive(pHandle);
		key=getnumber();
		//key=5;
		//uni_msleep(5000);
		if(key !=0){
			if(g_f_dsp==1){
				printf("DSP key_number==:%d\n",key);
				protocolSend(pHandle,key,0);//DSP控制
				g_f_dsp=0;
			}
			else{
				printf("IOT key_number==:%d\n",key);
				protocolSend(pHandle,key,1);//DSP控制
			}
		}
		uni_msleep(100);
	}
}

/*static void _hb_task_message(void *args) {
  TaskStatus_t *StatusArray;
  UBaseType_t task_num;
  int TotalRunTime = 0;
  int x = 0;

  while (1) {

  task_num=uxTaskGetNumberOfTasks();      //获取系统任务数量
  printf("uxTaskGetNumberOfTasks %d\r\n", task_num);

  StatusArray=pvPortMalloc(task_num*sizeof(TaskStatus_t));//申请内存
  if(StatusArray!=NULL)                   //内存申请成功
  {
      uxTaskGetSystemState((TaskStatus_t*   )StatusArray,   //任务信息存储数组
                                     (UBaseType_t     )task_num,  //任务信息存储数组大小
                                     (uint32_t*       )&TotalRunTime);//保存系统总的运行时间
      printf("TaskName\t\tPriority\t\tTaskNumber\t\t\r\n");
      for(x=0;x<task_num;x++)
      {
          printf("%s\t\t%d\t\t\t%d\t\t\t\r\n",
                  StatusArray[x].pcTaskName, //任务名称
                  (int)StatusArray[x].uxCurrentPriority, //任务优先级
                  (int)StatusArray[x].xTaskNumber); //任务编号

      }
  }
  vPortFree(StatusArray); //释放内存
  uni_sleep(5);
  }
}*/


static void send_ir_data_melodyInit(void)
{
	printf("send_ir_data_melodyInit\n");
	thread_param param;
  	uni_pthread_t pid;
  	uni_memset(&param, 0, sizeof(param));
	param.stack_size = STACK_NORMAL_SIZE;
  	param.priority = OS_PRIORITY_NORMAL;
	printf("OS_PRIORITY_NORMAL:%d,OS_PRIORITY_REALTIME:%d\n",OS_PRIORITY_NORMAL,OS_PRIORITY_REALTIME);
	uni_strncpy(param.task_name, "ir_send_process", sizeof(param.task_name) - 1);
	if (0 != uni_pthread_create(&pid, &param, data_send, NULL)) 
  		printf("create thread failed\n");

	/*thread_param param_task;
  	uni_pthread_t pid_task;
  	uni_memset(&param_task, 0, sizeof(param_task));
	param_task.stack_size = STACK_LARGE_SIZE;
  	param_task.priority = OS_PRIORITY_NORMAL;
	uni_strncpy(param_task.task_name, "show_task_process", sizeof(param_task.task_name) - 1);
	if (0 != uni_pthread_create(&pid_task, &param_task, _hb_task_message, NULL)) 
  		printf("create show task thread failed\n");*/
	
	protocolInit(&pHandle);
  	ir_melodyInit();
	ir_tx_init();
  	return E_FAILED;
}


static void _register_event_callback(void) {
  user_event_subscribe_event(USER_CUSTOM_SETTING, _custom_setting_cb);
  user_event_subscribe_event(USER_GOTO_AWAKENED, _goto_awakend_cb);
  user_event_subscribe_event(USER_GOTO_SLEEPING, _goto_sleeping_cb);
}

int hb_smart_ac(void) {
  AcDeviceInit();
  printf("\n===v1.1.3_AUX===\n");
  send_ir_data_melodyInit();
  _register_event_callback();
  return 0;
}

