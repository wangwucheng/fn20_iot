#ifndef _BUZZ_H
#define _BUZZ_H

//#define DEBUG

//#define TIMER0_2MS 		((silan_get_timer_cclk()/1000000)*2000/34)//0x4E20//0x2710

//#define FRQ_OLDSET
#ifdef FRQ_OLDSET
#define TIMER0_2MS 		0x2710
#define DO	25445//33955
#define	RE	22675//30257
#define MI	20200//28571
#define	FA	19065//25445
#define SO	16997//22675
#define	LA	15128//20202
#define XI	13477//17985

#define MUSIC_BEAT						100		//beat time = 2ms * music Beat
#define MUSIC_WAKEUP_BEAT			60
#define MUSIC_ORDER_BEAT			50
#else
#define TIMER0_2MS		20000
//#define TIMER0_2MS		0x4E20//0x2710
#define DO	1712//25445//33955跟进互问测出的
#define	RE	1921//22675//30257
#define MI	2158//20200//28571
#define	FA	2288//19065//25445
#define SO	2568//16997//22675
#define	LA	2898//15128//20202推测的
#define XI	3258//13477//17985推测的

#define MUSIC_BEAT					120		//beat time = 2ms * music Beat
#define MUSIC_WAKEUP_BEAT			72
#define MUSIC_ORDER_BEAT			60
#endif

#define	PERIOD_OFF		20000
#define	DUTY_OFF			0

typedef union{
	unsigned char ubyte;
	struct{
		unsigned char b0:1;
		unsigned char b1:1;
		unsigned char b2:1;
		unsigned char b3:1;
		unsigned char b4:1;
		unsigned char b5:1;
		unsigned char b6:1;
		unsigned char b7:1;
	}b;
}utBytebit;

#define POWER_ON_FLAG					wakeup_flag.b.b0
#define WAKEUP_FLAG 					wakeup_flag.b.b1
#define TIME_OUT_FLAG					wakeup_flag.b.b4
#define ORDER_FLAG						wakeup_flag.b.b3
#define TIPS_FLAG						wakeup_flag.b.b6
#define FAIL_FLAG						wakeup_flag.b.b7

#define STATUS_BIT						1
#define POWER_ON_STATUS				(STATUS_BIT << 0)
#define WAKEUP_STATUS				(STATUS_BIT << 1)
#define TIME_OUT_STATUS				(STATUS_BIT << 4)
#define ORDER_STATUS				(STATUS_BIT << 3)
#define TIPS_STATUS				(STATUS_BIT << 6)
#define FAIL_STATUS				(STATUS_BIT << 7)

#define MUSIC_ON(x) 			x |= 1
#define MUSIC_OFF(x)			x &= 0

#pragma pack(4)
typedef struct{
	unsigned char music_len;//melody time
	unsigned char music_beat;//melody beat
	unsigned int 	*music_note;//MUSIC note
	unsigned char *music_data;//melody data
}music_struct; //config your melody.

void music_play(unsigned char status);
void music_deconfig(void);
void power_on_buzz(void);
void wakeup_buzz(void);
void timeout_on_buzz(void);
void order_buzz(void);
void fail_buzz(void);
void tips_buzz(void);

#endif
