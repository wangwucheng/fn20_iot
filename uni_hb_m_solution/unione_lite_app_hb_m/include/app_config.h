/******************************************************************************
 * @file    app_config.h
 * @author
 * @version V1.0.0
 * @date    02-03-2017
 * @maintainer
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 MVSilicon </center></h2>
 */

#ifndef __APP_CONFIG_H__
#define __APP_CONFIG_H__

#include "type.h"
#include "i2c_host.h"
#include "flash_boot.h"
/**固件版本FIRMWARE_VERSION 在flash_config.h配置**/

/**本系统默认开启2个系统全局宏，在IDE工程配置(Build Settings-Compiler-Symbols)，此处用于提醒**/
/*CFG_APP_CONFIG 和 FUNC_OS_EN*/

/**********************************************************************************************************
 *功能开关说明：
 *CFG_APP_*  : 软件系统应用模式开启，比如USB U盘播放歌曲应用模式的选择
 *CFG_FUNC_* : 软件功能开关
 *CFG_PARA_* : 系统软件参数设置
 *CFG_RES_*  : 系统硬件资源配置
 ************************************************************************************************************
 */

//****************************************************************************************
// 系统App功能模式选择
//****************************************************************************************

/**UDisk播放模式**/
//#define	CFG_APP_USB_PLAY_MODE_EN

/**SD Card播放模式**/
#define	CFG_APP_CARD_PLAY_MODE_EN

/**线路输入模式**/
#define	CFG_APP_LINEIN_MODE_EN

/**HDMI IN模式**/
//#define CFG_APP_HDMIIN_MODE_EN

/**蓝牙功能**/
/*缺省配置：插MT6626蓝牙板,使用COM0,P4.A13/A14/A15/A16(clk)跳线帽连P5，P4.A12飞线T21/EN,P17跳线帽连E3V3*/
//#define CFG_APP_BT_MODE_EN

/**收音机功能**/
/*通信clk 32K使用A16，注意和btplay 协同共享*/
//#define CFG_APP_RADIOIN_MODE_EN

/**SPDIF 光纤模式**/
//#define	CFG_APP_OPTICAL_MODE_EN

/**SPDIF 同轴模式**/
//#define CFG_APP_COAXIAL_MODE_EN

//#define SAVE_RECORD_FILE

//#define READ_TTS_FILE
/**USB声卡，读卡器，一线通功能**/
//#define CFG_APP_USB_AUDIO_MODE_EN

/**I2SIN输入模式**/
//#define CFG_APP_I2SIN_MODE_EN


/**RTC 闹钟配置模式：功能演示,默认不开启**/
#define CFG_APP_RTC_MODE_EN
//****************************************************************************************
// 系统功能配置选项以及参数资源配置
// CFG_FUNC_ : 软件功能开关
// CFG_PARA_ : 系统软件参数设置
// CFG_RES_  : 系统硬件资源配置
//****************************************************************************************

/**音频配置与功能**/
//#define	CFG_PARA_SAMPLE_RATE				(44100)
#define	CFG_PARA_SAMPLE_RATE				(16000)
#define	CFG_PARA_SAMPLES_PER_FRAME          (320) //(128)	//系统默认的帧数大小
#define CFG_PARA_MAX_SAMPLES_PER_FRAME		(512)

#define CFG_PARA_MAX_VOLUME_NUM		        (32)	//SDK 32 级音量
#define CFG_PARA_SYS_VOLUME_DEFAULT			(28)	//SDK默认音量，Source端，Sink端用32


/**转采样**/
/* 使能该宏表示系统会将会以统一的采样率输出，默认使用44.1KHz*/
/* 当做卡拉ok类应用时，强烈建议使能该宏,关闭此宏产生问题：*/
/* 1、mic/dac采样率需要同步跟随音源，sdk暂时只调整dac；音源采样率高时，dac依旧跟随mic。*/
/* 2、提示音默认系统采样率，输出时会更新dac采样率，未做还原，影响音源dac */
#define	CFG_FUNC_MIXER_SRC_EN


/**采样率微调**/ 
/*监测异步音源混音缓冲区水位，（硬件）微调系统音频时钟（分频）,跟随音源(时钟) */
#define	CFG_FUNC_FREQ_ADJUST


/**I2SOUT和DAC-X通道二选一，如果要同时支持需要修改代码**/
/**DAC-X通道配置选择**/
/**其他音频硬件资源在具体的task中配置**/
/**会影响AudioCore sink配置和DMA通道配置**/
#define CFG_RES_AUDIO_DACX_EN


/**I2S音频输出通道配置选择**/
/**其他音频硬件资源在具体的task中配置**/
/**会影响AudioCore sink配置和DMA通道配置**/
/**推荐AP82作为master，MCLK使用内部AP82 PLL产生**/
/**如果作为slave，需要注意MCLK选择和采样率问题，以及异步时钟问题**/
//#define CFG_RES_AUDIO_I2SOUT_EN
#ifdef CFG_RES_AUDIO_I2SOUT_EN
#ifndef	CFG_APP_I2SIN_MODE_EN
#define CFG_RES_I2S_PORT  					1//1:I2S1总线; 0:I2S0总线
#define	CFG_RES_I2S_MODE					0//0:master mode ;1:slave mode
#define	CFG_RES_I2S_IO_PORT					0//选择I2S0时。 0：A0--A4; 1:A24--A28
											 //I2S无效。A8--A12
#endif
#endif

/**I2S输入时硬件资源配置 **/
/**I2S输出也使能时端口选择和模式需要注意保持一致**/
#ifdef CFG_APP_I2SIN_MODE_EN
#define CFG_RES_I2S_PORT  					1//1:I2S1总线; 0:I2S0总线
#define	CFG_RES_I2S_MODE					0//0:master mode ;1:slave mode
#define	CFG_RES_I2S_IO_PORT					0//选择I2S0时。 0：A0--A4; 1:A24--A28
											 //I2S无效。A8--A12
#endif


/**LRC歌词功能 **/
#if(defined(CFG_APP_USB_PLAY_MODE_EN) || defined(CFG_APP_CARD_PLAY_MODE_EN))
//	#define CFG_FUNC_LRC_EN			 	// LRC歌词文件解析
#endif


/**字符编码类型转换 **/
/**目前支持Unicode     ==> Simplified Chinese (DBCS)**/
/**字符转换库由fatfs提供，故需要包含文件系统**/
/**如果支持转换其他语言，需要修改fatfs配置表**/
//#define CFG_FUNC_STRING_CONVERT_EN	// 支持字符编码转换


/**蓝牙资源配置 **/
#ifdef CFG_APP_BT_MODE_EN
	#define	CFG_RES_BT_COM0
	/*某些需求:在非蓝牙模式下,释放协议栈的资源*/
	/*实现方式:关闭蓝牙(BT_LDO_EN DISABLE),未按照正常的蓝牙断开流程,等待手机自动断开*/
	//#define CFG_BT_STACK_MODE
#endif


/**提示音 **/
/*烧录工具参见MVs26_SDK\tools\script*/
/*提示音功能开启，注意flash中const data提示音数据需要预先烧录，否则不会播放。*/
/*const data数据开机检查，影响开机速度，主要用于验证。*/
#define CFG_FUNC_REMIND_SOUND_EN
#ifdef CFG_FUNC_REMIND_SOUND_EN
	#define CFG_FUNC_REMIND_CHECK_EN
	//#define CFG_FUNC_REMIND_SBC
#endif


/**录音支持2种情况，一是写入到SD卡/U盘；二是写入到Flash **/
/*以上2种情况不能同时支持，开启宏时注意一下*/
//#define CFG_FUNC_RECORDER_EN
#ifdef CFG_FUNC_RECORDER_EN
	#define CFG_FUNC_RECORD_SD_UDISK	//录音到SD卡或者U盘
	#ifdef CFG_FUNC_RECORD_SD_UDISK
		#define CFG_FUNC_RECORD_UDISK_FIRST				//U盘和卡同时存在时，录音设备优先选择U盘，否则优先选择录音到SD卡。
		#define CFG_PARA_RECORDS_FOLDER 		"REC"	//录卡录U盘时根目录文件夹。注意ffpresearch_init 使用回调函数过滤字符串。
		#define CFG_FUNC_RECORD_PLAYBACK				//使用播卡播U模式回放录音文件夹
		#define CFG_FUNC_RECORDS_MIN_TIME		1000	//单位ms，开启此宏后，小于这个长度的自动删除。
	#endif

	/*注意flash空间，避免冲突   middleware/flashfs/file.h FLASH_BASE*/
	//#define CFG_FUNC_RECORD_FLASHFS 	//不可同时开启 CFG_FUNC_RECORD_SD_UDISK
	#ifdef CFG_FUNC_RECORD_FLASHFS
		#define CFG_PARA_FLASHFS_FILE_NAME		"REC.MP3"//RECORDER_FILE_NAME
		#define CFG_FUNC_FLASHFS_PLAYBACK		//后续和mediaplay 的回放合并。
	#endif
#endif //CFG_FUNC_RECORDER_EN


/**深度休眠功能**/
#define 	CFG_FUNC_MAIN_DEEPSLEEP_EN	//系统启动先睡眠，等唤醒。
#define	CFG_FUNC_DEEPSLEEP_EN //app模式运行时 可反复睡眠/唤醒。
#if defined(CFG_FUNC_DEEPSLEEP_EN) || defined(CFG_FUNC_MAIN_DEEPSLEEP_EN)
	/**至少一个唤醒源**/ //Source(通道) 不可重复,唤醒IO和功能IO配置需一致。 参见:deepsleep.c
		/*红外按键唤醒,注意CFG_PARA_WAKEUP_GPIO_IR和 唤醒键IR_KEY_POWER设置*/
//	#define CFG_PARA_WAKEUP_SOURCE_IR 		WAKEUP_SOURCE0_GPIO 
		/*ADCKey唤醒 配合CFG_PARA_WAKEUP_GPIO_ADCKEY 唤醒键ADCKEYWAKEUP设置及其电平*/
	#define CFG_PARA_WAKEUP_SOURCE_ADCKEY	WAKEUP_SOURCE1_GPIO//
//	#define CFG_PARA_WAKEUP_SOURCE_CEC		WAKEUP_SOURCE2_GPIO//HDMI专用，CFG_PARA_WAKEUP_GPIO_CEC
	#define CFG_PARA_WAKEUP_SOURCE_RTC		WAKEUP_SOURCE6_RTC//
		/*注意:RTC唤醒默认是延时唤醒，影响rtc模式已设置的闹钟；如需指定时刻唤醒功能，请屏蔽下一行，在rtcmode设置闹钟。*/
//		#define CFG_PARA_WAKEUP_TIME_RTC	60//睡眠时长 秒	-
#endif

/**8段LED显示操作**/
/*LED显存刷新需要在Timer1ms中断进行，读写flash操作时会关闭中断*/
/*所以需要做特殊处理，请关注该宏包含的代码段*/
/*注意timer中断服务函数和调用到的API必须进入TCM*/
//#define	CFG_FUNC_LED_REFRESH


/**FLASH_BOOT功能配置使能 请在flash_boot.h中选择配置。**/
/*1、flash boot占用最开始的64K Byte */
/*2、默认启用flash boot功能，FLASH_BOOT_EN = 1 */
/*3、flashboot实施mva包升级，必须确保void stub(void)指定的0xB8~0xBB版本号不同，同版本不升级*/
/*4、使用pctool mva升级usercode功能，需开启usbdevice模式（检测）*/


/**shell功能启用以及配置请到shell.c中配置**/
/*shell功能默认关闭，默认使用UART1 */
//#define CFG_FUNC_SHELL_EN


/**后插先播功能（开启后关闭后插先播）**/
//#define CFG_FUNC_APP_MODE_AUTO


/**Debug 串口功能选择配置**/
/**硬件UART共有2组，A13和A14为 UART0，其余端口为UART1**/
/**B0和B1和SW调试端口复用**/
/**B2和B3和USB调试端口复用**/
/**彻底关闭打印，请将到debug.h中相关宏注释掉**/
#define CFG_FUNC_DEBUG_EN
#ifdef CFG_FUNC_DEBUG_EN
	#define CFG_UART_RX_PORT  				0//rx port  0--A14，1--B7，2--B1，3--B3
	#define CFG_UART_TX_PORT  				0//tx port  0--A13，1--B6，2--B0，3--B2

	//启用软件模拟UART之后，硬件UART端口自动屏蔽
	//#define CFG_USE_SW_UART
		#define SW_UART_IO_PORT				SWUART_GPIO_PORT_A//SWUART_GPIO_PORT_B
		#define SW_UART_IO_PORT_PIN_INDEX	1//bit num

	#define  CFG_UART_BANDRATE   			115200//DEBUG UART波特率设置
#endif


/**音效控制**/
//#define CFG_FUNC_AUDIO_EFFECT_EN //总音效使能开关
#ifdef CFG_FUNC_AUDIO_EFFECT_EN
	#define CFG_FUNC_AUDIO_EFFECT_LOAD_EN		//导入调音参数，请确保音效列表和参数表一致性。
	#define	CFG_FUNC_AUDIO_EFFECT_ON_LINE_EN	//在线调音开关
	#ifdef	CFG_FUNC_AUDIO_EFFECT_ON_LINE_EN
		#define  CFG_EFFECT_MAJOR_VERSION						(0x1)//音频SDK版本号,不要修改
		#define  CFG_EFFECT_MINOR_VERSION						(0x0)
		#define  CFG_EFFECT_USER_VERSION						(0x0)

		#define  CFG_COMMUNICATION_CRYPTO						(0)////调音通讯加密=1 调音通讯不加密=0
		#define  CFG_COMMUNICATION_PASSWORD                     0x11223344//////四字节的长度密码

		/**在线调音硬件接口有USB HID接口，或者UART接口*/
		/**建议使用USB HID接口，收发buf共512Byte*/
		/**UART接口占用2路DMA，收发Buf共2k Byte*/
		/**建议使用USB HID作为调音接口，DMA资源紧张*/
		/**如果使用UART作为调音接口请设置DMA接口并保证DMA资源充足*/
		#define  CFG_COMMUNICATION_BY_USB						(0)//1:开启	0:关闭
		#define  CFG_COMMUNICATION_BY_UART						(0)//1:开启	0:关闭
		
		#define	 CFG_UART_COMMUNICATION_TX_PIN					GPIOB7
		#define  CFG_UART_COMMUNICATION_TX_PIN_MUX_SEL			(3)
		#define  CFG_UART_COMMUNICATION_RX_PIN					GPIOB6
		#define  CFG_UART_COMMUNICATION_RX_PIN_MUX_SEL			(1)
	#endif
#endif


/**断点记忆功能配置**/
//#define CFG_FUNC_BREAKPOINT_EN
#ifdef CFG_FUNC_BREAKPOINT_EN
	#define BP_PART_SAVE_TO_NVM			// 断点信息保存到NVM
	#define BP_SAVE_TO_FLASH			// 断电信息保存到Flash

	#define FUNC_MATCH_PLAYER_BP		// 获取FS扫描后与播放模式断点信息匹配的文件。文件夹和ID号
#endif


/**PowerKey功能，NVM存储，低功耗RTC等**/
/*PowerKey软开关建议POWERKEY_CNT: 2000；硬开关建议POWERKEY_CNT < 100,可以配置为8 */
//#define	CFG_FUNC_BACKUP_EN
#ifdef CFG_FUNC_BACKUP_EN
	#define	CFG_FUNC_POWERKEY_EN
	#define POWERKEY_MODE					POWERKEY_MODE_PUSH_BUTTON//POWERKEY_MODE_PUSH_BUTTON
	#define POWERKEY_CNT					2000
#endif

/**RTC功能**/
/*注意:当前条件，rtc在rc时钟下精度低，不推荐；32K晶振方案量产芯片不支持;推荐12M晶振模式，deepsleep功耗有所抬高*/
#if defined(CFG_APP_RTC_MODE_EN) || defined(CFG_PARA_WAKEUP_SOURCE_RTC) || defined(CFG_FUNC_ALARM_EN)
#define CFG_RES_RTC_EN
#endif

/**取消AA55判断**/
/*fatfs内磁盘系统MBR和DBR扇区结尾有此标记检测，为提高非标类型盘兼容性，可开启此项, 为有效鉴定无效盘，此项默认关闭*/
//#define	CANCEL_COMMON_SIGNATURE_JUDGMENT

//****************************************************************************************
// TF卡，USB设备检测功能
//****************************************************************************************
#if (defined(CFG_APP_CARD_PLAY_MODE_EN) )|| defined(CFG_FUNC_RECORD_SD_UDISK)
#define	CFG_RES_CARD_USE  //启用SD电路
#ifdef CFG_RES_CARD_USE
#include "sd_card.h"
	#define	CFG_RES_CARD_GPIO				SDIO0_A10_A11_A12	//SDIO1_A5_A6_A7 //注意其他文件参数修改。
	#define	CFG_FUNC_CARD_DETECT	//检测sd卡插入弹出，电路配合
#endif //CFG_RES_CARD_USE
#endif

#if CFG_RES_CARD_GPIO == SDIO1_A5_A6_A7
	//Use SDIO1 请在driver_api\sd_card.h中设置#define SDIO_CARD	SDIO1, 模式中DmaChannelMap中PERIPHERAL_ID_SDIO1_RX
	#define SDIO_Clk_Disable				SDIO1_ClkDisable
	#define SDIO_Clk_Eable					SDIO1_ClkEnable
	#define CARD_DETECT_GPIO				GPIOA6
	#define CARD_DETECT_GPIO_IN				GPIO_A_IN
	#define CARD_DETECT_BIT_MASK			GPIO_INDEX6
	#define CARD_DETECT_GPIO_IE				GPIO_A_IE
	#define CARD_DETECT_GPIO_OE				GPIO_A_OE
	#define CARD_DETECT_GPIO_PU				GPIO_A_PU
	#define CARD_DETECT_GPIO_PD				GPIO_A_PD
#elif CFG_RES_CARD_GPIO == SDIO0_A2_A3_A4
	//Use SDIO0 请在driver_api\sd_card.h中设置#define SDIO_CARD	SDIO0，模式中DmaChannelMap中PERIPHERAL_ID_SDIO0_RX
	#define SDIO_Clk_Disable				SDIO0_ClkDisable
	#define SDIO_Clk_Eable					SDIO0_ClkEnable
	#define CARD_DETECT_GPIO				GPIOA3
	#define CARD_DETECT_GPIO_IN				GPIO_A_IN
	#define CARD_DETECT_BIT_MASK			GPIO_INDEX3
	#define CARD_DETECT_GPIO_IE				GPIO_A_IE
	#define CARD_DETECT_GPIO_OE				GPIO_A_OE
	#define CARD_DETECT_GPIO_PU				GPIO_A_PU
	#define CARD_DETECT_GPIO_PD				GPIO_A_PD
#elif CFG_RES_CARD_GPIO == SDIO0_A10_A11_A12
	//Use SDIO0 请在driver_api\sd_card.h中设置#define SDIO_CARD	SDIO0，模式中DmaChannelMap中PERIPHERAL_ID_SDIO0_RX
	#define SDIO_Clk_Disable				SDIO0_ClkDisable
	#define SDIO_Clk_Eable					SDIO0_ClkEnable
	#define CARD_DETECT_GPIO				GPIOA11
	#define CARD_DETECT_GPIO_IN				GPIO_A_IN
	#define CARD_DETECT_BIT_MASK			GPIO_INDEX11
	#define CARD_DETECT_GPIO_IE				GPIO_A_IE
	#define CARD_DETECT_GPIO_OE				GPIO_A_OE
	#define CARD_DETECT_GPIO_PU				GPIO_A_PU
	#define CARD_DETECT_GPIO_PD				GPIO_A_PD
#endif

/**USB Host检测功能**/
#if(defined(CFG_APP_USB_PLAY_MODE_EN) || defined(CFG_FUNC_RECORD_SD_UDISK))
#define CFG_RES_UDISK_USE
#define CFG_FUNC_UDISK_DETECT
#endif

/**USB Device检测功能**/
#if (defined (CFG_APP_USB_AUDIO_MODE_EN)) || ((CFG_COMMUNICATION_BY_USB == 1))
	#define CFG_FUNC_USB_DEVICE_EN
#endif

//****************************************************************************************
//                            Key 按键相关配置
//****************************************************************************************
/**ADC按键**/
//#define CFG_RES_ADC_KEY_SCAN				//在device service 中启用Key扫描ADCKEY
#if defined(CFG_RES_ADC_KEY_SCAN) || defined(CFG_PARA_WAKEUP_SOURCE_ADCKEY)
	#define CFG_RES_ADC_KEY_USE						//ADC按键功能 启用
#endif
#define CFG_PARA_ADC_KEY_COUNT				1  //key count per adc channel 电路配合
#ifdef CFG_RES_ADC_KEY_USE
	#define CFG_RES_ADC_KEY_PORT_CH1		ADC_CHANNEL_GPIOA29
	#define CFG_RES_ADC_KEY_CH1_ANA_EN		GPIO_A_ANA_EN
	#define CFG_RES_ADC_KEY_CH1_ANA_MASK	GPIO_INDEX29

//	#define CFG_RES_ADC_KEY_PORT_CH2		ADC_CHANNEL_GPIOA23
//	#define CFG_RES_ADC_KEY_CH2_ANA_EN		GPIO_A_ANA_EN
//	#define CFG_RES_ADC_KEY_CH2_ANA_MASK	GPIO_INDEX23
#endif //CFG_RES_ADC_KEY_USE

/**IE按键，PWC捕获IR波形，DMA进内存，解析IR数据。目前仅支持NEC格式遥控器**/
/*需要资源：code:2K; Ram:340Byte，一个PWC，一个DMA*/
/*PWC相关配置见 ir_nec_key.h*/
/*默认使用Timer4，GPIOA24*/
//#define CFG_RES_IR_KEY_SCAN				//启用device service Key扫描IRKey
#if defined(CFG_RES_IR_KEY_SCAN) || defined(CFG_PARA_WAKEUP_SOURCE_IR)
#define	CFG_RES_IR_KEY_USE
	#define IR_TIMER_USE_RC12M //目的:deepsleep scan和工作态统一用rc时钟 控制功耗
#endif


/**编码旋钮按键**/
#define	CFG_RES_CODE_KEY_USE
#ifdef CFG_RES_CODE_KEY_USE
	#define CFG_CODE_KEY1P_BANK				'A'
	#define CFG_CODE_KEY1P_PIN				(1)
	#define CFG_CODE_KEY1N_BANK				'A'
	#define CFG_CODE_KEY1N_PIN				(0)
#endif

//****************************************************************************************
//                            HDMI功能相关配置参数
//****************************************************************************************
#ifdef  CFG_APP_HDMIIN_MODE_EN
	/**ARC**/
	#define HDMI_ARC_RECV_IO_OE				GPIO_A_OE
	#define HDMI_ARC_RECV_IO_IE				GPIO_A_IE
	#define HDMI_ARC_RECV_IO_ANA			GPIO_A_ANA_EN
	#define HDMI_ARC_RECV_IO_PIN			GPIOA28
	#define HDMI_ARC_RECV_ANA_PIN           SPDIF_ANA_INPUT_A28

	/**CEC**/  //timer3使用rc时钟
	#define HDMI_CEC_RECV_IO	            TIMER3_PWC_A23_A27_B0_B2
	#define HDMI_CEC_RECV_IO_PIN	    	(1)
	#define HDMI_CEC_SEND_IO	            TIMER3_PWM_A23_A27_B0_B2
	#define HDMI_CEC_SEND_IO_PIN			(1)

	#define HDMI_CEC_RECOVERY_OUT_IO()		GPIO_RegOneBitClear(GPIO_A_IE, GPIOA27),\
											GPIO_RegOneBitSet(GPIO_A_OE, GPIOA27),\
											GPIO_RegOneBitClear(GPIO_A_OUT, GPIOA27)
	#define HDMI_CEC_RECOVERY_IN_IO()		GPIO_RegOneBitSet(GPIO_A_IE, GPIOA27),\
											GPIO_RegOneBitClear(GPIO_A_OE, GPIOA27)
	#define HDMI_CEC_RECOVERY_RECV_IO()		GPIO_RegOneBitClear(GPIO_A_OE, GPIOA27),\
											GPIO_RegOneBitClear(GPIO_A_IE, GPIOA27)
	#define HDMI_CEC_PULLUP_RECV_IO()	    GPIO_RegOneBitSet(GPIO_A_PU, GPIOA27),\
											GPIO_RegOneBitClear(GPIO_A_PD, GPIOA27)
	#define HDMI_CEC_LEVEL()				GPIO_RegOneBitGet(GPIO_A_IN, GPIO_INDEX27)
	#define HDMI_CEC_WAKEUP_SOURCE			WAKEUP_GPIOA27

	#define	TIMER3_PWC_DATA_ADDR			(0x4002F034)
	#define TIMER3_PWM_DUTY_ADDR			(0x4002F024)

	#define HDMI_CEC_RECV_TIMER_ID	        TIMER3
	#define HDMI_CEC_RECV_DMA_ID	        PERIPHERAL_ID_TIMER3
	#define HDMI_CEC_RECV_DMA_ADDR	        TIMER3_PWC_DATA_ADDR

	#define HDMI_CEC_SEND_TIMER_ID	        TIMER3
	#define HDMI_CEC_SEND_DMA_ID	        PERIPHERAL_ID_TIMER3
	#define HDMI_CEC_SEND_DMA_ADDR	        TIMER3_PWM_DUTY_ADDR

	#define HDMI_CEC_ARBITRATION_TIMER_ID   TIMER5
	#define HDMI_CEC_ARBITRATION_TIMER_IRQ  Timer5_IRQn
	#define HDMI_CEC_ARBITRATION_TIMER_FUNC Timer5Interrupt

	/**HPD**/
	#define HDMI_HPD_CHECK_STATUS_IO	   	GPIO_A_IN
	#define HDMI_HPD_CHECK_INI_IO		   	GPIO_A_INT
	#define HDMI_HPD_CHECK_STATUS_IO_PIN   	GPIOA8
	#define HDMI_HPD_CHECK_IO_INIT()	   	GPIO_RegOneBitClear(GPIO_A_PU, GPIOA8),\
										   	GPIO_RegOneBitSet(GPIO_A_PD, GPIOA8),\
										   	GPIO_RegOneBitClear(GPIO_A_OE, GPIOA8),\
										   	GPIO_RegOneBitSet(GPIO_A_IE, GPIOA8),\
										   	GPIO_INTEnable(GPIO_A_INT, GPIOA8, GPIO_EDGE_TRIGGER),\
										   	GPIO_INTFlagClear(GPIO_A_INT, GPIOA8)

	/**DDC**/
	#define DDC_USE_SW_I2C
	#ifdef DDC_USE_SW_I2C
		#define DDCSclPortSel               PORT_A
		#define SclIndex                    (3)
		#define DDCSdaPortSel               PORT_A
		#define SdaIndex                    (2)
	#else
	   #define HDMI_DDC_DATA_IO_INIT()		GPIO_RegOneBitSet(GPIO_B_PU, GPIOB3),\
										    GPIO_RegOneBitClear(GPIO_B_PD, GPIOB3)
	   #define HDMI_DDC_CLK_IO_INIT()		GPIO_RegOneBitSet(GPIO_B_PU, GPIOB2),\
										    GPIO_RegOneBitClear(GPIO_B_PD, GPIOB2)
	   #define HDMI_DDC_IO_PIN				I2C_PORT_B2_B3
    #endif
#endif

//****************************************************************************************
//						SPDIF光纤同轴功能相关配置参数
//						当前SDK暂时不支持2者同时使用，后续改进
//****************************************************************************************
#if (defined (CFG_APP_OPTICAL_MODE_EN)) || (defined (CFG_APP_COAXIAL_MODE_EN))
	#define	CFG_FUNC_SPDIF_EN

	#ifdef CFG_APP_OPTICAL_MODE_EN
		#define SPDIF_OPTICAL_INDEX				GPIOA28
		//#define SPDIF_PORT_OE					GPIO_A_OE
		//#define SPDIF_PORT_IE					GPIO_A_IE
		#define SPDIF_OPTICAL_PORT_MODE			4//(digital)
		#define SPDIF_OPTICAL_PORT_ANA_INPUT	SPDIF_ANA_INPUT_A28
	#endif
	#ifdef CFG_APP_COAXIAL_MODE_EN
		#define SPDIF_COAXIAL_INDEX				GPIOA25
		//#define SPDIF_PORT_OE					GPIO_A_OE
		//#define SPDIF_PORT_IE					GPIO_A_IE
		#define SPDIF_COAXIAL_PORT_MODE			12
		#define SPDIF_COAXIAL_PORT_ANA_INPUT	SPDIF_ANA_INPUT_A25
	#endif
#endif

//****************************************************************************************
//                            FLASH相关配置
// 2M大小flash，提示音和flash录音不能同时存在，flash录音的空间大小由flashfs来分配
//****************************************************************************************
/*需关注启动扫描数秒问题。*/
#ifdef CFG_FUNC_RECORD_FLASHFS
#define CFG_RES_FLASHFS_EN
#endif

/*关注：init-defalut.c的stub()、file.h的FLASH_BASE和FLASH_FS_SIZE 参数宏在flash_config.h配置*/

#define REMIND_FLASH_STORE_BASE				CONST_DATA_ADDR //系统flash空间分配决定
/*//0x1A0000  提示音数据空间溢出地址 */
#ifdef FLASHFS_ADDR
#define REMIND_FLASH_STORE_OVERFLOW			FLASHFS_ADDR //flashfs不是缺省配置。
#else
#define REMIND_FLASH_STORE_OVERFLOW			BP_DATA_ADDR
#endif





//****************************************************************************************
//                            配置冲突 编译警告
//****************************************************************************************

//#if defined(CFG_PARA_SHELL_COM0) && defined(CFG_RES_BT_COM0)
//#error	Conflict: Com0  used by Bt / Shell.
//#endif

//#if defined(CFG_PARA_SHELL_COM1) && defined(CFG_PARA_AUDIO_EFFECT_ON_LINE_COM1)
//#error Conflict: Com1  used by effect / shell. //请确认端口不重叠后屏蔽
//#endif

#endif /* APP_CONFIG_H_ */
