#ifndef INC_UNI_PCM_ARRY_H_
#define INC_UNI_PCM_ARRY_H_

typedef struct {
  uni_u32 number;
  uni_u32 offset;
  uni_u32 len;
}PCM_RESOURCE;

const PCM_RESOURCE g_pcm_arry[] = {
  {0, 0x0L, 0x480},
  {1, 0x480L, 0x1b80},
  {2, 0x2000L, 0x500},
  {3, 0x2500L, 0x500},
  {4, 0x2a00L, 0x3c0},
  {5, 0x2dc0L, 0x600},
  {6, 0x33c0L, 0x3c0},
  {7, 0x3780L, 0x600},
  {8, 0x3d80L, 0x3c0},
  {9, 0x4140L, 0xa80},
  {101, 0x4bc0L, 0xc00},
  {102, 0x57c0L, 0xc40},
  {103, 0x6400L, 0x440},
  {104, 0x6840L, 0x7c0},
  {105, 0x7000L, 0x900},
  {106, 0x7900L, 0xcc0},
  {107, 0x85c0L, 0x900},
  {108, 0x8ec0L, 0x8c0},
  {109, 0x9780L, 0x840},
  {110, 0x9fc0L, 0x800},
  {111, 0xa7c0L, 0x980},
  {112, 0xb140L, 0x980},
  {113, 0xbac0L, 0x980},
  {114, 0xc440L, 0x9c0},
  {115, 0xce00L, 0x900},
  {116, 0xd700L, 0xa40},
  {117, 0xe140L, 0xa80},
  {118, 0xebc0L, 0xa80},
  {119, 0xf640L, 0xa80},
  {120, 0x100c0L, 0xa80},
  {121, 0x10b40L, 0xa80},
  {122, 0x115c0L, 0xa40},
  {123, 0x12000L, 0xa40},
  {124, 0x12a40L, 0xa80},
  {125, 0x134c0L, 0x980},
  {126, 0x13e40L, 0xa80},
  {127, 0x148c0L, 0xa80},
  {128, 0x15340L, 0xac0},
  {129, 0x15e00L, 0xb00},
  {130, 0x16900L, 0xac0},
  {131, 0x173c0L, 0xa00},
  {132, 0x17dc0L, 0x9c0},
  {133, 0x18780L, 0x9c0},
  {134, 0x19140L, 0x980},
  {135, 0x19ac0L, 0x9c0},
  {136, 0x1a480L, 0x880},
  {137, 0x1ad00L, 0x8c0},
  {138, 0x1b5c0L, 0xbc0},
  {139, 0x1c180L, 0xb00},
  {140, 0x1cc80L, 0xc80},
  {141, 0x1d900L, 0xc00},
  {142, 0x1e500L, 0x8c0},
  {143, 0x1edc0L, 0x880},
  {144, 0x1f640L, 0x900},
  {145, 0x1ff40L, 0x7c0},
  {146, 0x20700L, 0x800},
  {147, 0x20f00L, 0x9c0},
  {148, 0x218c0L, 0x840},
  {149, 0x22100L, 0x840},
  {150, 0x22940L, 0xa00},
  {151, 0x23340L, 0xc80},
  {152, 0x23fc0L, 0xa80},
};

#endif
