#ifndef INC_UNI_CMD_CODE_H_
#define INC_UNI_CMD_CODE_H_

typedef struct {
  uni_u8      cmd_code; /* cmd code fro send base on SUCP */
  const char  *cmd_str; /* action string on UDP */;
} cmd_code_map_t;

const cmd_code_map_t g_cmd_code_arry[] = {
  {0x0, "wakeup_uni"},
  {0x1, "TurnOn"},
  {0x2, "TurnOff"},
  {0x3, "TempAdd"},
  {0x4, "TempRed"},
  {0x5, "RgreeAir"},
  {0x6, "TempSet11"},
  {0x7, "TempSet12"},
  {0x8, "TempSet13"},
  {0x9, "TempSet14"},
  {0xa, "TempSet15"},
  {0xb, "TempSet16"},
  {0xc, "TempSet17"},
  {0xd, "TempSet18"},
  {0xe, "TempSet19"},
  {0xf, "TempSet20"},
  {0x10, "TempSet21"},
  {0x11, "TempSet22"},
  {0x12, "TempSet23"},
  {0x13, "TempSet24"},
  {0x14, "TempSet25"},
  {0x15, "TempSet26"},
  {0x16, "TempSet27"},
  {0x17, "TempSet28"},
  {0x18, "TempSet29"},
  {0x19, "TempSet30"},
  {0x1a, "TempSet31"},
  {0x1b, "TempSet32"},
  {0x1c, "TempSet33"},
  {0x1d, "TempSet34"},
  {0x1e, "TempSet35"},
  {0x1f, "TempSet36"},
  {0x20, "TempSet37"},
  {0x21, "TempSet38"},
  {0x22, "TempSet39"},
  {0x23, "TempSet40"},
  {0x24, "TempSet41"},
  {0x25, "TempSet42"},
  {0x26, "TempSet43"},
  {0x27, "TempSet44"},
  {0x28, "TempSet45"},
  {0x29, "TempSet46"},
  {0x2a, "TempSet47"},
  {0x2b, "TempSet48"},
  {0x2c, "TempSet49"},
  {0x2d, "TempSet50"},
  {0x2e, "ModuleAuto"},
  {0x2f, "ModuleCool"},
  {0x30, "ModuleDry"},
  {0x31, "ModuleFan"},
  {0x32, "ModuleHeat"},
  {0x33, "Fantiny"},
  {0x34, "FanLow"},
  {0x35, "FanMid"},
  {0x36, "FanHigh"},
  {0x37, "FanTrub"},
  {0x38, "FanAuto"},
  {0x39, "FanAdd"},
  {0x3a, "FanRed"},
  {0x3b, "FanLrOn"},
  {0x3c, "FanLrOff"},
  {0x3d, "FanUpOn"},
  {0x3e, "FanUpOff"},
  {0x3f, "FanSwingOn"},
  {0x40, "FanSwingOff"},
  {0x41, "LightOn"},
  {0x42, "LightOff"},
  {0x43, "ModuleNet"},
  {0x44, "ModuleRet"},
  {0x45, "ModuleStatus"},
};

#endif
