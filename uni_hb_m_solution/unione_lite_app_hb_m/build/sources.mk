################################################################################
# Automatically-generated file. Do not edit!
################################################################################

O_SRCS := 
C_SRCS := 
S_SRCS := 
S_UPPER_SRCS := 
OBJ_SRCS := 
ASM_SRCS := 
SAG_SRCS += \

OBJCOPY_OUTPUTS := 
OBJS := 
C_DEPS := 
GCOV_OUT := 
SYMBOL_OUTPUTS := 
READELF_OUTPUTS := 
GPROF_OUT := 
SIZE_OUTPUTS := 
EXECUTABLES := 
S_UPPER_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
middleware/rtos/freertos/src \
middleware/rtos/rtos_api \
src/app/src \
src/app/src/sessions \
src/hal/src \
src/sdk/audio/audio_player/src \
src/sdk/idle_detect/src \
src/sdk/player/src/pcm/src \
src/sdk/player/src \
src/sdk/uart/src \
src/sdk/vui/src \
src/utils/arpt/src \
src/utils/auto_string/src \
src/utils/bitmap/src \
src/utils/black_board/src \
src/utils/cJSON/src \
src/utils/config/src \
src/utils/crc16/src \
src/utils/data_buf/src \
src/utils/event/src \
src/utils/event_list/src \
src/utils/event_route/src \
src/utils/float2string/src \
src/utils/fsm/src \
src/utils/hash/src \
src/utils/interruptable_sleep/src \
src/utils/list/src \
src/utils/log/src \
src/utils/string/src \
src/utils/timer/src \
src/utils/uart/src \
startup \
user/src \
user/src/examples
